const { writeFile } = require('fs');
const { argv } = require('yargs');

// read environment variables from .env file
require('dotenv').config();

// read the command line arguments passed with yargs
const environment = argv.environment;
const isProduction = environment === 'prod';

let targetPath = isProduction ? `./src/environments/environment.prod.ts` : `./src/environments/environment.ts`;

targetPath = process.env.IS_CYPRESS_BUILD ? `./src/environments/environment.cypress.ts` : `./src/environments/environment.ts`;

/*production: false,
    isCypressBuild: false,
    apiUrl: 'https://cydash.mooo.com',
    isDemo: false*/

// we have access to our environment variables
// in the process.env object thanks to dotenv
const environmentFileContent = `export const environment = {
       production: ${isProduction},
       isCypress: ${process.env.IS_CYPRESS_BUILD},
       apiUrl: "${process.env.API_URL}",
       isDemo: ${false},
};

`; // write the content to the respective file
writeFile(targetPath, environmentFileContent, function (err: any) {
    if (err) {
        // eslint-disable-next-line no-console
        console.log(err);
    }
    // eslint-disable-next-line no-console
    console.log(`Wrote variables to ${targetPath}`);
});
