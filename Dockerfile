# nginx state for serving content
FROM nginx:1.21.6-alpine AS build

# Set working directory to nginx asset directory
WORKDIR /usr/share/nginx/html

# Remove default nginx static assets
RUN rm -rf ./*

# Copy static assets from builder stage
ADD ./dist/vrt-prod .
ADD ./nginx.conf .
COPY /nginx.conf  /etc/nginx/conf.d/default.conf

# Containers run nginx with global directives and daemon off
ENTRYPOINT ["nginx", "-g", "daemon off;"]
