import { AppVenue, AppVenuePagedResult } from '@vrt/api';
import { ApiCallParameters } from '@vrt/utils';

export class AddVenue {
    static readonly type = '[Venue] Add venue';

    constructor(public payload: { appVenue: AppVenue }) {}
}

export class AddVenueToState {
    static readonly type = '[Venue] Add venue to state';

    constructor(public payload: { appVenue: AppVenue }) {}
}

export class LoadVenues {
    static readonly type = '[Venue] Load venues';

    constructor(public payload: { apiCallParameters: ApiCallParameters }) {}
}

export class LoadAvailableVenues {
    static readonly type = '[Venue] Load venues for normal users';

    constructor(public payload: { apiCallParameters: ApiCallParameters }) {}
}

export class RemoveVenue {
    static readonly type = '[Venue] Remove venue';

    constructor(public payload: { appVenue: AppVenue }) {}
}

export class RemoveVenueFromState {
    static readonly type = '[Venue] Remove venue from state';

    constructor(public payload: { appVenueId: number | undefined }) {}
}

export class SelectVenue {
    static readonly type = '[Venue] select venue from state';

    constructor(public payload: { venueId: number | undefined }) {}
}

export class SetVenues {
    static readonly type = '[Venue] Set venues';

    constructor(public payload: { appVenuePagedResult: AppVenuePagedResult }) {}
}

export class UpdateVenue {
    static readonly type = '[Venue] update venue';

    constructor(public payload: { appVenue: AppVenue }) {}
}

export class UpdateVenueInState {
    static readonly type = '[Venue] update venue in state';

    constructor(public payload: { appVenue: AppVenue }) {}
}
