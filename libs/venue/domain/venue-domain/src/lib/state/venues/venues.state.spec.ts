import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule } from '@angular/router';
import { NgxsModule, Store } from '@ngxs/store';
import { AppVenue, AppVenuePagedResult, VenuesService } from '@vrt/api';
import { METADATA_EXAMPLE, VENUE_EXAMPLE } from '@vrt/testing';
import { DEFAULTPARAMETERS, NotificationSnackBarService } from '@vrt/utils';
import { Spy, createSpyFromClass } from 'jest-auto-spies';
import { MockModule, MockProvider } from 'ng-mocks';
import { of } from 'rxjs';
import { AddVenue, LoadAvailableVenues, LoadVenues, RemoveVenue, SelectVenue, SetVenues, UpdateVenue } from './venues.actions';
import { VenuesState } from './venues.state';

describe('Venues actions', () => {
    let store: Store;
    let venuesService: Spy<VenuesService>;
    let notifyService: Spy<NotificationSnackBarService>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                NgxsModule.forRoot([VenuesState], { developmentMode: true }),
                HttpClientTestingModule,
                MockModule(MatSnackBarModule),
                RouterModule.forRoot([]),
            ],
            providers: [
                MockProvider(MatSnackBar),
                {
                    provide: VenuesService,
                    useValue: createSpyFromClass(VenuesService),
                },
                {
                    provide: NotificationSnackBarService,
                    useValue: createSpyFromClass(NotificationSnackBarService),
                },
            ],
            teardown: { destroyAfterEach: false },
        });
        venuesService = TestBed.inject<any>(VenuesService);
        notifyService = TestBed.inject<any>(NotificationSnackBarService);
        store = TestBed.inject(Store);
    }));

    it('should create an action and add an item', () => {
        store.dispatch(
            new SetVenues({
                appVenuePagedResult: {
                    items: [{ name: 'Test' } as AppVenue],
                    metadata: {},
                },
            }),
        );
        expect(store.selectSnapshot(VenuesState.venues)).toEqual([{ name: 'Test' } as AppVenue]);
    });

    it('should return metadata state', () => {
        store.reset({
            venues: {
                appVenuePagedResult: {
                    metadata: METADATA_EXAMPLE,
                },
            },
        });
        expect(store.selectSnapshot(VenuesState.metadata)).toEqual(METADATA_EXAMPLE);
    });

    it('should return venue state', () => {
        store.reset({
            venues: {
                selectedVenueId: VENUE_EXAMPLE.id,
                appVenuePagedResult: {
                    items: [VENUE_EXAMPLE],
                    metadata: METADATA_EXAMPLE,
                },
            },
        });
        expect(store.selectSnapshot(VenuesState.venue)).toEqual(VENUE_EXAMPLE);
    });

    it('should add a venue', () => {
        venuesService.createVenue.mockReturnValue(of(VENUE_EXAMPLE));
        store.dispatch(new AddVenue({ appVenue: VENUE_EXAMPLE }));

        expect(store.selectSnapshot(VenuesState.venues)).toEqual([VENUE_EXAMPLE]);
        expect(notifyService.showSuccess).toHaveBeenCalled();
    });

    it('should load available venues', () => {
        venuesService.findAvailableVenues.mockReturnValue(
            of({
                items: [VENUE_EXAMPLE],
                metadata: METADATA_EXAMPLE,
            } as AppVenuePagedResult),
        );
        store.dispatch(new LoadAvailableVenues({ apiCallParameters: DEFAULTPARAMETERS }));
        expect(store.selectSnapshot(VenuesState.venues)).toEqual([VENUE_EXAMPLE]);
    });

    it('should load venues', () => {
        venuesService.findVenues.mockReturnValue(
            of({
                items: [VENUE_EXAMPLE],
                metadata: METADATA_EXAMPLE,
            } as AppVenuePagedResult),
        );
        store.dispatch(new LoadVenues({ apiCallParameters: DEFAULTPARAMETERS }));
        expect(store.selectSnapshot(VenuesState.venues)).toEqual([VENUE_EXAMPLE]);
    });

    it('should set selected venue', () => {
        store.reset({
            venues: {
                appVenuePagedResult: {
                    items: [VENUE_EXAMPLE],
                    metadata: METADATA_EXAMPLE,
                },
            },
        });
        store.dispatch(new SelectVenue({ venueId: VENUE_EXAMPLE.id }));
        expect(store.selectSnapshot(VenuesState.venue)).toEqual(VENUE_EXAMPLE);
    });

    it('should remove venue from state', () => {
        venuesService.deleteVenue.mockReturnValue(of(true));
        store.reset({
            venues: {
                appVenuePagedResult: {
                    items: [VENUE_EXAMPLE],
                    metadata: METADATA_EXAMPLE,
                },
            },
        });
        store.dispatch(new RemoveVenue({ appVenue: VENUE_EXAMPLE }));
        expect(store.selectSnapshot(VenuesState.venues)).toEqual([]);
        expect(notifyService.showSuccess).toHaveBeenCalled();
    });

    it('should update venue', () => {
        venuesService.updateVenue.mockReturnValue(of({ ...VENUE_EXAMPLE, name: 'UPDATED' }));
        store.reset({
            venues: {
                appVenuePagedResult: {
                    items: [VENUE_EXAMPLE],
                    metadata: METADATA_EXAMPLE,
                },
            },
        });
        store.dispatch(new UpdateVenue({ appVenue: VENUE_EXAMPLE }));
        expect(store.selectSnapshot(VenuesState.venues)).toEqual([{ ...VENUE_EXAMPLE, name: 'UPDATED' }]);
        expect(notifyService.showSuccess).toHaveBeenCalled();
    });

    it('should return undefined when no venues are available', () => {
        store.reset({
            venues: {
                appVenuePagedResult: {
                    items: undefined,
                    metadata: undefined,
                },
            },
        });

        expect(store.selectSnapshot(VenuesState.venues)).toBeFalsy();
    });
});
