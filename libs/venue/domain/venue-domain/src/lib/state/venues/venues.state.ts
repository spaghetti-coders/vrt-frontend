import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { append, patch, removeItem, updateItem } from '@ngxs/store/operators';
import { AppVenue, AppVenuePagedResult, Metadata, PaginatorState, VenueModel, VenuesService } from '@vrt/api';
import { NotificationSnackBarService } from '@vrt/utils';
import { Observable, from } from 'rxjs';
import { tap } from 'rxjs/operators';
import {
    AddVenue,
    AddVenueToState,
    LoadAvailableVenues,
    LoadVenues,
    RemoveVenue,
    RemoveVenueFromState,
    SelectVenue,
    SetVenues,
    UpdateVenue,
    UpdateVenueInState,
} from './venues.actions';

export interface VenuesStateModel {
    appVenuePagedResult: AppVenuePagedResult;
    selectedVenueId: number | undefined;
}

const defaults = {
    appVenuePagedResult: { items: [], metadata: {} },
    selectedVenueId: undefined,
};

@State<VenuesStateModel>({
    name: 'venues',
    defaults,
})
@Injectable()
export class VenuesState {
    @Selector([VenuesState])
    static metadata({ appVenuePagedResult: { metadata } }: VenuesStateModel): Metadata | undefined {
        return metadata;
    }

    @Selector([VenuesState])
    static venue({ appVenuePagedResult: { items }, selectedVenueId }: VenuesStateModel): AppVenue | undefined {
        return items?.find((appVenue: AppVenue) => appVenue.id === selectedVenueId);
    }

    @Selector([VenuesState])
    static venues({ appVenuePagedResult: { items } }: VenuesStateModel): AppVenue[] | undefined | null {
        return items;
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    constructor(
        private venuesService: VenuesService,
        private store: Store,
        private notificationService: NotificationSnackBarService,
        protected router: Router,
        protected ngZone: NgZone,
    ) {}

    @Action(AddVenue)
    addVenue({ dispatch }: StateContext<VenuesStateModel>, { payload }: AddVenue) {
        const { appVenue } = payload;

        return this.venuesService.createVenue({ ...appVenue } as VenueModel).pipe(
            tap((responseAppVenue: AppVenue) => {
                dispatch(new AddVenueToState({ appVenue: responseAppVenue }));
                this.notificationService.showSuccess('Eintrag gespeichert');
                return this.navigate('/venues', false);
            }),
        );
    }

    @Action(AddVenueToState)
    addVenueToState({ setState }: StateContext<VenuesStateModel>, { payload }: AddVenueToState) {
        const { appVenue } = payload;

        setState(
            patch<VenuesStateModel>({
                appVenuePagedResult: patch<AppVenuePagedResult>({
                    items: append<AppVenue>([appVenue]) as unknown as AppVenue[],
                }),
            }),
        );
    }

    @Action(LoadAvailableVenues)
    loadAvailableVenues({ dispatch }: StateContext<VenuesStateModel>, { payload }: LoadAvailableVenues) {
        const { filter, page, sortBy, sortOrder } = payload.apiCallParameters;
        return this.venuesService
            .findAvailableVenues(page, this.store.selectSnapshot(PaginatorState.pageSize), filter, sortBy, sortOrder)
            .pipe(
                tap((appVenuePagedResult: AppVenuePagedResult) => {
                    dispatch(new SetVenues({ appVenuePagedResult }));
                }),
            );
    }

    @Action(LoadVenues)
    loadVenues({ dispatch }: StateContext<VenuesStateModel>, { payload }: LoadVenues) {
        const { filter, page, sortBy, sortOrder } = payload.apiCallParameters;
        return this.venuesService
            .findVenues(page, this.store.selectSnapshot(PaginatorState.pageSize), filter, sortBy, sortOrder)
            .pipe(tap((appVenuePagedResult: AppVenuePagedResult) => dispatch(new SetVenues({ appVenuePagedResult }))));
    }

    @Action(SelectVenue)
    selectVenue({ patchState }: StateContext<VenuesStateModel>, { payload }: SelectVenue) {
        const { venueId } = payload;
        patchState({ selectedVenueId: venueId });
    }

    @Action(SetVenues)
    setVenues({ patchState }: StateContext<VenuesStateModel>, { payload }: SetVenues) {
        const { appVenuePagedResult } = payload;
        patchState({ appVenuePagedResult: { ...appVenuePagedResult } });
    }

    @Action(RemoveVenue)
    removeVenue({ dispatch }: StateContext<VenuesStateModel>, { payload }: RemoveVenue) {
        const { appVenue } = payload;
        return this.venuesService.deleteVenue(appVenue.id as number).pipe(
            tap(() => {
                dispatch(new RemoveVenueFromState({ appVenueId: appVenue.id }));
                this.notificationService.showSuccess('Eintrag gelöscht');
            }),
        );
    }

    @Action(RemoveVenueFromState)
    removeVenueFromState({ setState }: StateContext<VenuesStateModel>, { payload }: RemoveVenueFromState) {
        const { appVenueId } = payload;
        setState(
            patch<VenuesStateModel>({
                appVenuePagedResult: patch<AppVenuePagedResult>({
                    items: removeItem(({ id }: AppVenue) => id === appVenueId) as unknown as AppVenue[],
                }),
                selectedVenueId: undefined,
            }),
        );
    }

    @Action(UpdateVenue)
    updateVenue({ dispatch }: StateContext<VenuesStateModel>, { payload }: UpdateVenue) {
        const { appVenue } = payload;
        return this.venuesService.updateVenue(appVenue.id as number, appVenue).pipe(
            tap((responseAppVenue: AppVenue) => {
                dispatch(new UpdateVenueInState({ appVenue: responseAppVenue }));
                this.notificationService.showSuccess('Gespeichert');
                return this.navigate('/venues', false);
            }),
        );
    }

    @Action(UpdateVenueInState)
    updateVenueInState({ setState }: StateContext<VenuesStateModel>, { payload }: UpdateVenueInState) {
        const { appVenue } = payload;

        setState(
            patch<VenuesStateModel>({
                appVenuePagedResult: patch<AppVenuePagedResult>({
                    items: updateItem<AppVenue>((venue: AppVenue) => venue.id === appVenue.id, appVenue) as unknown as AppVenue[],
                }),
            }),
        );
    }

    protected navigate(targetPath: string, preserveParams = true): Observable<boolean> {
        return this.ngZone.run(() => {
            if (preserveParams) {
                return from(
                    this.router.navigate([targetPath], {
                        queryParamsHandling: 'preserve',
                    }),
                );
            } else {
                return from(this.router.navigate([targetPath]));
            }
        });
    }
}
