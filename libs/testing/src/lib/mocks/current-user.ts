import { AppUser } from '@vrt/api';

export const currentUser = (loginModel: any): AppUser => {
    return {
        username: loginModel.username,
        role: 'admin',
        status: 1,
        id: 10,
        receiveNotifications: false,
        displayName: loginModel.username,
    };
};
