import { AppVenue } from '@vrt/api';

export const VENUE_EXAMPLE = {
    name: 'Example 1',
    id: 11,
    colorCode: '#BC6BF1',
    address: 'Muster Str. 12',
    capacity: 23,
    isActive: true,
} as AppVenue;
