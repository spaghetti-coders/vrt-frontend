import { AppEvent, AppRegistration, Metadata, RegistrationStatus } from '@vrt/api';

export const EVENT_EXAMPLE = {
    id: 12,
    name: 'Example',
    date: new Date(2024, 12, 12).toISOString(),
    registrationCount: 12,
    registrationStatus: RegistrationStatus.NUMBER_1,
    capacity: 24,
    description: '',
    isOpenForRegistration: true,
    venueAddress: 'Example Street',
    venueId: 11,
    venueName: 'Example Venue',
    waitingListCount: 10,
} as AppEvent;

export const METADATA_EXAMPLE = {
    currentPage: 1,
    totalPages: 10,
    pageSize: 10,
    totalCount: 100,
    hasPrevious: false,
    hasNext: true,
} as Metadata;

export const REGISTRATION_EXAMPLE = {
    userDisplayName: 'Max Muster',
    userId: 25,
    registrationDate: new Date(2021, 10, 25).toISOString(),
    status: RegistrationStatus.NUMBER_1,
    eventId: 12,
} as AppRegistration;
