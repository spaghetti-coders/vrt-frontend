import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { AdminLogService, AppLogEntry, AppLogEntryPagedResult, Metadata, PaginatorState } from '@vrt/api';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoadAudits, SetAudits } from './audit.actions';

export interface AuditStateModel {
    adminLogEntryPagedResult: AppLogEntryPagedResult;
    loaded: boolean;
    selectedLog?: AppLogEntry;
}

const DEFAULT_STATE = {
    adminLogEntryPagedResult: { metadata: {}, items: [] },
    loaded: false,
};

@State<AuditStateModel>({
    name: 'audit',
    defaults: DEFAULT_STATE,
})
@Injectable()
export class AuditState {
    @Selector([AuditState])
    static logEntries({ adminLogEntryPagedResult: { items } }: AuditStateModel): AppLogEntry[] | null | undefined {
        return items;
    }

    @Selector([AuditState])
    static metadata({ adminLogEntryPagedResult: { metadata } }: AuditStateModel): Metadata | undefined {
        return metadata;
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    constructor(
        private store: Store,
        private adminLogService: AdminLogService,
    ) {}

    @Action(LoadAudits)
    loadAudits({ dispatch }: StateContext<AuditStateModel>, { payload }: LoadAudits) {
        const { page, filter, sortColumn, sortBy, isAdmin } = payload;
        if (isAdmin) {
            return this.adminLogService
                .findLogEntries(sortBy, sortColumn, page, this.store.selectSnapshot(PaginatorState.pageSize), filter)
                .pipe(tap((adminLogEntryPagedResult: AppLogEntryPagedResult) => dispatch(new SetAudits({ adminLogEntryPagedResult }))));
        }

        return of(null);
    }

    @Action(SetAudits)
    setAudits({ patchState }: StateContext<AuditStateModel>, { payload }: SetAudits) {
        const { adminLogEntryPagedResult } = payload;
        patchState({ adminLogEntryPagedResult });
    }
}
