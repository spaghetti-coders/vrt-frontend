import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgxsModule, Store } from '@ngxs/store';
import { AdminLogService, AppLogEntry, AppLogEntryPagedResult } from '@vrt/api';
import { LOG_EXAMPLE, METADATA_EXAMPLE } from '@vrt/testing';
import { of } from 'rxjs';
import { LoadAudits, SetAudits } from './audit.actions';
import { AuditState } from './audit.state';

describe('Audit actions', () => {
    let store: Store;
    let adminLogService: AdminLogService;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([AuditState]), HttpClientTestingModule, MatSnackBarModule],
            teardown: { destroyAfterEach: false },
        });
        adminLogService = TestBed.inject(AdminLogService);
        store = TestBed.inject(Store);
    }));

    it('should return log entries and metadata', () => {
        store.reset({
            audit: {
                adminLogEntryPagedResult: {
                    metadata: METADATA_EXAMPLE,
                    items: [LOG_EXAMPLE],
                },
            },
        });
        expect(store.selectSnapshot(AuditState.logEntries)).toEqual([LOG_EXAMPLE]);
        expect(store.selectSnapshot(AuditState.metadata)).toEqual(METADATA_EXAMPLE);
    });

    it('should set audit logs', () => {
        const items = [{ action: 'deleted' } as AppLogEntry];
        store.dispatch(new SetAudits({ adminLogEntryPagedResult: { items } }));
        expect(store.selectSnapshot(AuditState.logEntries)).toEqual(items);
    });

    it('should load audit logs', () => {
        const items = [{ action: 'deleted' } as AppLogEntry];
        adminLogService.findLogEntries = jest.fn().mockReturnValue(
            of({
                items,
                metadata: METADATA_EXAMPLE,
            } as AppLogEntryPagedResult),
        );
        store.dispatch(
            new LoadAudits({
                filter: undefined,
                isAdmin: true,
                page: undefined,
                pageSize: undefined,
                sortBy: undefined,
                sortColumn: undefined,
            }),
        );

        expect(store.selectSnapshot(AuditState.logEntries)).toEqual(items);
    });
});
