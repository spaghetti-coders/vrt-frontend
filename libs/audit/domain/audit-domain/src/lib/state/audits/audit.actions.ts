import { AppLogEntryPagedResult } from '@vrt/api';

export class LoadAudits {
    static readonly type = '[Audit] load audits';

    constructor(
        public payload: {
            page: number | undefined;
            pageSize: number | undefined;
            filter: string | undefined;
            sortColumn: string | undefined;
            sortBy: string | undefined;
            isAdmin: boolean;
        },
    ) {}
}

export class SetAudits {
    static readonly type = '[Audit] set audits';

    constructor(public payload: { adminLogEntryPagedResult: AppLogEntryPagedResult }) {}
}
