import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { ResetUserState } from '@vrt/admin-api';

@Injectable({
    providedIn: 'root',
})
export class LogoutHelperService {
    constructor(private store: Store) {}

    resetCache(): void {
        this.store.dispatch(new ResetUserState());
    }
}
