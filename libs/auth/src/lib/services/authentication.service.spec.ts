import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router, RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';
import { AdminUserState } from '@vrt/admin-api';
import {
    AccountService,
    AppUser,
    LoginModel,
    LoginResult,
    PasswordResetModel,
    RecoveryTokenRequest,
    RefreshTokenRequest,
    RegistrationModel,
    UpdateAccountModel,
    UpdatePasswordModel,
} from '@vrt/api';
import { USER_EXAMPLE } from '@vrt/testing';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { provideAutoSpy } from 'jest-auto-spies';
import { Observable, of } from 'rxjs';
import { AuthenticationService } from './authentication.service';

describe('Authentication Service', () => {
    let service: AuthenticationService;
    let accountService: AccountService;
    let notificationService: NotificationSnackBarService;
    let router: Router;
    const consoleError = console.error;
    const token =
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE2MzY5MTI5MDIsImV4cCI6MTY2ODQ0ODg5NiwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsInVzZXJuYW1lIjoiUGV0ZXIiLCJkaXNwbGF5bmFtZSI6IlJvY2tldCIsIkVtYWlsIjoianJvY2tldEBleGFtcGxlLmNvbSIsIlJvbGUiOlsiVXNlciIsIkFkbWluIl19.GR6cbXbGVxNzs38D8s8nqkaLFoyKSYnWXbTVsx17TT8';
    const refreshToken = 'refreshToken';

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterModule.forRoot([]), NgxsModule.forRoot([AdminUserState]), NoopAnimationsModule],
            providers: [
                provideAutoSpy(NotificationSnackBarService),
                { provide: IS_DEMO_BUILD, useValue: false },
                {
                    provide: AccountService,
                    useValue: {
                        deleteAccount(): Observable<any> {
                            return of(true);
                        },
                        login(params?: { body?: LoginModel }): Observable<LoginResult> {
                            return of({
                                accessToken: token,
                                refreshToken: refreshToken,
                            } as LoginResult);
                        },
                        getCurrentUser(): Observable<AppUser> {
                            return of({ ...USER_EXAMPLE, role: 'admin' });
                        },
                        logout(): Observable<any> {
                            return of(true);
                        },
                        refreshToken(body: RefreshTokenRequest | undefined): Observable<LoginResult> {
                            return of({
                                accessToken: token,
                                refreshToken: refreshToken,
                            } as LoginResult);
                        },
                        register(body: RegistrationModel | undefined): Observable<any> {
                            return of(true);
                        },
                        requestRecoveryToken(body: RecoveryTokenRequest | undefined): Observable<any> {
                            return of(true);
                        },
                        resetPassword(body: PasswordResetModel | undefined): Observable<any> {
                            return of(true);
                        },
                        upateAccount(body: UpdateAccountModel | undefined): Observable<AppUser> {
                            return of({ ...USER_EXAMPLE });
                        },
                    },
                },
            ],
            teardown: { destroyAfterEach: false },
        });
        router = TestBed.inject(Router);
        service = TestBed.inject(AuthenticationService);
        notificationService = TestBed.inject(NotificationSnackBarService);
        accountService = TestBed.inject(AccountService);
    });

    it('should return true for isAdmin check', waitForAsync(() => {
        accountService.getCurrentUser = jest.fn().mockReturnValue(of({ ...USER_EXAMPLE, role: 'admin' }));
        service.requestCurrentUser();
        const isAdmin = service.isAdmin();

        expect(isAdmin).toBe(true);
    }));

    it('should return false for isAdmin check', waitForAsync(() => {
        const isAdmin = service.isAdmin();
        expect(isAdmin).toBe(false);
    }));

    it('should check if user is in role', () => {
        accountService.getCurrentUser = jest.fn().mockReturnValue(of({ ...USER_EXAMPLE, role: 'admin' }));
        service.requestCurrentUser();
        const isAdmin = service.isInRole(['admin', 'manager']);

        expect(isAdmin).toBe(true);
    });

    it('should change user password', waitForAsync(() => {
        accountService.upatePassword = jest.fn().mockReturnValue(of(true));

        notificationService.showSuccess = jest.fn();
        notificationService.showError = jest.fn();

        service.changePassword({ password: 'Changed' } as UpdatePasswordModel);

        expect(accountService.upatePassword).toHaveBeenCalledTimes(1);
        expect(notificationService.showSuccess).toHaveBeenCalledWith('Passwort geändert');
    }));

    it('should delete user account', () => {
        console.error = jest.fn();
        const processLogout = jest.spyOn(service, 'processLogout');
        const success = jest.spyOn(notificationService, 'showSuccess');

        service.deleteAccount();

        expect(processLogout).toHaveBeenCalledTimes(1);
        expect(success).toHaveBeenCalledTimes(1);
        expect(success).toHaveBeenCalledWith('Dein Konto wurde erfolgreich gelöscht.');
        expect(service.getCurrentState()).toStrictEqual({});
    });

    it('should return jwt token', () => {
        localStorage.setItem('auth_token', token);

        expect(service.getJwtToken()).toStrictEqual(token);
    });

    it('should validate the login workflow', () => {
        const storeToken = jest.spyOn(service, 'storeTokens');

        service.login({ username: 'Example', password: 'topSecure' });

        expect(storeToken).toHaveBeenCalledTimes(1);
        expect(service.getCurrentState()).toEqual({
            ...USER_EXAMPLE,
            role: 'admin',
        });
    });

    it('should trigger the demo login workflow', () => {
        service.login({ username: 'Example', password: 'topSecure' });

        expect(service.getJwtToken()).toStrictEqual(token);
    });

    it('should validate the logout workflow', () => {
        const logout = jest.spyOn(service, 'processLogout');

        service.logout();

        expect(logout).toHaveBeenCalledTimes(1);
        expect(service.getCurrentState()).toEqual({});
        expect(service.getJwtToken()).toEqual(null);
    });

    it('should call refreshToken methode', () => {
        localStorage.setItem('auth_token', token);
        localStorage.setItem('refreshToken', refreshToken);

        service.refreshToken().subscribe();

        expect(service.getJwtToken()).toEqual(token);

        localStorage.clear();
        service.refreshToken().subscribe();
    });

    it('should register a new user', () => {
        const navigate = jest.spyOn(router, 'navigate');
        const success = jest.spyOn(notificationService, 'showSuccess');
        const registration = {
            username: 'Example',
            displayName: 'Example',
            password: '123455678',
        };

        service.register(registration);

        expect(success).toHaveBeenCalledTimes(1);
        expect(navigate).toHaveBeenCalledTimes(1);
        expect(navigate).toHaveBeenCalledWith(['/auth/confirm']);
    });

    it('should request a token to reset the user password', () => {
        const navigate = jest.spyOn(router, 'navigate');
        const success = jest.spyOn(notificationService, 'showSuccess');

        service.requestPasswordToken({ email: 'fake.email@mail.com' });

        expect(success).toHaveBeenCalledTimes(1);
        expect(navigate).toHaveBeenCalledTimes(1);
        expect(navigate).toHaveBeenCalledWith(['/auth/login']);
    });

    it('should reset the user password', () => {
        const navigate = jest.spyOn(router, 'navigate');
        const success = jest.spyOn(notificationService, 'showSuccess');

        service.resetPassword({ password: 'password', recoveryToken: token });

        expect(success).toHaveBeenCalledTimes(1);
        expect(navigate).toHaveBeenCalledTimes(1);
        expect(navigate).toHaveBeenCalledWith(['/auth/login']);
    });

    it('should set login status', waitForAsync(() => {
        service.setLoggedInState(true);
        service.isLoggedIn$.subscribe((isLoggedIn: boolean) => {
            expect(isLoggedIn).toBe(true);
        });
    }));

    it('should update user entity', () => {
        const success = jest.spyOn(notificationService, 'showSuccess');

        service.updateUserProfile(USER_EXAMPLE);

        expect(success).toHaveBeenCalledTimes(1);
        expect(service.getCurrentState()).toEqual(USER_EXAMPLE);
    });
});
