import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
    AccountService,
    AppUser,
    LoginModel,
    LoginResult,
    PasswordResetModel,
    RecoveryTokenRequest,
    RefreshTokenRequest,
    RegistrationModel,
    UpdateAccountModel,
    UpdatePasswordModel,
} from '@vrt/api';
import { currentUser } from '@vrt/testing';
import { IS_DEMO_BUILD, NotificationSnackBarService, UrlParser } from '@vrt/utils';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { catchError, mergeMap, tap } from 'rxjs/operators';
import { LogoutHelperService } from './logout-helper.service';

@Injectable({
    providedIn: 'root',
})
export class AuthenticationService {
    private store: BehaviorSubject<AppUser | null> = new BehaviorSubject<AppUser | null>(null);

    // eslint-disable-next-line @typescript-eslint/member-ordering
    currentUser$: Observable<AppUser | null> = this.store.asObservable();

    private _loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    // eslint-disable-next-line @typescript-eslint/member-ordering
    isLoggedIn$: Observable<boolean> = this._loggedIn.asObservable();

    private readonly JWT_TOKEN = 'auth_token';

    private readonly REFRESH_TOKEN = 'refresh_token';

    constructor(
        @Inject(IS_DEMO_BUILD) private isDemoBuild: boolean,
        private notificationService: NotificationSnackBarService,
        private accountService: AccountService,
        private router: Router,
        private logoutHelperService: LogoutHelperService,
    ) {}

    isAdmin(): boolean {
        const user = this.store.getValue();
        if (user?.role) {
            return user.role === 'admin';
        }
        return false;
    }

    isInRole(roles: string[]): boolean {
        let result = false;
        const user = this.store.getValue();
        if (user && user.role) {
            result = roles.includes(user.role);
        }
        return result;
    }

    changePassword(password: UpdatePasswordModel): void {
        this.accountService.upatePassword(password).subscribe(() => {
            this.notificationService.showSuccess('Passwort geändert');
        });
    }

    deleteAccount(): void {
        this.accountService.deleteAccount().subscribe(() => {
            this.store.next({} as AppUser);
            this.processLogout();
            this.notificationService.showSuccess('Dein Konto wurde erfolgreich gelöscht.');
        });
    }

    getCurrentState(): AppUser | null {
        return this.store.getValue();
    }

    getJwtToken(): string | null {
        return localStorage.getItem(this.JWT_TOKEN);
    }

    login(loginModel: LoginModel, route?: string): void {
        if (!this.isDemoBuild) {
            this.accountService
                .login(loginModel)
                .pipe(
                    tap((response: LoginResult) => this.storeTokens(response)),
                    mergeMap((_) => this.accountService.getCurrentUser()),
                )
                .subscribe((data) => {
                    if (data) {
                        this.store.next(data);
                        if (route) {
                            UrlParser.getParams(route);
                            this.router.navigate([UrlParser.returnUrl], UrlParser.queryParams);
                        } else {
                            this.router.navigateByUrl('/events');
                        }
                        this.notificationService.showSuccess('Du hast dich angemeldet.');
                    }
                });
        } else {
            this.demoLogin(loginModel);
        }
    }

    logout(): void {
        this.accountService.logout().subscribe();
        this.processLogout();
    }

    processLogout(): void {
        this.removeTokens();
        this.logoutHelperService.resetCache();
        this.store.next({} as AppUser);
        this.router.navigate(['/auth/login']);
    }

    refreshToken(): Observable<LoginResult> {
        const refreshModel = this.getRefreshTokenRequestModel();
        if (refreshModel) {
            return this.accountService.refreshToken(refreshModel).pipe(
                tap((data: LoginResult) => {
                    this.storeJwtToken(data.accessToken ? data.accessToken : ' ');
                }),
            );
        } else {
            return of({} as LoginResult);
        }
    }

    register(model: RegistrationModel): void {
        this.accountService.register(model).subscribe(() => {
            this.router.navigate(['/auth/confirm']);
            this.notificationService.showSuccess('Du hast dich erfolgreich registriert.');
        });
    }

    requestCurrentUser(route?: string): void {
        this.accountService
            .getCurrentUser()
            .pipe(
                catchError((err) => {
                    this.router.navigate(['/auth/login']);
                    return throwError(err);
                }),
            )
            .subscribe((res) => {
                if (res) {
                    this.store.next(res);
                    if (route) {
                        this.router.navigate([route]);
                    }
                }
            });
    }

    requestPasswordToken(tokenRequest: RecoveryTokenRequest): void {
        this.accountService.requestRecoveryToken(tokenRequest).subscribe(() => {
            this.notificationService.showSuccess('Prüf bitte Dein E-Mail Postfach');
            this.router.navigate(['/auth/login']);
        });
    }

    resetPassword(resetModel: PasswordResetModel): void {
        this.accountService.resetPassword(resetModel).subscribe(() => {
            this.notificationService.showSuccess('Dein Passwort wurde geändert');
            this.router.navigate(['/auth/login']);
        });
    }

    setLoggedInState(loggedIn: boolean): void {
        this._loggedIn.next(loggedIn);
    }

    updateUserProfile(profile: UpdateAccountModel): void {
        this.accountService.upateAccount(profile).subscribe((res) => {
            if (res) {
                this.store.next(res);
                this.notificationService.showSuccess('Gespeichert');
            }
        });
    }

    storeTokens(data: LoginResult): void {
        if (data && data.accessToken && data.refreshToken) {
            localStorage.setItem(this.JWT_TOKEN, data.accessToken);
            localStorage.setItem(this.REFRESH_TOKEN, data.refreshToken);
        }
    }

    private getRefreshTokenRequestModel(): RefreshTokenRequest | null {
        const authToken = localStorage.getItem(this.JWT_TOKEN);
        const refreshToken = localStorage.getItem(this.REFRESH_TOKEN);
        return {
            accessToken: authToken,
            refreshToken: refreshToken,
        } as RefreshTokenRequest;
    }

    private storeJwtToken(jwt: string): void {
        localStorage.setItem(this.JWT_TOKEN, jwt);
    }

    private removeTokens(): void {
        localStorage.removeItem(this.JWT_TOKEN);
        localStorage.removeItem(this.REFRESH_TOKEN);
    }

    private demoLogin(loginModel: LoginModel) {
        const user = currentUser(loginModel);
        sessionStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem(
            this.JWT_TOKEN,
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE2MzY5MTI5MDIsImV4cCI6MTY2ODQ0ODg5NiwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsInVzZXJuYW1lIjoiUGV0ZXIiLCJkaXNwbGF5bmFtZSI6IlJvY2tldCIsIkVtYWlsIjoianJvY2tldEBleGFtcGxlLmNvbSIsIlJvbGUiOlsiVXNlciIsIkFkbWluIl19.GR6cbXbGVxNzs38D8s8nqkaLFoyKSYnWXbTVsx17TT8',
        );
        this.store.next(user);
        this.router.navigate(['/events']);
        this.notificationService.showSuccess('Du hast dich angemeldet.');
    }
}
