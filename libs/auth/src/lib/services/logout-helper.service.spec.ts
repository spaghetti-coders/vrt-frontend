import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';
import { NotificationSnackBarService } from '@vrt/utils';
import { provideAutoSpy } from 'jest-auto-spies';
import { LogoutHelperService } from './logout-helper.service';

describe('LogoutHelperService', () => {
    let service: LogoutHelperService;
    let spy: any;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterTestingModule, MatSnackBarModule, NgxsModule.forRoot()],
            providers: [provideAutoSpy(NotificationSnackBarService)],
            teardown: { destroyAfterEach: false },
        });
        service = TestBed.inject(LogoutHelperService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should call resetCache', () => {
        spy = jest.spyOn(service, 'resetCache');
        service.resetCache();
        expect(service.resetCache).toBeCalledTimes(1);
    });
});
