import { AppUser } from '@vrt/api';

export class ChangeUserStatusQuery {
    static readonly type = '[User] change query type';

    constructor(public payload: { status: number }) {}
}

export class DeleteUser {
    static readonly type = '[User] delete user';

    constructor(public payload: { userId: number }) {}
}

export class ResetUserRegistrations {
    static readonly type = '[User] reset user registrations';
}

export class LoadUserRegistrations {
    static readonly type = '[User] load user registrations';

    constructor(
        public payload: {
            userId: number;
            sortBy?: string;
            page?: number;
            pageSize?: number;
            filter?: string;
            sortOrder?: string;
        },
    ) {}
}

export class LoadUsers {
    static readonly type = '[User] load users';

    constructor(
        public payload: {
            page: number | undefined;
            pageSize: number | undefined;
            filter: string | undefined;
            sortBy: string | undefined;
            sortOrder: string | undefined;
            isAdmin: boolean;
        },
    ) {}
}

export class ResetUserState {
    static readonly type = '[User] Reset state';
}

export class UpdateUser {
    static readonly type = '[User] Update user';

    constructor(public payload: { user: AppUser; currentStatus: number }) {}
}

export class UpdateUserStatus {
    static readonly type = '[User] Update user status';

    constructor(public payload: { user: AppUser }) {}
}
