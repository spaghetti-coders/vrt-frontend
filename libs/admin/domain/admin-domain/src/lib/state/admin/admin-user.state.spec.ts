import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule } from '@angular/router';
import { NgxsModule, Store } from '@ngxs/store';
import { AppRegistrationPagedResult, AppUserPagedResult, EventRegistrationsService, UserStatus, UsersService } from '@vrt/api';
import { METADATA_EXAMPLE, REGISTRATION_EXAMPLE, USERS_EXAMPLE, USER_EXAMPLE } from '@vrt/testing';
import { NotificationSnackBarService } from '@vrt/utils';
import { of } from 'rxjs';
import {
    ChangeUserStatusQuery,
    DeleteUser,
    LoadUserRegistrations,
    LoadUsers,
    ResetUserRegistrations,
    ResetUserState,
    UpdateUser,
    UpdateUserStatus,
} from './admin-user.actions';
import { AdminUserState } from './admin-user.state';

describe('AdminUser actions', () => {
    let store: Store;
    let usersService: UsersService;
    let eventRegistrationsService: EventRegistrationsService;
    let notificationSnackBarService: NotificationSnackBarService;
    console.error = jest.fn();

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([AdminUserState]), HttpClientTestingModule, MatSnackBarModule, RouterModule.forRoot([])],
            providers: [
                {
                    provide: NotificationSnackBarService,
                    useValue: {
                        showSuccess: jest.fn(),
                    },
                },
            ],
            teardown: { destroyAfterEach: false },
        });
        notificationSnackBarService = TestBed.inject(NotificationSnackBarService);
        store = TestBed.inject(Store);
        usersService = TestBed.inject(UsersService);
        eventRegistrationsService = TestBed.inject(EventRegistrationsService);
    }));

    it('should return current user state', () => {
        store.reset({
            adminUser: {
                appUserPagedResult: {
                    items: [USER_EXAMPLE],
                },
            },
        });
        expect(store.selectSnapshot(AdminUserState.users)).toEqual([USER_EXAMPLE]);
    });

    it('should update user status', () => {
        const user = { ...USER_EXAMPLE, status: UserStatus.NUMBER_3 };
        usersService.updateUserStatus = jest.fn().mockReturnValue(of(user));
        store.reset({
            adminUser: {
                appUserPagedResult: {
                    items: [USER_EXAMPLE],
                },
            },
        });

        store.dispatch(new UpdateUserStatus({ user }));
        expect(store.selectSnapshot(AdminUserState.users)).toEqual([user]);
    });

    it('should return metadata', () => {
        store.reset({
            adminUser: {
                appUserPagedResult: {
                    metadata: METADATA_EXAMPLE,
                },
            },
        });

        expect(store.selectSnapshot(AdminUserState.metadata)).toEqual(METADATA_EXAMPLE);
    });

    it('should return user registrations metadata', () => {
        store.reset({
            adminUser: {
                userRegistrationPagedResult: {
                    metadata: METADATA_EXAMPLE,
                },
            },
        });

        expect(store.selectSnapshot(AdminUserState.userRegistrationsMetadata)).toEqual(METADATA_EXAMPLE);
    });

    it('should return user registrations items', () => {
        store.reset({
            adminUser: {
                userRegistrationPagedResult: {
                    items: [REGISTRATION_EXAMPLE],
                },
            },
        });
        expect(store.selectSnapshot(AdminUserState.userRegistrations)).toEqual([REGISTRATION_EXAMPLE]);
    });

    it('should change user status query', () => {
        store.dispatch(new ChangeUserStatusQuery({ status: 1 }));

        expect(store.selectSnapshot(AdminUserState.users)).toEqual([]);
        expect(store.selectSnapshot(AdminUserState.metadata)).toEqual(undefined);
    });

    it('should delete user', () => {
        usersService.deleteUser = jest.fn().mockReturnValue(of(true));
        notificationSnackBarService.showSuccess = jest.fn();
        store.reset({
            adminUser: {
                appUserPagedResult: {
                    items: [USER_EXAMPLE],
                },
            },
        });

        store.dispatch(new DeleteUser({ userId: USER_EXAMPLE.id as number }));

        expect(store.selectSnapshot(AdminUserState.users)).toEqual([]);
        expect(notificationSnackBarService.showSuccess).toHaveBeenCalled();
    });

    it('should load users', () => {
        usersService.findUsers = jest.fn().mockReturnValue(
            of({
                items: USERS_EXAMPLE,
                metadata: METADATA_EXAMPLE,
            } as AppUserPagedResult),
        );
        store.dispatch(
            new LoadUsers({
                filter: undefined,
                page: undefined,
                pageSize: undefined,
                sortBy: undefined,
                sortOrder: undefined,
                isAdmin: true,
            }),
        );

        expect(store.selectSnapshot(AdminUserState.users)).toEqual(USERS_EXAMPLE);
        expect(store.selectSnapshot(AdminUserState.metadata)).toEqual(METADATA_EXAMPLE);
    });

    it('should reset state', () => {
        store.dispatch(new ResetUserState());
        expect(store.selectSnapshot(AdminUserState.users)).toEqual([]);
        expect(store.selectSnapshot(AdminUserState.metadata)).toEqual(undefined);
        expect(store.selectSnapshot(AdminUserState.userRegistrations)).toEqual([]);
        expect(store.selectSnapshot(AdminUserState.userRegistrationsMetadata)).toEqual({
            pageSize: {
                appUserPagedResult: {
                    items: [],
                },
                userRegistrationPagedResult: {
                    items: [],
                },
            },
        });
    });

    it('should update a user', () => {
        const user = { ...USER_EXAMPLE, displayName: 'PETER' };
        usersService.updateUser = jest.fn().mockReturnValue(of(user));
        store.reset({
            adminUser: {
                appUserPagedResult: {
                    items: [USER_EXAMPLE],
                },
            },
        });
        store.dispatch(new UpdateUser({ currentStatus: 1, user }));

        expect(store.selectSnapshot(AdminUserState.users)).toEqual([user]);
        expect(notificationSnackBarService.showSuccess).toHaveBeenCalled();
    });

    it('should load all user registrations', () => {
        eventRegistrationsService.findRegistrationsByUser = jest.fn().mockReturnValue(
            of({
                items: [REGISTRATION_EXAMPLE],
                metadata: METADATA_EXAMPLE,
            } as AppRegistrationPagedResult),
        );

        store.dispatch(new LoadUserRegistrations({ userId: 0 }));
        expect(store.selectSnapshot(AdminUserState.userRegistrationsMetadata)).toEqual(METADATA_EXAMPLE);
        expect(store.selectSnapshot(AdminUserState.userRegistrations)).toEqual([REGISTRATION_EXAMPLE]);
    });

    it('should reset user registration state', () => {
        store.dispatch(new ResetUserRegistrations());
        expect(store.selectSnapshot(AdminUserState.userRegistrationsMetadata)).toEqual({
            pageSize: {
                appUserPagedResult: {
                    items: [],
                },
                userRegistrationPagedResult: {
                    items: [],
                },
            },
        });
        expect(store.selectSnapshot(AdminUserState.userRegistrations)).toEqual([]);
    });
});
