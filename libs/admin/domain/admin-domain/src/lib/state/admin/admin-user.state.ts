import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { patch } from '@ngxs/store/operators';
import {
    AppRegistration,
    AppRegistrationPagedResult,
    AppUser,
    AppUserPagedResult,
    EventRegistrationsService,
    Metadata,
    PaginatorState,
    UpdateUserModel,
    UserStatus,
    UsersService,
} from '@vrt/api';
import { NotificationSnackBarService } from '@vrt/utils';
import { Observable, from, mergeMap, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import {
    ChangeUserStatusQuery,
    DeleteUser,
    LoadUserRegistrations,
    LoadUsers,
    ResetUserRegistrations,
    ResetUserState,
    UpdateUser,
    UpdateUserStatus,
} from './admin-user.actions';

export interface AdminUserStateModel {
    appUserPagedResult: AppUserPagedResult;
    userRegistrationPagedResult: AppRegistrationPagedResult;
    userStatus?: UserStatus;
    selectedUserId?: number;
}

const DEFAULT_STATE = {
    appUserPagedResult: { items: [] } as AppUserPagedResult,
    userRegistrationPagedResult: { items: [] } as AppRegistrationPagedResult,
};

@State<AdminUserStateModel>({
    name: 'adminUser',
    defaults: DEFAULT_STATE,
})
@Injectable()
export class AdminUserState {
    @Selector([AdminUserState])
    static metadata({ appUserPagedResult: { metadata } }: AdminUserStateModel): Metadata | undefined {
        return metadata;
    }

    @Selector([AdminUserState])
    static users({ appUserPagedResult: { items } }: AdminUserStateModel): AppUser[] {
        return <AppUser[]>items;
    }

    @Selector([AdminUserState])
    static userRegistrations({ userRegistrationPagedResult: { items } }: AdminUserStateModel): AppRegistration[] {
        return <AppRegistration[]>items;
    }

    @Selector([AdminUserState, PaginatorState.pageSize])
    static userRegistrationsMetadata(
        { userRegistrationPagedResult: { metadata } }: AdminUserStateModel,
        pageSize: number,
    ): Metadata | undefined {
        return metadata === undefined ? <Metadata>{ pageSize } : metadata;
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    constructor(
        private usersService: UsersService,
        private registrationsService: EventRegistrationsService,
        private store: Store,
        private notificationService: NotificationSnackBarService,
        protected router: Router,
        protected ngZone: NgZone,
    ) {}

    @Action(ChangeUserStatusQuery) changeUserStatusQuery(
        { patchState }: StateContext<AdminUserStateModel>,
        { payload }: ChangeUserStatusQuery,
    ) {
        const { status } = payload;
        patchState({
            userStatus: status as UserStatus,
        });
    }

    @Action(DeleteUser) deleteUser({ patchState, getState }: StateContext<AdminUserStateModel>, { payload }: DeleteUser) {
        const { userId } = payload;
        const users = [...(getState().appUserPagedResult.items ?? [])];
        return this.usersService.deleteUser(userId).pipe(
            tap(() => {
                patchState({
                    appUserPagedResult: {
                        ...getState().appUserPagedResult,
                        items: users?.filter((item) => item.id !== userId),
                    },
                });
                this.notificationService.showSuccess('Gelöscht');
            }),
        );
    }

    @Action(LoadUsers) loadUsers({ patchState, getState }: StateContext<AdminUserStateModel>, { payload }: LoadUsers) {
        const { page, filter, sortBy, sortOrder, isAdmin } = payload;
        if (isAdmin) {
            return this.usersService
                .findUsers(getState().userStatus, page, this.store.selectSnapshot(PaginatorState.pageSize), filter, sortBy, sortOrder)
                .pipe(
                    tap((appUserPagedResult: AppUserPagedResult) => {
                        patchState({
                            appUserPagedResult,
                        });
                    }),
                );
        }
        return of(null);
    }

    @Action(ResetUserState)
    resetUserState({ patchState }: StateContext<AdminUserStateModel>) {
        patchState(DEFAULT_STATE);
    }

    @Action(UpdateUser) updateUser({ getState, patchState }: StateContext<AdminUserStateModel>, { payload }: UpdateUser) {
        const { user, currentStatus } = payload;
        if (user.id) {
            const users = [...(getState().appUserPagedResult.items ?? [])];
            return this.usersService.updateUser(user.id, user as UpdateUserModel).pipe(
                tap((response: AppUser) => {
                    patchState({
                        appUserPagedResult: {
                            ...getState().appUserPagedResult,
                            items: users.map((stateUser) => (stateUser.id === user.id ? response : stateUser)),
                        },
                        selectedUserId: user.id,
                    });
                }),
                mergeMap(() => {
                    this.notificationService.showSuccess('Gespeichert');
                    return this.navigate(`/admin/users/${currentStatus}`);
                }),
            );
        }
        return of(null);
    }

    @Action(UpdateUserStatus) updateUserStatus({ getState, patchState }: StateContext<AdminUserStateModel>, { payload }: UpdateUserStatus) {
        const { user } = payload;
        if (user.id !== undefined && user.status !== undefined) {
            const users = [...(getState().appUserPagedResult.items ?? [])];
            return this.usersService.updateUserStatus(user.id, { status: user.status }, 'body').pipe(
                tap((appUser: AppUser) => {
                    patchState({
                        appUserPagedResult: {
                            ...getState().appUserPagedResult,
                            items: users.map((stateUser) => (stateUser.id === user.id ? appUser : stateUser)),
                        },
                        selectedUserId: user.id,
                    });
                    this.notificationService.showSuccess('Gespeichert');
                }),
            );
        }

        return of(null);
    }

    @Action(LoadUserRegistrations) loadUserRegistrations(
        { patchState }: StateContext<AdminUserStateModel>,
        { payload }: LoadUserRegistrations,
    ) {
        const { userId, sortBy, sortOrder, page, filter } = payload;
        return this.registrationsService
            .findRegistrationsByUser(userId, sortBy, undefined, page, this.store.selectSnapshot(PaginatorState.pageSize), filter, sortOrder)
            .pipe(
                tap((userRegistrationPagedResult: AppRegistrationPagedResult) => {
                    patchState({
                        userRegistrationPagedResult,
                    });
                }),
            );
    }

    @Action(ResetUserRegistrations) userRegistrations({ setState }: StateContext<AdminUserStateModel>) {
        return setState(
            patch<AdminUserStateModel>({
                userRegistrationPagedResult: {
                    ...DEFAULT_STATE.userRegistrationPagedResult,
                },
            }),
        );
    }

    protected navigate(targetPath: string, preserveParams = true): Observable<boolean> {
        return this.ngZone.run(() => {
            if (preserveParams) {
                return from(
                    this.router.navigate([targetPath], {
                        queryParamsHandling: 'preserve',
                    }),
                );
            } else {
                return from(this.router.navigate([targetPath]));
            }
        });
    }
}
