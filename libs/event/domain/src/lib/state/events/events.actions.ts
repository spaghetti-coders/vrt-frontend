import {
    AppEvent,
    AppEventPagedResult,
    AppRegistration,
    AppRegistrationPagedResult,
    EventModel,
    RegistrationStatus,
    RegistrationsApiParameters,
} from '@vrt/api';
import { ApiCallParameters } from '@vrt/utils';

export class AddEvent {
    static readonly type = '[Events] Add event';

    constructor(public payload: { eventModel: EventModel }) {}
}

export class AddUserToAnEvent {
    static readonly type = '[Events] Add user registration to an event';

    constructor(public payload: { userId: number; eventId: number }) {}
}

export class AddCurrentUserToAnEvent {
    static readonly type = '[Events] Add current user registration to an event';

    constructor(public payload: { eventId: number }) {}
}

export class ChangeSelectedEventRegState {
    static readonly type = '[Events] Change registration for event';

    constructor(public payload: { registrationStatus: RegistrationStatus }) {}
}

export class ChangeFilteredState {
    static readonly type = '[Events] Change filter state for event';

    constructor(public payload: { isFiltered: boolean; filterValue: string | undefined }) {}
}

export class EventSaved {
    static readonly type = '[Events] Event saved';

    constructor(public payload: { appEvent: AppEvent }) {}
}

export class FindRegistration {
    static readonly type = '[Events], find registrations';

    constructor(public payload: { registrationsApiParameters: RegistrationsApiParameters }) {}
}

export class LoadAvailableEvents {
    static readonly type = '[Events] load available events';

    constructor(public payload: { apiCallParameters: ApiCallParameters }) {}
}

export class LoadEventById {
    static readonly type = '[Events] load event by id';

    constructor(public payload: { id: number }) {}
}

export class LoadFutureEvents {
    static readonly type = '[Events] load future events ';

    constructor(public payload: { apiCallParameters: ApiCallParameters }) {}
}

export class LoadPastEvents {
    static readonly type = '[Events] load past events';

    constructor(
        public payload: {
            page?: number;
            pageSize?: number;
            filter?: string;
            sortOrder?: 'desc' | 'asc';
            sortBy?: 'Date' | string;
        },
    ) {}
}

export class RegistrationChanged {
    static readonly type = '[Events] Registration changed';

    constructor(
        public payload: {
            appRegistration: AppRegistration;
            appEventId: number;
            removed: boolean;
            userId?: number;
        },
    ) {}
}

export class RemoveEvent {
    static readonly type = '[Event] remove event';

    constructor(public payload: { id: number }) {}
}

export class RemoveEventSaved {
    static readonly type = '[Event] event removed';

    constructor(public payload: { id: number }) {}
}

export class RemoveUserFromEvent {
    static readonly type = '[Events] Remove user from event';

    constructor(public payload: { eventId: number; userId: number }) {}
}

export class RemoveCurrentUserFromEvent {
    static readonly type = '[Events] Remove current user from event';

    constructor(public payload: { eventId: number; userId: number }) {}
}

export class SetEvent {
    static readonly type = '[Event] set event';

    constructor(public payload: { appEvent: AppEvent | undefined }) {}
}

export class SetEventFromStore {
    static readonly type = '[Event] set event from store';

    constructor(public payload: { id: number }) {}
}

export class SetEvents {
    static readonly type = '[Event] set events';

    constructor(public payload: { response: AppEventPagedResult }) {}
}

export class SetRegistrations {
    static readonly type = '[Event] set registrations';

    constructor(public payload: { appRegistrationPagedResult: AppRegistrationPagedResult }) {}
}

export class UpdateEventInState {
    static readonly type = '[Event] set updated event';

    constructor(public payload: { appEvent: AppEvent }) {}
}

export class UpdateEvent {
    static readonly type = '[Event] update event';

    constructor(public payload: { appEvent: AppEvent }) {}
}
