import { HttpEvent } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgxsModule, Store } from '@ngxs/store';
import {
    AppEvent,
    AppEventPagedResult,
    AppRegistration,
    EventModel,
    EventRegistrationsService,
    EventsService,
    RegistrationStatus,
    RegistrationsApiParameters,
} from '@vrt/api';
import { EVENT_EXAMPLE, METADATA_EXAMPLE, REGISTRATION_EXAMPLE, VENUE_EXAMPLE } from '@vrt/testing';
import { ApiCallParameters, DEFAULTPARAMETERS, dayjs } from '@vrt/utils';
import { VenuesState } from '@vrt/venue-api';
import { Spy, createSpyFromClass } from 'jest-auto-spies';
import { of, throwError } from 'rxjs';
import {
    AddCurrentUserToAnEvent,
    AddEvent,
    AddUserToAnEvent,
    ChangeFilteredState,
    FindRegistration,
    LoadAvailableEvents,
    LoadEventById,
    LoadFutureEvents,
    LoadPastEvents,
    RegistrationChanged,
    RemoveCurrentUserFromEvent,
    RemoveEvent,
    RemoveUserFromEvent,
    SetEventFromStore,
    SetEvents,
    UpdateEvent,
} from './events.actions';
import { DEFAULT_EVENT_STATE, EventsState } from './events.state';

import { Component } from '@angular/core';

@Component({
    selector: 'vrt-mock',
    template: '',
})
export class MockComponent {}

describe('AppEvents actions', () => {
    let store: Store;
    let eventsService: Spy<EventsService>;
    let registrationsService: Spy<EventRegistrationsService>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                NgxsModule.forRoot([EventsState, VenuesState], {
                    developmentMode: true,
                    selectorOptions: { injectContainerState: false },
                }),
                HttpClientTestingModule,
                RouterModule.forRoot([
                    {
                        path: 'events',
                        component: MockComponent,
                    },
                ]),
                MatSnackBarModule,
                NoopAnimationsModule,
            ],
            providers: [
                {
                    provide: EventsService,
                    useValue: createSpyFromClass(EventsService),
                },
                {
                    provide: EventRegistrationsService,
                    useValue: createSpyFromClass(EventRegistrationsService),
                },
            ],
            teardown: { destroyAfterEach: false },
        });
        store = TestBed.inject(Store);
        eventsService = TestBed.inject<any>(EventsService);
        registrationsService = TestBed.inject<any>(EventRegistrationsService);
        jest.spyOn(console, 'error').mockImplementation(() => Object);
    });

    afterEach(() => {
        (console.error as jest.Mock).mockRestore();
        jest.clearAllMocks();
    });

    it('should create an action and add an item', () => {
        const EVENT = {
            id: 1,
            registrationStatus: RegistrationStatus.NUMBER_1,
            registrationCount: 12,
            name: 'Test',
            date: new Date(2099, 12, 23).toISOString(),
        } as AppEvent;
        store.dispatch(new SetEvents({ response: { items: [EVENT], metadata: {} } }));
        expect(store.selectSnapshot(EventsState.events)).toEqual([EVENT]);
    });

    it('should add event to state', waitForAsync(() => {
        eventsService.createEvent.mockReturnValue(of(EVENT_EXAMPLE));
        const spy = jest.spyOn(store, 'dispatch');
        expect(store.selectSnapshot(EventsState.events)).toEqual([]);
        store.dispatch(new AddEvent({ eventModel: { ...EVENT_EXAMPLE } as EventModel }));

        expect(spy).toHaveBeenCalledWith({
            payload: { eventModel: { ...EVENT_EXAMPLE } as EventModel },
        });
        expect(store.selectSnapshot(EventsState.events)).toEqual([EVENT_EXAMPLE]);
    }));

    it('should sort events by date and filter value', () => {
        const expectedResponse = [
            {
                capacity: 24,
                date: '2024-12-24T00:00:00.000Z',
                description: '',
                id: 134,
                isOpenForRegistration: true,
                name: 'Example',
                registrationCount: 12,
                registrationStatus: 1,
                venueAddress: 'Example Street',
                venueId: 11,
                venueName: 'Example Venue',
                waitingListCount: 10,
            },
        ];

        store.reset({
            appEvents: {
                ...DEFAULT_EVENT_STATE,
                appEventPagedResult: {
                    items: [
                        {
                            ...EVENT_EXAMPLE,
                            id: 12,
                            date: dayjs('2024-12-22T00:00:00.000Z').toISOString(),
                            venueId: 12,
                        },
                        {
                            ...EVENT_EXAMPLE,
                            id: 13,
                            date: dayjs('2024-10-10T00:00:00.000Z').toISOString(),
                            venueId: 113,
                        },
                        {
                            ...EVENT_EXAMPLE,
                            id: 134,
                            date: dayjs('2024-12-24T00:00:00.000Z').toISOString(),
                        },
                    ],
                    metadata: METADATA_EXAMPLE,
                },
                isFiltered: true,
                filterValue: 'Example 1',
            },
            venues: {
                appVenuePagedResult: {
                    items: [VENUE_EXAMPLE],
                    metadata: METADATA_EXAMPLE,
                },
            },
        });

        expect(store.selectSnapshot(EventsState.events)).toEqual(expectedResponse);
    });

    it('should return events from state', waitForAsync(() => {
        store.reset({
            appEvents: {
                ...DEFAULT_EVENT_STATE,
                appEventPagedResult: {
                    items: [EVENT_EXAMPLE, { ...EVENT_EXAMPLE, id: 13 }],
                },
            },
        });
        expect(store.selectSnapshot(EventsState.events)).toEqual([EVENT_EXAMPLE, { ...EVENT_EXAMPLE, id: 13 }]);
    }));

    it('should return selected event', waitForAsync(() => {
        store.reset({
            appEvents: {
                selectedEvent: EVENT_EXAMPLE,
            },
        });
        expect(store.selectSnapshot(EventsState.selectedEvent)).toEqual(EVENT_EXAMPLE);
    }));

    it('should return selected event by id', waitForAsync(() => {
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    items: [EVENT_EXAMPLE, { ...EVENT_EXAMPLE, id: 13 }],
                },
                selectedEventId: 12,
            },
        });
        expect(store.selectSnapshot(EventsState.selectedVrtEventFromStore)).toEqual(EVENT_EXAMPLE);
    }));

    it('should return inactive events', () => {
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    items: [
                        { ...EVENT_EXAMPLE, date: new Date(2020, 10, 12) },
                        { ...EVENT_EXAMPLE, id: 13, date: new Date(2020, 10, 23) },
                    ],
                },
            },
        });

        expect(store.selectSnapshot(EventsState.inactiveEvents)).toHaveLength(2);
    });

    it('should return meta data information for events', () => {
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    metadata: METADATA_EXAMPLE,
                },
            },
        });
        expect(store.selectSnapshot(EventsState.eventMetadata)).toBeTruthy();
    });

    it('should return registrations', () => {
        store.reset({
            appEvents: {
                appRegistrationPagedResult: {
                    items: [REGISTRATION_EXAMPLE],
                },
            },
        });

        expect(store.selectSnapshot(EventsState.registrations)).toEqual([REGISTRATION_EXAMPLE]);
    });

    it('should return meta data information for registrations', () => {
        store.reset({
            appEvents: {
                appRegistrationPagedResult: {
                    metadata: METADATA_EXAMPLE,
                },
            },
        });
        expect(store.selectSnapshot(EventsState.registrationMetadata)).toBeTruthy();
    });

    it('should return an error when adding a new event', () => {
        eventsService.createEvent.mockReturnValue(throwError(() => new Error('Artificial unexpected HTTP error')));
        store.dispatch(new AddEvent({ eventModel: EVENT_EXAMPLE as EventModel }));
        expect(eventsService.createEvent).toHaveBeenCalledWith(EVENT_EXAMPLE);
    });

    it('should add an user to an event', waitForAsync(() => {
        const response: HttpEvent<AppRegistration> = {
            body: {
                userDisplayName: 'Max Mustermann',
                userId: 12,
                eventId: 12,
                status: RegistrationStatus.NUMBER_0,
                registrationDate: new Date(2021, 10, 12).toISOString(),
            },
        } as HttpEvent<AppRegistration>;
        registrationsService.addRegistration.mockReturnValue(of(response));
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    metadata: METADATA_EXAMPLE,
                },
                appRegistrationPagedResult: {
                    items: [REGISTRATION_EXAMPLE],
                },
            },
        });

        store.dispatch(new AddUserToAnEvent({ userId: 12, eventId: 12 }));

        expect(registrationsService.addRegistration).toHaveBeenCalled();
    }));

    it('should add current to an event', waitForAsync(() => {
        registrationsService.registerToEvent.mockReturnValue(of(REGISTRATION_EXAMPLE));
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    metadata: METADATA_EXAMPLE,
                },
                appRegistrationPagedResult: {
                    items: [REGISTRATION_EXAMPLE],
                },
            },
        });

        store.dispatch(new AddCurrentUserToAnEvent({ eventId: 12 }));

        expect(registrationsService.registerToEvent).toHaveBeenCalled();
    }));

    it('should change event filtered state', () => {
        const spy = jest.spyOn(store, 'dispatch');

        expect(store.selectSnapshot(EventsState.isFiltered)).toBeFalsy();

        store.dispatch(new ChangeFilteredState({ isFiltered: true, filterValue: 'Example' }));

        expect(store.selectSnapshot(EventsState.isFiltered)).toBeTruthy();
        expect(store.selectSnapshot(EventsState.filterValue)).toBe('Example');

        expect(spy).toHaveBeenCalledWith(new ChangeFilteredState({ isFiltered: true, filterValue: 'Example' }));
    });

    it('should return an error when adding current user to an event', () => {
        const logError = console.error;
        console.error = jest.fn();
        registrationsService.registerToEvent.mockReturnValue(throwError(() => new Error('Artificial unexpected HTTP error')));
        store.dispatch(new AddCurrentUserToAnEvent({ eventId: 12 }));
        expect(registrationsService.registerToEvent).toHaveBeenCalled();
        console.error = logError;
    });

    it('should return an error when adding an user to an event', () => {
        const logError = console.error;
        console.error = jest.fn();
        registrationsService.addRegistration.mockReturnValue(throwError(() => new Error('Artificial unexpected HTTP error')));
        store.dispatch(new AddUserToAnEvent({ userId: 12, eventId: 12 }));
        expect(registrationsService.addRegistration).toHaveBeenCalled();
        console.error = logError;
    });

    it('should find registrations', waitForAsync(() => {
        registrationsService.findRegistrationsByEvent.mockReturnValue(of({ items: [REGISTRATION_EXAMPLE] } as any));
        store.dispatch(
            new FindRegistration({
                registrationsApiParameters: DEFAULTPARAMETERS as RegistrationsApiParameters,
            }),
        );

        expect(registrationsService.findRegistrationsByEvent).toHaveBeenCalled();
        expect(store.selectSnapshot(EventsState.registrations)).toEqual([REGISTRATION_EXAMPLE]);
    }));

    it('should return an error when find registrations is called', () => {
        const logError = console.error;
        console.error = jest.fn();
        registrationsService.findRegistrationsByEvent.mockReturnValue(throwError(() => new Error('Artificial unexpected HTTP error')));
        store.dispatch(
            new FindRegistration({
                registrationsApiParameters: DEFAULTPARAMETERS as RegistrationsApiParameters,
            }),
        );
        expect(registrationsService.findRegistrationsByEvent).toHaveBeenCalled();
        console.error = logError;
    });

    it('should return an error when find available events is called', () => {
        eventsService.findAvailableEvents.mockReturnValue(throwError(() => new Error('Artificial unexpected HTTP error')));
        store.dispatch(
            new LoadAvailableEvents({
                apiCallParameters: DEFAULTPARAMETERS as ApiCallParameters,
            }),
        );

        expect(eventsService.findAvailableEvents).toHaveBeenCalled();
    });

    it('should load available events', () => {
        eventsService.findAvailableEvents.mockReturnValue(of({ items: [EVENT_EXAMPLE] } as AppEventPagedResult));

        store.dispatch(
            new LoadAvailableEvents({
                apiCallParameters: DEFAULTPARAMETERS as ApiCallParameters,
            }),
        );

        expect(store.selectSnapshot(EventsState.events)).toEqual([EVENT_EXAMPLE]);
    });

    it('should load event by id', waitForAsync(() => {
        eventsService.getEvent.mockReturnValue(of(EVENT_EXAMPLE));
        const spyStore = jest.spyOn(store, 'dispatch');

        store.dispatch(new LoadEventById({ id: 12 }));

        expect(eventsService.getEvent).toHaveBeenCalled();
        expect(spyStore).toHaveBeenCalledWith(new LoadEventById({ id: 12 }));
        expect(store.selectSnapshot(EventsState.selectedEvent)).toEqual(EVENT_EXAMPLE);
    }));

    it('should load past events', () => {
        eventsService.findPastEvents.mockReturnValue(of({ items: [EVENT_EXAMPLE] } as AppEventPagedResult));

        store.dispatch(new LoadPastEvents({}));

        expect(store.selectSnapshot(EventsState.events)).toEqual([EVENT_EXAMPLE]);
    });

    it('should delete an user registration from state', () => {
        const resonse: HttpEvent<AppRegistration> = {
            body: REGISTRATION_EXAMPLE,
        } as HttpEvent<AppRegistration>;

        registrationsService.deleteRegistration.mockReturnValue(of(resonse));
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    items: [],
                    metadata: METADATA_EXAMPLE,
                },
                appRegistrationPagedResult: {
                    items: [REGISTRATION_EXAMPLE],
                },
            },
        });
        expect(store.selectSnapshot(EventsState.registrations)).toEqual([REGISTRATION_EXAMPLE]);
        store.dispatch(
            new RemoveUserFromEvent({
                eventId: REGISTRATION_EXAMPLE.eventId as number,
                userId: REGISTRATION_EXAMPLE.userId as number,
            }),
        );
        expect(store.selectSnapshot(EventsState.registrations)).toEqual([]);
    });

    it('should remove event', () => {
        eventsService.deleteEvent.mockReturnValue(of(true));
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    items: [EVENT_EXAMPLE],
                    metadata: METADATA_EXAMPLE,
                },
            },
        });
        store.dispatch(new RemoveEvent({ id: EVENT_EXAMPLE.id as number }));
        expect(store.selectSnapshot(EventsState.events)).toEqual([]);
    });

    it('should update event', () => {
        const appEvent: AppEvent = { ...EVENT_EXAMPLE, name: 'UPDATED' };
        eventsService.updateEvent.mockReturnValue(of(appEvent));
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    items: [EVENT_EXAMPLE],
                    metadata: METADATA_EXAMPLE,
                },
            },
        });
        store.dispatch(new UpdateEvent({ appEvent }));
        expect(store.selectSnapshot(EventsState.events)).toEqual([appEvent]);
    });

    it('should load future events', () => {
        eventsService.findFutureEvents.nextWith({
            items: [EVENT_EXAMPLE],
            metadata: METADATA_EXAMPLE,
        });
        store.dispatch(new LoadFutureEvents({ apiCallParameters: DEFAULTPARAMETERS }));
        expect(store.selectSnapshot(EventsState.events)).toEqual([EVENT_EXAMPLE]);
    });

    it('should change registration', () => {
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    items: [EVENT_EXAMPLE],
                    metadata: METADATA_EXAMPLE,
                },
                appRegistrationPagedResult: {
                    items: [],
                    metadata: METADATA_EXAMPLE,
                },
                selectedEvent: EVENT_EXAMPLE,
            },
        });
        store.dispatch(
            new RegistrationChanged({
                appEventId: EVENT_EXAMPLE.id as number,
                userId: 12,
                appRegistration: REGISTRATION_EXAMPLE,
                removed: false,
            }),
        );

        expect(store.selectSnapshot(EventsState.selectedEvent)).toEqual({
            ...EVENT_EXAMPLE,
            registrationCount: 13,
        });
    });

    it('should remove current user from event', () => {
        const registrationChangedSpy = jest.spyOn(store, 'dispatch');
        const resonse: HttpEvent<boolean> = {
            body: true,
        } as HttpEvent<boolean>;

        registrationsService.unregisterFromEvent.mockReturnValue(of(resonse));
        store.dispatch(new RemoveCurrentUserFromEvent({ eventId: 12, userId: 11 }));
        expect(registrationChangedSpy).toHaveBeenCalled();
    });

    it('should set event from state', () => {
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    items: [EVENT_EXAMPLE],
                    metadata: METADATA_EXAMPLE,
                },
            },
        });
        store.dispatch(new SetEventFromStore({ id: 12 }));
        expect(store.selectSnapshot(EventsState.selectedEvent)).toEqual(EVENT_EXAMPLE);
    });
});
