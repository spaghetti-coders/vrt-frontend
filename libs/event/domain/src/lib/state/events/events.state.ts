import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { append, patch, removeItem, updateItem } from '@ngxs/store/operators';
import {
    AppEvent,
    AppEventPagedResult,
    AppRegistration,
    AppRegistrationPagedResult,
    AppVenue,
    EventModel,
    EventRegistrationsService,
    EventsService,
    Metadata,
    PaginatorState,
    RegistrationStatus,
} from '@vrt/api';
import { NotificationSnackBarService, dayjs } from '@vrt/utils';
import { VenuesState } from '@vrt/venue-api';
import { Observable, from } from 'rxjs';
import { tap } from 'rxjs/operators';
import {
    AddCurrentUserToAnEvent,
    AddEvent,
    AddUserToAnEvent,
    ChangeFilteredState,
    ChangeSelectedEventRegState,
    EventSaved,
    FindRegistration,
    LoadAvailableEvents,
    LoadEventById,
    LoadFutureEvents,
    LoadPastEvents,
    RegistrationChanged,
    RemoveCurrentUserFromEvent,
    RemoveEvent,
    RemoveEventSaved,
    RemoveUserFromEvent,
    SetEvent,
    SetEventFromStore,
    SetEvents,
    SetRegistrations,
    UpdateEvent,
    UpdateEventInState,
} from './events.actions';

export interface EventsStateModel {
    appEventPagedResult: AppEventPagedResult;
    appRegistrationPagedResult: AppRegistrationPagedResult;
    isFiltered: boolean;
    filterValue?: string;
    selectedEventId?: number;
    selectedEvent?: AppEvent;
}

export const DEFAULT_EVENT_STATE = {
    appEventPagedResult: { items: [], metadata: {} },
    appRegistrationPagedResult: { items: [], metadata: {} },
    isFiltered: false,
    filterValue: undefined,
    selectedEvent: undefined,
    selectedEventId: undefined,
} as EventsStateModel;

@State<EventsStateModel>({
    name: 'appEvents',
    defaults: DEFAULT_EVENT_STATE,
})
@Injectable()
export class EventsState {
    constructor(
        private eventsService: EventsService,
        private store: Store,
        private registrationsService: EventRegistrationsService,
        private notificationService: NotificationSnackBarService,
        protected router: Router,
        protected ngZone: NgZone,
    ) {}

    @Selector([EventsState, VenuesState?.venues])
    static events({ appEventPagedResult: { items }, isFiltered, filterValue }: EventsStateModel, venues: AppVenue[]): AppEvent[] {
        return (
            items
                ?.filter((vrtEvent: AppEvent) => new Date(vrtEvent.date) >= new Date())
                .filter((vrtEvent: AppEvent) =>
                    !isFiltered ? vrtEvent : venues?.find((vrtVenue: AppVenue) => vrtVenue.id === vrtEvent.venueId)?.name === filterValue,
                )
                .sort((a, b) => {
                    return new Date(a.date).getTime() - new Date(b.date).getTime();
                }) ?? []
        );
    }

    @Selector([EventsState])
    static selectedEvent({ selectedEvent }: EventsStateModel): AppEvent | undefined {
        return selectedEvent;
    }

    @Selector([EventsState])
    static selectedVrtEventFromStore({ appEventPagedResult: { items }, selectedEventId }: EventsStateModel): AppEvent | undefined {
        return items?.find((value) => value.id === selectedEventId);
    }

    @Selector([EventsState])
    static filterValue({ filterValue }: EventsStateModel): string | undefined {
        return sessionStorage.getItem('filter') ?? filterValue;
    }

    @Selector([EventsState])
    static inactiveEvents({ appEventPagedResult: { items } }: EventsStateModel): AppEvent[] {
        return items?.filter((value) => dayjs(value.date).isBefore(dayjs(new Date()))) ?? [];
    }

    @Selector([EventsState])
    static isFiltered({ isFiltered }: EventsStateModel): boolean {
        return !!sessionStorage.getItem('filter') ?? isFiltered;
    }

    @Selector([EventsState])
    static eventMetadata({ appEventPagedResult: { metadata } }: EventsStateModel): Metadata | undefined {
        return metadata;
    }

    @Selector([EventsState])
    static registrations({ appRegistrationPagedResult: { items } }: EventsStateModel): AppRegistration[] | undefined | null {
        return items;
    }

    @Selector([EventsState])
    static registrationMetadata({ appRegistrationPagedResult: { metadata } }: EventsStateModel): Metadata | undefined {
        return metadata;
    }

    @Action(AddEvent)
    add({ dispatch }: StateContext<EventsStateModel>, { payload }: AddEvent) {
        const { eventModel } = payload;
        return this.eventsService.createEvent(eventModel).pipe(tap((response) => dispatch(new EventSaved({ appEvent: response }))));
    }

    @Action(AddUserToAnEvent)
    addUserToAnEvent({ dispatch }: StateContext<EventsStateModel>, { payload }: AddUserToAnEvent) {
        const { userId, eventId } = payload;
        return this.registrationsService.addRegistration({ userId, eventId }).pipe(
            tap((response: AppRegistration) => {
                dispatch(
                    new RegistrationChanged({
                        appEventId: eventId,
                        appRegistration: response,
                        removed: false,
                        userId,
                    }),
                );
                dispatch(
                    new ChangeSelectedEventRegState({
                        registrationStatus: response.status ?? RegistrationStatus.NUMBER_0,
                    }),
                );
            }),
        );
    }

    @Action(AddCurrentUserToAnEvent)
    addCurrentUserToAnEvent({ dispatch }: StateContext<EventsStateModel>, { payload }: AddCurrentUserToAnEvent) {
        const { eventId } = payload;
        return this.registrationsService.registerToEvent(eventId).pipe(
            tap((response) => {
                dispatch(
                    new RegistrationChanged({
                        appEventId: eventId,
                        appRegistration: response,
                        removed: false,
                        userId: response.userId,
                    }),
                );
                dispatch(
                    new ChangeSelectedEventRegState({
                        registrationStatus: response.status !== undefined ? response.status : RegistrationStatus.NUMBER_0,
                    }),
                );
            }),
        );
    }

    @Action(ChangeSelectedEventRegState)
    changeSelectedEventRegState({ getState, patchState }: StateContext<EventsStateModel>, { payload }: ChangeSelectedEventRegState) {
        const event = { ...getState().selectedEvent } as AppEvent;
        if (event) {
            event.registrationStatus = payload.registrationStatus;
            patchState({
                selectedEvent: { ...event },
            });
        }
    }

    @Action(EventSaved)
    eventSaved({ setState }: StateContext<EventsStateModel>, { payload }: EventSaved) {
        const { appEvent } = payload;
        setState(
            patch<EventsStateModel>({
                appEventPagedResult: patch<AppEventPagedResult>({
                    items: append<AppEvent>([appEvent]) as unknown as AppEvent[],
                }),
            }),
        );
        this.notificationService.showSuccess('Eintrag gespeichert');
        return this.navigate('/events', false);
    }

    @Action(FindRegistration)
    findRegistration({ dispatch }: StateContext<EventsStateModel>, { payload }: FindRegistration) {
        const { eventId, filter, sortBy, sortOrder, status, page } = payload.registrationsApiParameters;
        return this.registrationsService
            .findRegistrationsByEvent(eventId, sortBy, status, page, this.store.selectSnapshot(PaginatorState.pageSize), filter, sortOrder)
            .pipe(
                tap((appRegistrationPagedResult) => {
                    dispatch(new SetRegistrations({ appRegistrationPagedResult }));
                }),
            );
    }

    @Action(ChangeFilteredState)
    changeFilteredState({ patchState }: StateContext<EventsStateModel>, { payload }: ChangeFilteredState) {
        const { isFiltered, filterValue } = payload;
        sessionStorage.setItem('filter', filterValue ?? '');
        patchState({ isFiltered, filterValue });
    }

    @Action(LoadAvailableEvents)
    loadAvailableEvents({ dispatch }: StateContext<EventsStateModel>, { payload }: LoadAvailableEvents) {
        const { filter, page, sortBy, sortOrder } = payload.apiCallParameters;
        return this.eventsService
            .findAvailableEvents(sortBy, page, this.store.selectSnapshot(PaginatorState.pageSize), filter, sortOrder)
            .pipe(tap((response: AppEventPagedResult) => dispatch(new SetEvents({ response }))));
    }

    @Action(LoadEventById)
    loadEventById({ dispatch }: StateContext<EventsStateModel>, { payload }: LoadEventById) {
        const { id } = payload;
        return this.eventsService.getEvent(id).pipe(tap((appEvent: AppEvent) => dispatch(new SetEvent({ appEvent }))));
    }

    @Action(LoadFutureEvents)
    loadFutureEvents({ dispatch }: StateContext<EventsStateModel>, { payload }: LoadFutureEvents) {
        const { filter, page, sortBy, sortOrder } = payload.apiCallParameters;
        return this.eventsService
            .findFutureEvents(sortBy, page, this.store.selectSnapshot(PaginatorState.pageSize), filter, sortOrder)
            .pipe(tap((response: AppEventPagedResult) => dispatch(new SetEvents({ response }))));
    }

    @Action(LoadPastEvents)
    loadPastEvents({ dispatch }: StateContext<EventsStateModel>, { payload }: LoadPastEvents) {
        const { filter, page, sortBy, sortOrder } = payload;
        return this.eventsService
            .findPastEvents(sortOrder, sortBy, page, this.store.selectSnapshot(PaginatorState.pageSize), filter)
            .pipe(tap((response: AppEventPagedResult) => dispatch(new SetEvents({ response }))));
    }

    @Action(RegistrationChanged)
    registrationChanged({ getState, setState }: StateContext<EventsStateModel>, { payload }: RegistrationChanged) {
        const { appRegistration, userId, appEventId, removed } = payload;
        const stateEvents = getState().appEventPagedResult.items as unknown as AppEvent[];
        const registrationMeta = getState().appRegistrationPagedResult.metadata as unknown as Metadata;
        const eventToUpdate = stateEvents.find(({ id }) => id === appEventId);

        const propertiesToUpdate = {
            registrationCount: Math.max(0, (eventToUpdate?.registrationCount ?? 0) + (removed ? -1 : 1)),
            registrationStatus: appRegistration.status ? appRegistration.status : RegistrationStatus.NUMBER_0,
        };
        setState(
            patch<EventsStateModel>({
                appEventPagedResult: patch<AppEventPagedResult>({
                    items: updateItem<AppEvent>(
                        ({ id }: AppEvent) => id === appEventId,
                        patch<AppEvent>(propertiesToUpdate),
                    ) as unknown as AppEvent[],
                }),

                appRegistrationPagedResult: patch<AppRegistrationPagedResult>({
                    items: (removed
                        ? removeItem<AppRegistration>((item) => item.userId === userId)
                        : append([appRegistration])) as unknown as AppRegistration[],
                    metadata: patch<Metadata>({
                        totalCount: Math.max(0, (registrationMeta?.totalCount ?? 0) + (removed ? -1 : 1)),
                    }),
                }),
            }),
        );

        if (removed) {
            this.showSignOutMsg();
        } else {
            this.showRegistrationMsg(appRegistration.status);
        }

        if (getState().selectedEvent) {
            setState(
                patch<EventsStateModel>({
                    selectedEvent: patch<AppEvent>(propertiesToUpdate),
                }),
            );
        }
    }

    @Action(RemoveCurrentUserFromEvent)
    removeCurrentUserFromEvent({ dispatch }: StateContext<EventsStateModel>, { payload }: RemoveCurrentUserFromEvent) {
        const { userId, eventId } = payload;
        return this.registrationsService.unregisterFromEvent(eventId).pipe(
            tap(() =>
                dispatch(
                    new RegistrationChanged({
                        appRegistration: {},
                        userId,
                        appEventId: eventId,
                        removed: true,
                    }),
                ),
            ),
        );
    }

    @Action(RemoveUserFromEvent)
    removeUserFromEventState({ dispatch }: StateContext<EventsStateModel>, { payload }: RemoveUserFromEvent) {
        const { userId, eventId } = payload;
        return this.registrationsService.deleteRegistration({ eventId, userId }).pipe(
            tap((appRegistration: AppRegistration) =>
                dispatch(
                    new RegistrationChanged({
                        appRegistration,
                        userId,
                        appEventId: eventId,
                        removed: true,
                    }),
                ),
            ),
        );
    }

    @Action(RemoveEvent)
    removeEvent({ dispatch }: StateContext<EventsStateModel>, { payload }: RemoveEvent) {
        const { id } = payload;
        return this.eventsService.deleteEvent(id).pipe(
            tap(() => {
                dispatch(new RemoveEventSaved({ id }));
            }),
        );
    }

    @Action(RemoveEventSaved)
    removeEventSaved({ getState, setState }: StateContext<EventsStateModel>, { payload }: RemoveEventSaved) {
        const { id } = payload;
        setState(
            patch<EventsStateModel>({
                appEventPagedResult: patch<AppEventPagedResult>({
                    items: removeItem<AppEvent>((item: AppEvent) => item.id === id) as unknown as AppEvent[],
                    metadata: patch<Metadata>({
                        totalCount: Math.max(0, (getState().appEventPagedResult.metadata?.totalCount ?? 0) - 1),
                    }) as unknown as Metadata,
                }),
            }),
        );
        this.notificationService.showSuccess('Eintrag gelöscht');
    }

    @Action(SetEvents)
    setEvents({ patchState }: StateContext<EventsStateModel>, { payload }: SetEvents) {
        patchState({
            appEventPagedResult: { ...payload.response },
        });
    }

    @Action(SetEvent)
    setEvent({ patchState }: StateContext<EventsStateModel>, { payload }: SetEvent) {
        patchState({
            selectedEvent: payload.appEvent ? { ...payload.appEvent } : undefined,
        });
    }

    @Action(SetEventFromStore)
    setEventFromStore({ patchState, getState, dispatch }: StateContext<EventsStateModel>, { payload }: SetEventFromStore) {
        const { id } = payload;
        const appEvent = getState().appEventPagedResult.items?.find((value) => value.id === id);
        if (appEvent) {
            dispatch(new SetEvent({ appEvent }));
        }
        patchState({ selectedEventId: id });
    }

    @Action(SetRegistrations)
    setRegistrations({ patchState }: StateContext<EventsStateModel>, { payload }: SetRegistrations) {
        const { appRegistrationPagedResult } = payload;
        patchState({
            appRegistrationPagedResult: { ...appRegistrationPagedResult },
        });
    }

    @Action(UpdateEvent)
    updateEvent({ dispatch }: StateContext<EventsStateModel>, { payload }: UpdateEvent) {
        const { appEvent } = payload;
        return this.eventsService
            .updateEvent(appEvent.id as number, { ...appEvent } as EventModel)
            .pipe(tap((response: AppEvent) => dispatch(new UpdateEventInState({ appEvent: response }))));
    }

    @Action(UpdateEventInState)
    updateEventInState({ setState }: StateContext<EventsStateModel>, { payload }: UpdateEventInState) {
        const { appEvent } = payload;

        setState(
            patch<EventsStateModel>({
                appEventPagedResult: patch<AppEventPagedResult>({
                    items: updateItem<AppEvent>((event: AppEvent) => event.id === appEvent.id, appEvent) as unknown as AppEvent[],
                }),
            }),
        );
        this.notificationService.showSuccess('Eintrag gespeichert');
        return this.navigate('/events', false);
    }

    protected navigate(targetPath: string, preserveParams = true): Observable<boolean> {
        return this.ngZone.run(() => {
            if (preserveParams) {
                return from(
                    this.router.navigate([targetPath], {
                        queryParamsHandling: 'preserve',
                    }),
                );
            } else {
                return from(this.router.navigate([targetPath]));
            }
        });
    }

    private showRegistrationMsg(registrationStatus: RegistrationStatus | undefined): void {
        this.notificationService.showSuccess(registrationStatus === 1 ? 'Angemeldet.' : 'Angemeldet. Du stehst auf der Warteliste.');
    }

    private showSignOutMsg(): void {
        this.notificationService.showSuccess('Abgemeldet.');
    }
}
