import { RegistrationStatus } from '../openapi';

export interface RegistrationsApiParameters {
    eventId: number;
    page: number | undefined;
    pageSize: number | undefined;
    status: RegistrationStatus | undefined;
    filter: string | undefined;
    sortBy: string | undefined;
    sortOrder: string | undefined;
}
