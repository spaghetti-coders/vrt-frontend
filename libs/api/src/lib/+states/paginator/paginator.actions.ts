export class SetPageSize {
    static readonly type = '[Paginator] Set page size';

    constructor(public payload: number) {}
}
