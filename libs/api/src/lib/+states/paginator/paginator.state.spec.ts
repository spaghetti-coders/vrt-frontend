import { TestBed } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import * as utils from '@vrt/utils';
import { SetPageSize } from './paginator.actions';
import { PaginatorState } from './paginator.state';

describe('PaginatorState', () => {
    let store: Store;

    const setup = (pageSize: number | undefined) => {
        TestBed.configureTestingModule({
            imports: [
                NgxsModule.forRoot([PaginatorState], {
                    developmentMode: true,
                    selectorOptions: { injectContainerState: false },
                }),
            ],
            teardown: { destroyAfterEach: false },
        });
        if (pageSize) {
            localStorage.setItem('pageSize', `${pageSize}`);
        }
        store = TestBed.inject(Store);
    };

    it('should load page size from localStorage', () => {
        setup(25);
        expect(store.selectSnapshot(PaginatorState.pageSize)).toEqual(25);
    });

    it('should set page size and store it in localStorage', () => {
        setup(10);
        const savePageSizeSpy = jest.spyOn(utils, 'savePageSize');
        store.dispatch(new SetPageSize(10));

        expect(savePageSizeSpy).toHaveBeenCalledTimes(1);
        expect(store.selectSnapshot(PaginatorState.pageSize)).toEqual(10);
    });

    it('should return default value', () => {
        setup(0);
        localStorage.clear();
        expect(store.selectSnapshot(PaginatorState.pageSize)).toEqual(10);
    });
});
