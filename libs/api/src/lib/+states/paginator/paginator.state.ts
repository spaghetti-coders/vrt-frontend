import { Injectable } from '@angular/core';
import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { patch } from '@ngxs/store/operators';
import { savePageSize } from '@vrt/utils';
import { SetPageSize } from './paginator.actions';

export interface PaginatorStateModel {
    pageSize: number;
}

const DEFAULT_PAGINATOR_STATE: PaginatorStateModel = {
    pageSize: 10,
};

@State<PaginatorStateModel>({
    name: 'paginator',
    defaults: DEFAULT_PAGINATOR_STATE,
})
@Injectable()
export class PaginatorState implements NgxsOnInit {
    @Selector([PaginatorState])
    static pageSize({ pageSize }: PaginatorStateModel): number {
        return pageSize;
    }

    ngxsOnInit({ setState }: StateContext<PaginatorStateModel>): void {
        const pageSize = Number(localStorage.getItem('pageSize')) ?? 10;
        setState(
            patch<PaginatorStateModel>({
                pageSize: pageSize > 1 ? pageSize : 10,
            }),
        );
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    @Action(SetPageSize)
    setPageSize({ setState }: StateContext<PaginatorStateModel>, { payload }: SetPageSize) {
        savePageSize(payload);
        setState(
            patch<PaginatorStateModel>({
                pageSize: payload,
            }),
        );
    }
}
