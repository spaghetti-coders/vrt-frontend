/**
 * VRT API v1.1.0
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1.1.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { HttpClient, HttpContext, HttpEvent, HttpHeaders, HttpParameterCodec, HttpParams, HttpResponse } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { Observable } from 'rxjs';
import { Configuration } from '../configuration';
import { CustomHttpParameterCodec } from '../encoder';

// @ts-ignore
import { AppLogEntryPagedResult } from '../model/appLogEntryPagedResult';
// @ts-ignore
// @ts-ignore
import { BASE_PATH } from '../variables';
import { AdminLogServiceInterface } from './adminLog.serviceInterface';

@Injectable({
    providedIn: 'root',
})
export class AdminLogService implements AdminLogServiceInterface {
    protected basePath = 'http://localhost';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();
    public encoder: HttpParameterCodec;

    constructor(
        protected httpClient: HttpClient,
        @Optional() @Inject(BASE_PATH) basePath: string,
        @Optional() configuration: Configuration,
    ) {
        if (configuration) {
            this.configuration = configuration;
        }
        if (typeof this.configuration.basePath !== 'string') {
            if (typeof basePath !== 'string') {
                basePath = this.basePath;
            }
            this.configuration.basePath = basePath;
        }
        this.encoder = this.configuration.encoder || new CustomHttpParameterCodec();
    }

    private addToHttpParams(httpParams: HttpParams, value: any, key?: string): HttpParams {
        if (typeof value === 'object' && value instanceof Date === false) {
            httpParams = this.addToHttpParamsRecursive(httpParams, value);
        } else {
            httpParams = this.addToHttpParamsRecursive(httpParams, value, key);
        }
        return httpParams;
    }

    private addToHttpParamsRecursive(httpParams: HttpParams, value?: any, key?: string): HttpParams {
        if (value == null) {
            return httpParams;
        }

        if (typeof value === 'object') {
            if (Array.isArray(value)) {
                (value as any[]).forEach((elem) => (httpParams = this.addToHttpParamsRecursive(httpParams, elem, key)));
            } else if (value instanceof Date) {
                if (key != null) {
                    httpParams = httpParams.append(key, (value as Date).toISOString().substr(0, 10));
                } else {
                    throw Error('key may not be null if value is Date');
                }
            } else {
                Object.keys(value).forEach(
                    (k) => (httpParams = this.addToHttpParamsRecursive(httpParams, value[k], key != null ? `${key}.${k}` : k)),
                );
            }
        } else if (key != null) {
            httpParams = httpParams.append(key, value);
        } else {
            throw Error('key may not be null if value is not object or array');
        }
        return httpParams;
    }

    /**
     * Returns a list of log entries matching the specified filter.
     * @param sortBy The column/property by which the results will be sorted by.
     * @param sortOrder The direction in which results will be sorted by (\&#39;asc\&#39; or \&#39;desc\&#39;)
     * @param page The page that is being requested.
     * @param pageSize The maximum number of entries per page.
     * @param filter A filter/search string by which the result is being filtered.
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findLogEntries(
        sortBy?: string,
        sortOrder?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        observe?: 'body',
        reportProgress?: boolean,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<AppLogEntryPagedResult>;
    public findLogEntries(
        sortBy?: string,
        sortOrder?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        observe?: 'response',
        reportProgress?: boolean,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<HttpResponse<AppLogEntryPagedResult>>;
    public findLogEntries(
        sortBy?: string,
        sortOrder?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        observe?: 'events',
        reportProgress?: boolean,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<HttpEvent<AppLogEntryPagedResult>>;
    public findLogEntries(
        sortBy?: string,
        sortOrder?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        observe: any = 'body',
        reportProgress: boolean = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        let localVarQueryParameters = new HttpParams({ encoder: this.encoder });
        if (sortBy !== undefined && sortBy !== null) {
            localVarQueryParameters = this.addToHttpParams(localVarQueryParameters, <any>sortBy, 'SortBy');
        }
        if (sortOrder !== undefined && sortOrder !== null) {
            localVarQueryParameters = this.addToHttpParams(localVarQueryParameters, <any>sortOrder, 'SortOrder');
        }
        if (page !== undefined && page !== null) {
            localVarQueryParameters = this.addToHttpParams(localVarQueryParameters, <any>page, 'Page');
        }
        if (pageSize !== undefined && pageSize !== null) {
            localVarQueryParameters = this.addToHttpParams(localVarQueryParameters, <any>pageSize, 'PageSize');
        }
        if (filter !== undefined && filter !== null) {
            localVarQueryParameters = this.addToHttpParams(localVarQueryParameters, <any>filter, 'Filter');
        }

        let localVarHeaders = this.defaultHeaders;

        let localVarCredential: string | undefined;
        // authentication (Bearer) required
        localVarCredential = this.configuration.lookupCredential('Bearer');
        if (localVarCredential) {
            localVarHeaders = localVarHeaders.set('Authorization', 'Bearer ' + localVarCredential);
        }

        let localVarHttpHeaderAcceptSelected: string | undefined = options && options.httpHeaderAccept;
        if (localVarHttpHeaderAcceptSelected === undefined) {
            // to determine the Accept header
            const httpHeaderAccepts: string[] = ['text/plain', 'application/json', 'text/json'];
            localVarHttpHeaderAcceptSelected = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        }
        if (localVarHttpHeaderAcceptSelected !== undefined) {
            localVarHeaders = localVarHeaders.set('Accept', localVarHttpHeaderAcceptSelected);
        }

        let localVarHttpContext: HttpContext | undefined = options && options.context;
        if (localVarHttpContext === undefined) {
            localVarHttpContext = new HttpContext();
        }

        let responseType_: 'text' | 'json' | 'blob' = 'json';
        if (localVarHttpHeaderAcceptSelected) {
            if (localVarHttpHeaderAcceptSelected.startsWith('text')) {
                responseType_ = 'text';
            } else if (this.configuration.isJsonMime(localVarHttpHeaderAcceptSelected)) {
                responseType_ = 'json';
            } else {
                responseType_ = 'blob';
            }
        }

        return this.httpClient.get<AppLogEntryPagedResult>(`${this.configuration.basePath}/api/AdminLog`, {
            context: localVarHttpContext,
            params: localVarQueryParameters,
            responseType: <any>responseType_,
            withCredentials: this.configuration.withCredentials,
            headers: localVarHeaders,
            observe: observe,
            reportProgress: reportProgress,
        });
    }
}
