/**
 * VRT API v1.1.0
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1.1.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Configuration } from '../configuration';
import { AppEvent, AppEventPagedResult, EventModel } from '../model/models';

export interface EventsServiceInterface {
    defaultHeaders: HttpHeaders;
    configuration: Configuration;

    /**
     * Creates a new event.
     *
     * @param eventModel The event\&#39;s properties
     */
    createEvent(eventModel?: EventModel, extraHttpRequestParams?: any): Observable<AppEvent>;

    /**
     * Deletes the event with the specified ID from the application.
     *
     * @param id ID of the event to delete
     */
    deleteEvent(id: number, extraHttpRequestParams?: any): Observable<{}>;

    /**
     * Exports the specified event\&#39;s details (name, description, location, and participants) as text file.
     *
     * @param id ID of the event to export
     */
    exportEventDetails(id: number, extraHttpRequestParams?: any): Observable<Blob>;

    /**
     * Returns a list of events that are open for registration and that are matching the specified filter.
     *
     * @param sortBy The column/property by which the results will be sorted by.
     * @param page The page that is being requested.
     * @param pageSize The maximum number of entries per page.
     * @param filter A filter/search string by which the result is being filtered.
     * @param sortOrder The direction in which results will be sorted by (\&#39;asc\&#39; or \&#39;desc\&#39;)
     */
    findAvailableEvents(
        sortBy?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        sortOrder?: string,
        extraHttpRequestParams?: any,
    ): Observable<AppEventPagedResult>;

    /**
     * Returns a list of events matching the specified filter.
     *
     * @param sortBy The column/property by which the results will be sorted by.
     * @param page The page that is being requested.
     * @param pageSize The maximum number of entries per page.
     * @param filter A filter/search string by which the result is being filtered.
     * @param sortOrder The direction in which results will be sorted by (\&#39;asc\&#39; or \&#39;desc\&#39;)
     */
    findEvents(
        sortBy?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        sortOrder?: string,
        extraHttpRequestParams?: any,
    ): Observable<AppEventPagedResult>;

    /**
     * Returns a list of future events that are matching the specified filter.
     *
     * @param sortBy The column/property by which the results will be sorted by.
     * @param page The page that is being requested.
     * @param pageSize The maximum number of entries per page.
     * @param filter A filter/search string by which the result is being filtered.
     * @param sortOrder The direction in which results will be sorted by (\&#39;asc\&#39; or \&#39;desc\&#39;)
     */
    findFutureEvents(
        sortBy?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        sortOrder?: string,
        extraHttpRequestParams?: any,
    ): Observable<AppEventPagedResult>;

    /**
     * Returns a list of events that have already taken place and that are matching the specified filter.
     *
     * @param sortOrder The direction in which results will be sorted by (\&#39;asc\&#39; or \&#39;desc\&#39;)
     * @param sortBy The column/property by which the results will be sorted by.
     * @param page The page that is being requested.
     * @param pageSize The maximum number of entries per page.
     * @param filter A filter/search string by which the result is being filtered.
     */
    findPastEvents(
        sortOrder?: string,
        sortBy?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        extraHttpRequestParams?: any,
    ): Observable<AppEventPagedResult>;

    /**
     * Returns the event with the specified ID.
     *
     * @param id ID of the event to return
     */
    getEvent(id: number, extraHttpRequestParams?: any): Observable<AppEvent>;

    /**
     * Updates the event with the specified ID.
     *
     * @param id ID of the event to update
     * @param eventModel The updated event properties
     */
    updateEvent(id: number, eventModel?: EventModel, extraHttpRequestParams?: any): Observable<AppEvent>;
}
