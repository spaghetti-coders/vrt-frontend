export * from './account.service';
import { AccountService } from './account.service';
import { AdminLogService } from './adminLog.service';
import { EventRegistrationsService } from './eventRegistrations.service';
import { EventsService } from './events.service';
import { MailTemplatesService } from './mailTemplates.service';
import { NotificationsService } from './notifications.service';
import { UsersService } from './users.service';
import { VenuesService } from './venues.service';

export * from './account.serviceInterface';
export * from './adminLog.service';
export * from './adminLog.serviceInterface';
export * from './eventRegistrations.service';
export * from './eventRegistrations.serviceInterface';
export * from './events.service';
export * from './events.serviceInterface';
export * from './mailTemplates.service';
export * from './mailTemplates.serviceInterface';
export * from './notifications.service';
export * from './notifications.serviceInterface';
export * from './users.service';
export * from './users.serviceInterface';
export * from './venues.service';
export * from './venues.serviceInterface';
export const APIS = [
    AccountService,
    AdminLogService,
    EventRegistrationsService,
    EventsService,
    MailTemplatesService,
    NotificationsService,
    UsersService,
    VenuesService,
];
