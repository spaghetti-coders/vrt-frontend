/**
 * VRT API v1.1.0
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1.1.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Configuration } from '../configuration';

import {
    AppUser,
    LoginModel,
    LoginResult,
    PasswordResetModel,
    RecoveryTokenRequest,
    RefreshTokenRequest,
    RegistrationModel,
    UpdateAccountModel,
    UpdatePasswordModel,
} from '../model/models';

export interface AccountServiceInterface {
    defaultHeaders: HttpHeaders;
    configuration: Configuration;

    /**
     * Deletes and de-authenticates the currently logged in user\&#39;s account.
     *
     */
    deleteAccount(extraHttpRequestParams?: any): Observable<{}>;

    /**
     * Returns the account of the currently authenticated user.
     *
     */
    getCurrentUser(extraHttpRequestParams?: any): Observable<AppUser>;

    /**
     * Tries to authenticate the user with the specified credentials.
     *
     * @param loginModel The user login model
     */
    login(loginModel?: LoginModel, extraHttpRequestParams?: any): Observable<LoginResult>;

    /**
     * De-authenticate the currently logged in user.
     *
     */
    logout(extraHttpRequestParams?: any): Observable<{}>;

    /**
     * Refreshes the user\&#39;s JWT token pair.
     *
     * @param refreshTokenRequest The refresh token model
     */
    refreshToken(refreshTokenRequest?: RefreshTokenRequest, extraHttpRequestParams?: any): Observable<LoginResult>;

    /**
     * Tries to register a new user with the specified attributes.
     *
     * @param registrationModel The user registration model
     */
    register(registrationModel?: RegistrationModel, extraHttpRequestParams?: any): Observable<{}>;

    /**
     * Generates an account recovery token that can be used to reset the specified user\&#39;s password and notifies the user by mail.
     *
     * @param recoveryTokenRequest The recovery token request model
     */
    requestRecoveryToken(recoveryTokenRequest?: RecoveryTokenRequest, extraHttpRequestParams?: any): Observable<{}>;

    /**
     * Reset the password of a user, using the provided account recovery token.
     *
     * @param passwordResetModel The recovery token and new password
     */
    resetPassword(passwordResetModel?: PasswordResetModel, extraHttpRequestParams?: any): Observable<{}>;

    /**
     * Updates the user\&#39;s account with the provided properties.
     *
     * @param updateAccountModel The user\&#39;s updated properties
     */
    upateAccount(updateAccountModel?: UpdateAccountModel, extraHttpRequestParams?: any): Observable<AppUser>;

    /**
     * Updates the user\&#39;s password.
     *
     * @param updatePasswordModel The user\&#39;s updated password
     */
    upatePassword(updatePasswordModel?: UpdatePasswordModel, extraHttpRequestParams?: any): Observable<{}>;
}
