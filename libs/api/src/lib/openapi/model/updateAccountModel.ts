/**
 * VRT API v1.1.0
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1.1.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

/**
 * The model used for updating a user\'s account.
 */
export interface UpdateAccountModel {
    /**
     * The user\'s display name.
     */
    displayName: string;
    /**
     * The user\'s email address (optional)
     */
    email?: string | null;
    /**
     * Boolean indicating whether the user wants to receive notifications from the application.
     */
    receiveNotifications?: boolean;
}
