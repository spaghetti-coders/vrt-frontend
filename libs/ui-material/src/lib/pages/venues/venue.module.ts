import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { TranslocoModule } from '@ngneat/transloco';
import { NgxsModule } from '@ngxs/store';
import { ConfirmDialogModule, NavigationModule, PagingModule } from '../../atomic/index';
import { VenueCreateViewComponent } from './components/venue-create-view/venue-create-view.component';
import { VenueDetailViewComponent } from './components/venue-detail-view/venue-detail-view.component';
import { VenueEditViewComponent } from './components/venue-edit-view/venue-edit-view.component';
import { VenueOverviewComponent } from './components/venue-overview/venue-overview.component';
import { VenueHelperService } from './services/venue-helper.service';

import { VenuesState } from '@vrt/venue-api';
import { VenueRoutingModule } from './venue-routing.module';

@NgModule({
    declarations: [VenueOverviewComponent, VenueEditViewComponent, VenueDetailViewComponent, VenueCreateViewComponent],
    imports: [
        CommonModule,
        ConfirmDialogModule,
        FormsModule,
        MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatTableModule,
        NavigationModule,
        NgxsModule.forFeature([VenuesState]),
        PagingModule,
        ReactiveFormsModule,
        TranslocoModule,
        VenueRoutingModule,
    ],
    providers: [VenueHelperService],
})
export class VenueModule {}
