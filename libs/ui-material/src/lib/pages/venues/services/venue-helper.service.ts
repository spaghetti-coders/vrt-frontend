import { Injectable } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AppVenue } from '@vrt/api';

@Injectable({
    providedIn: 'root',
})
export class VenueHelperService {
    constructor(private formBuilder: UntypedFormBuilder) {}

    initVenueForm(venue: AppVenue): UntypedFormGroup {
        return this.formBuilder.group({
            id: [venue.id],
            name: [venue.name, [Validators.required, Validators.minLength(2)]],
            address: [venue.address, Validators.required],
            capacity: [venue.capacity, [Validators.required, Validators.min(1)]],
            isActive: [venue.isActive ? venue.isActive : false],
            colorCode: [venue.colorCode ? venue.colorCode : '#FFFFFF', [Validators.required]],
        });
    }
}
