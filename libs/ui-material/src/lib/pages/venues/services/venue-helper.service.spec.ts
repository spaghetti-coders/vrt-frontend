import { TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';

import { VenueHelperService } from './venue-helper.service';

describe('VenueHelperService', () => {
    let service: VenueHelperService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [{ provide: UntypedFormBuilder, useValue: jest.fn() }],
            teardown: { destroyAfterEach: false },
        });
        service = TestBed.inject(VenueHelperService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
