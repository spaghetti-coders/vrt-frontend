import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavigationComponent } from '../../atomic/navigation/navigation.component';
import { VenueCreateViewComponent } from './components/venue-create-view/venue-create-view.component';
import { VenueEditViewComponent } from './components/venue-edit-view/venue-edit-view.component';
import { VenueOverviewComponent } from './components/venue-overview/venue-overview.component';

const routes: Routes = [
    {
        path: '',
        component: NavigationComponent,
        children: [
            {
                path: '',
                component: VenueOverviewComponent,
            },
            {
                path: 'create',
                component: VenueCreateViewComponent,
            },
            {
                path: 'edit/:id',
                component: VenueEditViewComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class VenueRoutingModule {}
