import { BreakpointObserver } from '@angular/cdk/layout';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule, Store } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { createSpyFromClass, provideAutoSpy, Spy } from 'jest-auto-spies';
import { MockComponent } from 'ng-mocks';
import { of } from 'rxjs';
import { ConfirmDialogComponent } from '../../../../atomic/confirm-dialog/confirm-dialog.component';
import { PagingComponent } from '../../../../atomic/paging/paging.component';
import { VenueDetailViewComponent } from '../venue-detail-view/venue-detail-view.component';

import { RouterModule } from '@angular/router';
import { METADATA_EXAMPLE, VENUE_EXAMPLE } from '@vrt/testing';
import { LoadVenues, UpdateVenue } from '@vrt/venue-api';
import { VenueOverviewComponent } from './venue-overview.component';

describe('VenueOverviewComponent', () => {
    let component: VenueOverviewComponent;
    let fixture: ComponentFixture<VenueOverviewComponent>;
    let store: Spy<Store>;
    let dialog: Spy<MatDialog>;
    let breakpointObserver: BreakpointObserver;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                NoopAnimationsModule,
                getTranslocoModule(),
                RouterModule.forRoot([]),
                HttpClientTestingModule,
                NgxsModule.forRoot([], { developmentMode: true }),
            ],
            declarations: [VenueOverviewComponent, MockComponent(PagingComponent), MockComponent(ConfirmDialogComponent)],
            providers: [
                provideAutoSpy(NotificationSnackBarService),
                { provide: IS_DEMO_BUILD, useValue: false },
                {
                    provide: MatDialog,
                    useValue: createSpyFromClass(MatDialog, {
                        methodsToSpyOn: ['open'],
                    }),
                },
                {
                    provide: Store,
                    useValue: createSpyFromClass(Store, {
                        methodsToSpyOn: ['select'],
                    }),
                },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(VenueOverviewComponent);
        component = fixture.componentInstance;
        store = TestBed.inject<any>(Store);
        dialog = TestBed.inject<any>(MatDialog);
        breakpointObserver = TestBed.inject(BreakpointObserver);
        breakpointObserver.observe = jest.fn().mockReturnValue(of({ matches: false }));
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        component.metadata$ = of(METADATA_EXAMPLE);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should delete venue', () => {
        dialog.open.mockReturnValue({
            afterClosed: () => of(true),
        } as any);
        component.deleteVenue(VENUE_EXAMPLE);
        expect(store.dispatch).toHaveBeenCalledWith({
            payload: {
                appVenue: {
                    address: 'Muster Str. 12',
                    capacity: 23,
                    colorCode: '#BC6BF1',
                    id: 11,
                    isActive: true,
                    name: 'Example 1',
                },
            },
        });
    });

    it('should open venue detail view', () => {
        component.openVenueDetailView(VENUE_EXAMPLE);
        expect(store.dispatch).toHaveBeenCalledWith({ payload: { venueId: 11 } });
        expect(dialog.open).toHaveBeenCalledWith(VenueDetailViewComponent);
    });

    it('should search/query venues from api', () => {
        const pageEvent: PageEvent = {
            pageIndex: 1,
            pageSize: 10,
            previousPageIndex: 0,
            length: 99,
        };
        component.search(pageEvent);
        expect(store.dispatch).toHaveBeenCalledWith(
            new LoadVenues({
                apiCallParameters: {
                    filter: undefined,
                    page: 1,
                    pageSize: 10,
                    sortBy: undefined,
                    sortOrder: undefined,
                },
            }),
        );
    });

    it('should switch active state of venue', () => {
        component.switchActiveState(VENUE_EXAMPLE);
        expect(store.dispatch).toHaveBeenCalledWith(
            new UpdateVenue({
                appVenue: { ...VENUE_EXAMPLE, isActive: !VENUE_EXAMPLE.isActive },
            }),
        );
    });

    it('should call search methode', () => {
        const search = component.search;
        component.search = jest.fn();
        const pageEvent: PageEvent = {
            pageIndex: 1,
            pageSize: 10,
            previousPageIndex: 0,
            length: 99,
        };

        component.onPageChange(pageEvent);
        expect(component.search).toHaveBeenCalledWith(pageEvent);
    });

    // TODO refactor later
    xit('should return true for isHandset ', fakeAsync(() => {
        breakpointObserver.observe = jest.fn().mockReturnValue(of({ matches: true }));
        fixture.detectChanges();
        component.isHandset$.subscribe((isHandset: boolean) => {
            expect(isHandset).toBeTruthy();
        });
        tick();
    }));
});
