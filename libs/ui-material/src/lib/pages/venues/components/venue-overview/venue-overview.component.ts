import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Select, Store } from '@ngxs/store';
import { AppVenue, Metadata, SetPageSize } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { AppConfigState } from '@vrt/common';
import { LoadVenues, RemoveVenue, SelectVenue, UpdateVenue, VenuesState } from '@vrt/venue-api';
import { combineLatest, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, shareReplay, tap } from 'rxjs/operators';
import { ConfirmDialogComponent } from '../../../../atomic/confirm-dialog/confirm-dialog.component';
import { VenueDetailViewComponent } from '../venue-detail-view/venue-detail-view.component';

@Component({
    selector: 'vrt-venue-overview',
    templateUrl: './venue-overview.component.html',
    styleUrls: ['./venue-overview.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VenueOverviewComponent implements OnInit {
    @Select(VenuesState.venues)
    venues$!: Observable<AppVenue[] | null | undefined>;

    metadata$: Observable<Metadata> = this.store
        .select(VenuesState.metadata)
        ?.pipe(filter((metaData: Metadata | undefined): metaData is Metadata => !!metaData));

    isDarkThemeSelected$: Observable<boolean> = this.store.select(AppConfigState.isDarkThemeSelected);

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map(({ matches }: BreakpointState) => matches),
        shareReplay(),
    );

    searchResult$?: Observable<any>;

    displayedColumns: string[] = ['name', 'address', 'capacity', 'isActive', 'action'];

    filter: string | undefined = undefined;

    searchForm: UntypedFormControl = new UntypedFormControl();

    data$?: Observable<any>;

    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private dialog: MatDialog,
        private breakpointObserver: BreakpointObserver,
        private authenticationService: AuthenticationService,
        private store: Store,
    ) {}

    deleteVenue(venue: AppVenue): void {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {});
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.store.dispatch(new RemoveVenue({ appVenue: venue }));
            }
        });
    }

    openVenueDetailView(venue: AppVenue): void {
        this.store.dispatch(new SelectVenue({ venueId: venue.id }));
        this.dialog.open(VenueDetailViewComponent);
    }

    switchActiveState(venue: AppVenue): void {
        const updateVenue = { ...venue, isActive: !venue.isActive };
        this.store.dispatch(new UpdateVenue({ appVenue: updateVenue }));
    }

    ngOnInit(): void {
        this.searchResult$ = this.searchForm?.valueChanges.pipe(
            debounceTime(500),
            distinctUntilChanged(),
            tap((value) => {
                this.filter = value;
                this.search();
            }),
        );

        this.data$ = combineLatest([this.venues$, this.isHandset$]);
    }

    onPageChange($event: PageEvent): void {
        this.search($event);
    }

    search(pageEvent?: PageEvent): void {
        this.store.dispatch(new SetPageSize(pageEvent?.pageSize ?? 10));
        this.store.dispatch(
            new LoadVenues({
                apiCallParameters: {
                    page: pageEvent?.pageIndex ?? undefined,
                    pageSize: pageEvent?.pageSize ?? undefined,
                    filter: this.filter,
                    sortBy: undefined,
                    sortOrder: undefined,
                },
            }),
        );
    }
}
