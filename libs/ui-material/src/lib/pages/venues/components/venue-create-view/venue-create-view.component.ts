import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AppVenue } from '@vrt/api';
import { AddVenue } from '@vrt/venue-api';
import { VenueHelperService } from '../../services/venue-helper.service';

@Component({
    selector: 'vrt-venue-create-view',
    templateUrl: './venue-create-view.component.html',
    styleUrls: ['./venue-create-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VenueCreateViewComponent implements OnInit {
    venue = {} as AppVenue;

    venueForm?: UntypedFormGroup;

    constructor(
        private venueHelperService: VenueHelperService,
        private store: Store,
    ) {}

    ngOnInit(): void {
        this.venueForm = this.venueHelperService.initVenueForm(this.venue);
    }

    submit(): void {
        if (this.venueForm) {
            this.venueForm.removeControl('id');
            this.store.dispatch(new AddVenue({ appVenue: this.venueForm.value }));
        }
    }
}
