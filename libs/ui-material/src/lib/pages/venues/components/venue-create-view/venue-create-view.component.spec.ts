import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule, Store } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { MockModule } from 'ng-mocks';

import { RouterModule } from '@angular/router';
import { VENUE_EXAMPLE } from '@vrt/testing';
import { AddVenue } from '@vrt/venue-api';
import { VenueCreateViewComponent } from './venue-create-view.component';

describe('VenueCreateViewComponent', () => {
    let component: VenueCreateViewComponent;
    let fixture: ComponentFixture<VenueCreateViewComponent>;
    let store: Store;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterModule.forRoot([]),
                getTranslocoModule(),
                FormsModule,
                MockModule(MatCheckboxModule),
                MockModule(MatCardModule),
                MockModule(MatFormFieldModule),
                ReactiveFormsModule,
                BrowserAnimationsModule,
                NgxsModule.forRoot(),
            ],
            declarations: [VenueCreateViewComponent],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(VenueCreateViewComponent);
        store = TestBed.inject(Store);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should submit form', waitForAsync(() => {
        const spy = jest.spyOn(store, 'dispatch');
        const venue = { ...VENUE_EXAMPLE };
        delete venue.id;
        component.venue = { ...venue };
        component.ngOnInit();
        component.submit();

        expect(spy).toHaveBeenCalledWith(new AddVenue({ appVenue: venue }));
    }));
});
