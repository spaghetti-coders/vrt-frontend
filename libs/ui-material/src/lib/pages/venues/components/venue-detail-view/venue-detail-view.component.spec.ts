import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';

import { VenueDetailViewComponent } from './venue-detail-view.component';

describe('VenueDetailViewComponent', () => {
    let component: VenueDetailViewComponent;
    let fixture: ComponentFixture<VenueDetailViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterTestingModule, getTranslocoModule(), NgxsModule.forRoot()],
            declarations: [VenueDetailViewComponent],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(VenueDetailViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
