import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { AppVenue } from '@vrt/api';
import { VenuesState } from '@vrt/venue-api';
import { Observable } from 'rxjs';

@Component({
    selector: 'vrt-venue-detail-view',
    templateUrl: './venue-detail-view.component.html',
    styleUrls: ['./venue-detail-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VenueDetailViewComponent {
    venue$: Observable<AppVenue | undefined> = this.store.select(VenuesState.venue);

    constructor(private store: Store) {}
}
