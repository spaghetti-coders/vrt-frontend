import { HttpClient } from '@angular/common/http';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { NgxsModule, Store } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { MockProvider } from 'ng-mocks';
import { VenueHelperService } from '../../services/venue-helper.service';

import { MatSnackBar } from '@angular/material/snack-bar';
import { VENUE_EXAMPLE } from '@vrt/testing';
import { VenuesState } from '@vrt/venue-api';
import { VenueEditViewComponent } from './venue-edit-view.component';

describe('VenueViewComponent', () => {
    let component: VenueEditViewComponent;
    let fixture: ComponentFixture<VenueEditViewComponent>;
    let store: Store;
    let venueHelperService: VenueHelperService;
    console.error = jest.fn();

    const paramMap = new Map([['id', '123']]);

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [RouterModule.forRoot([]), FormsModule, ReactiveFormsModule, getTranslocoModule(), NgxsModule.forRoot([VenuesState])],
            declarations: [VenueEditViewComponent],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            paramMap,
                        },
                    },
                },
                MockProvider(HttpClient),
                MockProvider(MatSnackBar),
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        store = TestBed.inject(Store);
        venueHelperService = TestBed.inject(VenueHelperService);
        fixture = TestBed.createComponent(VenueEditViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should dispatch select venue', fakeAsync(() => {
        const spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith({ payload: { venueId: 123 } });
    }));

    it('should submit form', fakeAsync(() => {
        const spy = jest.spyOn(store, 'dispatch');

        component.venueForm = venueHelperService.initVenueForm(VENUE_EXAMPLE);
        Object.defineProperty(component.venueForm, 'valid', {
            writable: true,
            value: true,
        });
        component.submit();

        expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith({ payload: { appVenue: VENUE_EXAMPLE } });
    }));

    it('should init venue form', fakeAsync(() => {
        store.reset({
            venues: {
                selectedVenueId: VENUE_EXAMPLE.id,
                appVenuePagedResult: {
                    items: [VENUE_EXAMPLE],
                    metadata: {},
                },
            },
        });

        component.venue$.subscribe({
            next: () => {
                expect(component.venueForm).toBeTruthy();
                expect(store.selectSnapshot(VenuesState.venue)).toEqual(VENUE_EXAMPLE);
            },
        });
    }));
});
