import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { AppVenue } from '@vrt/api';
import { SelectVenue, UpdateVenue, VenuesState } from '@vrt/venue-api';
import { Observable } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { VenueHelperService } from '../../services/venue-helper.service';

@Component({
    selector: 'vrt-venue-view',
    templateUrl: './venue-edit-view.component.html',
    styleUrls: ['./venue-edit-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VenueEditViewComponent implements OnInit {
    venueForm?: UntypedFormGroup;

    venue$: Observable<AppVenue> = this.store.select(VenuesState.venue).pipe(
        filter((venue: AppVenue | undefined): venue is AppVenue => !!venue),
        tap((venue: AppVenue) => {
            this.venueForm = this.venueHelperService.initVenueForm(venue);
        }),
    );

    constructor(
        private venueHelperService: VenueHelperService,
        private activatedRoute: ActivatedRoute,
        private store: Store,
    ) {}

    submit(): void {
        if (this.venueForm?.valid) {
            this.store.dispatch(new UpdateVenue({ appVenue: this.venueForm.value }));
        }
    }

    ngOnInit(): void {
        const params = this.activatedRoute.snapshot.paramMap;
        const venueId = params.get('id');
        if (venueId) {
            this.store.dispatch(new SelectVenue({ venueId: +venueId }));
        }
    }
}
