import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavigationComponent } from '../../atomic/navigation/navigation.component';
import { AdminAuditComponent } from './components/admin-audit/admin-audit.component';
import { UserDetailViewComponent } from './components/user-detail-view/user-detail-view.component';
import { UserOverviewComponent } from './components/user-overview/user-overview.component';
import { UserRegistrationViewComponent } from './components/user-registration-view/user-registration-view.component';

const routes: Routes = [
    {
        path: '',
        component: NavigationComponent,
        children: [
            {
                path: 'audit',
                component: AdminAuditComponent,
            },
            {
                path: 'users/:filter',
                component: UserOverviewComponent,
            },
            {
                path: 'edit/:id',
                component: UserDetailViewComponent,
            },
            {
                path: 'user-registration',
                component: UserRegistrationViewComponent,
            },
            {
                path: 'user-registration/:userId',
                component: UserRegistrationViewComponent,
            },
            { path: '**', redirectTo: 'audit', pathMatch: 'full' },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AdminAreaRoutingModule {}
