import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Store } from '@ngxs/store';
import { AppLogEntry, Metadata, SetPageSize } from '@vrt/api';
import { AuditState, LoadAudits } from '@vrt/audit-api';
import { AuthenticationService } from '@vrt/auth';
import { AppConfigState } from '@vrt/common';
import { combineLatest, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, shareReplay } from 'rxjs/operators';
import { AdminAuditDetailComponent } from '../admin-audit-detail/admin-audit-detail.component';

@Component({
    selector: 'vrt-admin-audit',
    templateUrl: './admin-audit.component.html',
    styleUrls: ['./admin-audit.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminAuditComponent implements OnInit {
    displayedColumns: string[] = ['actor', 'action', 'indirectTarget', 'target', 'timestamp', 'admin-actions'];

    searchForm = new UntypedFormGroup({ search: new UntypedFormControl() });

    sortBy: string | undefined = undefined;

    sortOrder: string | undefined = undefined;

    isDarkThemeSelected$: Observable<boolean> = this.store.select(AppConfigState.isDarkThemeSelected);

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map(({ matches }: BreakpointState) => {
            return matches;
        }),
        shareReplay(),
    );

    data$?: Observable<any>;

    logEntries$: Observable<AppLogEntry[] | null | undefined> = this.store.select(AuditState.logEntries);

    metaData$: Observable<Metadata> = this.store
        .select(AuditState.metadata)
        ?.pipe(filter((metaData: Metadata | undefined): metaData is Metadata => !!metaData));

    constructor(
        private authenticationService: AuthenticationService,
        private breakpointObserver: BreakpointObserver,
        private matDialog: MatDialog,
        private store: Store,
    ) {}

    ngOnInit(): void {
        this.data$ = combineLatest([this.logEntries$, this.isHandset$]);
        this.store.dispatch(
            new LoadAudits({
                page: undefined,
                pageSize: this.store.selectSnapshot(AuditState.metadata)?.pageSize,
                filter: undefined,
                sortBy: this.sortBy,
                sortColumn: this.sortOrder,
                isAdmin: this.authenticationService.isAdmin(),
            }),
        );

        this.searchForm
            .get('search')
            ?.valueChanges.pipe(debounceTime(1500), distinctUntilChanged())
            .subscribe((data: string) => {
                this.searchLogEntries(data);
            });
    }

    pageOptionChanged($event: PageEvent): void {
        this.store.dispatch([
            new LoadAudits({
                page: $event.pageIndex,
                pageSize: $event.pageSize,
                filter: undefined,
                sortBy: this.sortBy,
                sortColumn: this.sortOrder,
                isAdmin: this.authenticationService.isAdmin(),
            }),
            new SetPageSize($event.pageSize),
        ]);
    }

    openLogDetails(row: any): void {
        this.matDialog.open(AdminAuditDetailComponent, {
            disableClose: false,
            autoFocus: true,
            data: { audit: row },
            width: '350px',
        });
    }

    onSort($event: any): void {
        this.sortBy = $event.column.prop;
        this.sortOrder = $event.newValue;
        this.store.dispatch(
            new LoadAudits({
                page: undefined,
                pageSize: undefined,
                filter: undefined,
                sortBy: this.sortBy,
                sortColumn: this.sortOrder,
                isAdmin: this.authenticationService.isAdmin(),
            }),
        );
    }

    searchLogEntries(data: any) {
        this.store.dispatch(
            new LoadAudits({
                page: undefined,
                pageSize: undefined,
                filter: data,
                sortBy: this.sortBy,
                sortColumn: this.sortOrder,
                isAdmin: this.authenticationService.isAdmin(),
            }),
        );
    }
}
