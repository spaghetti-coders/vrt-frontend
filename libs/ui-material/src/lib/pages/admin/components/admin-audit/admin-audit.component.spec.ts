import { ClipboardModule } from '@angular/cdk/clipboard';
import { BreakpointObserver } from '@angular/cdk/layout';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Store } from '@ngxs/store';
import { AuthenticationService } from '@vrt/auth';
import { getTranslocoModule } from '@vrt/common';
import { createSpyFromClass, provideAutoSpy, Spy } from 'jest-auto-spies';
import { MockModule } from 'ng-mocks';
import { of } from 'rxjs';
import { PagingModule } from '../../../../atomic/index';
import { AdminAuditDetailComponent } from '../admin-audit-detail/admin-audit-detail.component';

import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { AdminAuditComponent } from './admin-audit.component';

describe('AdminAuditComponent', () => {
    let component: AdminAuditComponent;
    let fixture: ComponentFixture<AdminAuditComponent>;
    let store: Spy<Store> = createSpyFromClass(Store);
    let matDialog: MatDialog;
    let breakpointObserver: BreakpointObserver;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [AdminAuditComponent, AdminAuditDetailComponent],
            imports: [
                HttpClientTestingModule,
                NoopAnimationsModule,
                getTranslocoModule(),
                RouterModule.forRoot([]),
                ClipboardModule,
                ScrollingModule,
                ReactiveFormsModule,
                MockModule(MatDialogModule),
                MockModule(PagingModule),
            ],
            providers: [ChangeDetectorRef, provideAutoSpy(AuthenticationService), { provide: Store, useValue: store }],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        store = TestBed.inject<any>(Store);
        matDialog = TestBed.inject(MatDialog);
        breakpointObserver = TestBed.inject(BreakpointObserver);
        breakpointObserver.observe = jest.fn().mockReturnValue(of({ matches: false }));
        matDialog.open = jest.fn();
        fixture = TestBed.createComponent(AdminAuditComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        jest.resetAllMocks();
    });

    it('should match snapshot', () => {
        component.logEntries$ = of([]);
        component.isDarkThemeSelected$ = of(true);
        component.metaData$ = of({});
        expect(fixture).toMatchSnapshot();
    });

    it('should call loadAudit', () => {
        component.ngOnInit();
        expect(store.dispatch).toHaveBeenCalledTimes(1);
    });

    it('should load new admin log entry on page event', () => {
        component.pageOptionChanged({
            pageIndex: 1,
            pageSize: 10,
            previousPageIndex: 1,
            length: 10,
        });
        expect(store.dispatch).toHaveBeenCalledTimes(1);
    });

    it('should open log dialog', () => {
        matDialog.open = jest.fn();
        component.openLogDetails({});
        expect(matDialog.open).toHaveBeenCalledTimes(1);
    });

    it('should sort audtis entries', fakeAsync(() => {
        component.onSort({ column: { prop: 'hi' }, newValue: 'Test' });

        expect(store.dispatch).toHaveBeenCalledTimes(1);
    }));

    it('should submit value from keyup input', fakeAsync(() => {
        component.searchLogEntries({});

        expect(store.dispatch).toHaveBeenCalledTimes(1);
    }));

    xit('should return true for isHandset ', fakeAsync(() => {
        breakpointObserver.observe = jest.fn().mockReturnValue(of({ matches: true }));

        component.isHandset$.subscribe((isHandset: boolean) => {
            expect(isHandset).toBeTruthy();
        });
        tick();
    }));
});
