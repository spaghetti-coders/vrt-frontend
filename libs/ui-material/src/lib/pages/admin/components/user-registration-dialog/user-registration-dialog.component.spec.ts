import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { GetRegistrationStatusNameModule, getTranslocoModule } from '@vrt/common';
import { MockModule } from 'ng-mocks';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EXAMPLE_REGISTRATIONS } from '@vrt/testing';
import { UserRegistrationDialogComponent } from './user-registration-dialog.component';

describe('UserRegistrationDialogComponent', () => {
    let component: UserRegistrationDialogComponent;
    let fixture: ComponentFixture<UserRegistrationDialogComponent>;
    let dialogReg: MatDialogRef<UserRegistrationDialogComponent>;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [UserRegistrationDialogComponent],
            imports: [
                MockModule(GetRegistrationStatusNameModule),
                MockModule(MatDividerModule),
                MockModule(MatCardModule),
                getTranslocoModule(),
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: { reg: EXAMPLE_REGISTRATIONS[0] },
                },
                {
                    provide: MatDialogRef,
                    useValue: {
                        close: jest.fn(),
                    },
                },
            ],
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UserRegistrationDialogComponent);
        component = fixture.componentInstance;
        dialogReg = TestBed.inject(MatDialogRef);
        fixture.detectChanges();
    });

    it('should match snapshot', () => {
        expect(fixture).toMatchSnapshot();
    });
});
