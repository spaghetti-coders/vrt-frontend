import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppRegistration } from '@vrt/api';

@Component({
    selector: 'vrt-user-registration-dialog',
    templateUrl: './user-registration-dialog.component.html',
    styleUrls: ['./user-registration-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserRegistrationDialogComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public registration: AppRegistration) {}
}
