import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { AdminUserState, ChangeUserStatusQuery, LoadUserRegistrations, LoadUsers, ResetUserState } from '@vrt/admin-api';
import { AppRegistration, AppUser, Metadata, SetPageSize, UserStatus } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { UserRegistrationDialogComponent } from '../user-registration-dialog/user-registration-dialog.component';

@Component({
    selector: 'vrt-user-registration-view',
    templateUrl: './user-registration-view.component.html',
    styleUrls: ['./user-registration-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserRegistrationViewComponent implements OnInit {
    @Select(AdminUserState.users) users$?: Observable<AppUser[] | undefined | null>;

    @Select(AdminUserState.userRegistrations) userRegistrations$?: Observable<AppRegistration[] | undefined | null>;

    userRegistrationsMetadata$: Observable<Metadata> = this.store
        .select(AdminUserState.userRegistrationsMetadata)
        ?.pipe(filter((metaData: Metadata | undefined): metaData is Metadata => !!metaData));

    isUserSearchVisible = true;

    userId?: number | null;

    registrationFilter: string | undefined;

    sortBy: string | undefined;

    sortOrder: string | undefined;

    constructor(
        private route: ActivatedRoute,
        private store: Store,
        private authenticationService: AuthenticationService,
        private dialog: MatDialog,
    ) {}

    ngOnInit(): void {
        const param = this.route.snapshot.paramMap.get('userId');
        if (param) {
            this.userId = +param;
            this.onUserSelectChanged(+this.userId);
        }
    }

    onUserSearch(value: string) {
        if (value.length >= 3 && value) {
            this.store.dispatch(
                new LoadUsers({
                    page: undefined,
                    pageSize: 100,
                    filter: value,
                    sortBy: undefined,
                    sortOrder: undefined,
                    isAdmin: this.authenticationService.isAdmin(),
                }),
            );
        } else {
            this.store.dispatch(new ResetUserState());
            this._setUserStateToActiveUser();
        }
    }

    onUserSelectChanged(userId: number | undefined): void {
        if (userId) {
            this.userId = userId;
            this.searchRegistrations();
            this.isUserSearchVisible = false;
        }
    }

    onShowRegistrationDetails(registration: AppRegistration) {
        this.dialog.open(UserRegistrationDialogComponent, {
            width: '250px',
            data: registration,
        });
    }

    onShowUserSearch($event: boolean) {
        this.isUserSearchVisible = $event;
    }

    onQueryChange($event: string) {
        this.registrationFilter = $event;
        this.searchRegistrations();
    }

    searchRegistrations() {
        this.store.dispatch(
            new LoadUserRegistrations({
                userId: this.userId as number,
                filter: this.registrationFilter,
                pageSize: undefined,
                page: undefined,
                sortOrder: this.sortOrder ?? 'desc',
                sortBy: this.sortBy ?? 'EventDate',
            }),
        );
    }

    onSortRegistrationData($event: Sort) {
        this.sortBy = $event.active;
        this.sortOrder = $event.direction;
        this.searchRegistrations();
    }

    onPagingChange($event: PageEvent) {
        this.store.dispatch([
            new LoadUserRegistrations({
                userId: this.userId as number,
                filter: this.registrationFilter,
                pageSize: $event.pageSize,
                page: $event.pageIndex,
                sortOrder: this.sortOrder ?? 'desc',
                sortBy: this.sortBy ?? 'EventDate',
            }),
            new SetPageSize($event.pageSize),
        ]);
    }

    private _setUserStateToActiveUser(): void {
        this.store.dispatch(new ChangeUserStatusQuery({ status: UserStatus.NUMBER_1 }));
    }
}
