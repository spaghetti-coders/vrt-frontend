import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { NgxsModule, Store } from '@ngxs/store';
import { SetPageSize } from '@vrt/api';
import { IS_DEMO_BUILD } from '@vrt/utils';
import { MockComponent, MockModule } from 'ng-mocks';
import { UserSearchModule } from '../../../../atomic/index';
import { UserRegistrationDialogComponent } from '../user-registration-dialog/user-registration-dialog.component';
import { UserRegistrationTableComponent } from '../user-registration-table/user-registration-table.component';

import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoadUserRegistrations } from '@vrt/admin-api';
import { EXAMPLE_REGISTRATIONS } from '@vrt/testing';
import { UserRegistrationViewComponent } from './user-registration-view.component';

describe('UserRegistrationViewComponent', () => {
    let component: UserRegistrationViewComponent;
    let fixture: ComponentFixture<UserRegistrationViewComponent>;
    let store: Store;
    let dialog: MatDialog;

    const paramMap = new Map([['userId', '123']]);

    const beforeEachFn = () => {
        fixture = TestBed.createComponent(UserRegistrationViewComponent);
        store = TestBed.inject(Store);
        dialog = TestBed.inject(MatDialog);
        component = fixture.componentInstance;
        fixture.detectChanges();
    };

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [UserRegistrationViewComponent, MockComponent(UserRegistrationTableComponent)],
            imports: [MockModule(UserSearchModule), NgxsModule.forRoot(), RouterModule.forRoot([]), HttpClientTestingModule],
            providers: [
                { provide: IS_DEMO_BUILD, useValue: false },
                {
                    provide: Store,
                    useValue: {
                        select: jest.fn(),
                        dispatch: jest.fn(),
                    },
                },
                {
                    provide: MatSnackBar,
                    useValue: jest.fn(),
                },
                { provide: MatDialog, useValue: { open: jest.fn() } },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            paramMap,
                        },
                    },
                },
            ],
        });
    });

    describe('component with ActivatedRoute parameter', () => {
        beforeEach(() => {
            beforeEachFn();
        });
        it('should create', () => {
            expect(component).toBeTruthy();
        });

        it('should call onUserSelectChanged methode if userId is in ActivatedRoute route', () => {
            const userSearch = component.onUserSelectChanged;
            component.onUserSelectChanged = jest.fn();
            component.ngOnInit();
            expect(component.onUserSelectChanged).toHaveBeenCalledWith(123);
            component.onUserSelectChanged = userSearch;
        });

        it('should call onUserSearch methode', () => {
            component.onUserSearch('Example');

            expect(store.dispatch).toHaveBeenCalledWith({
                payload: {
                    filter: 'Example',
                    isAdmin: false,
                    page: undefined,
                    pageSize: 100,
                    sortBy: undefined,
                    sortOrder: undefined,
                },
            });
            jest.restoreAllMocks();
            component.onUserSearch('ex');

            expect(store.dispatch).toHaveBeenCalledWith({});
            expect(store.dispatch).toHaveBeenCalledWith({ payload: { status: 1 } });
        });

        it('should open registration details component', () => {
            component.onShowRegistrationDetails(EXAMPLE_REGISTRATIONS[0]);

            expect(dialog.open).toHaveBeenCalledWith(UserRegistrationDialogComponent, {
                data: {
                    eventId: 1,
                    registrationDate: '2018-10-10',
                    status: 1,
                    userDisplayName: 'YOU',
                    userId: 10,
                },
                width: '250px',
            });
        });

        it('should change visibility for user search', () => {
            component.isUserSearchVisible = true;

            component.onShowUserSearch(false);

            expect(component.isUserSearchVisible).toBeFalsy();
        });

        it('should dispatch load user registration via page event', () => {
            jest.resetAllMocks();
            const pageEvent: PageEvent = {
                pageIndex: 1,
                pageSize: 10,
                previousPageIndex: 0,
                length: 10,
            };

            component.onPagingChange(pageEvent);
            fixture.detectChanges();

            expect(store.dispatch).toHaveBeenCalledWith([
                new LoadUserRegistrations({
                    userId: 123,
                    filter: undefined,
                    pageSize: pageEvent.pageSize,
                    page: pageEvent.pageIndex,
                    sortOrder: 'desc',
                    sortBy: 'EventDate',
                }),
                new SetPageSize(10),
            ]);
        });

        it('should set new registration filter and trigger searchRegistration function', () => {
            const searchFn = component.searchRegistrations;
            component.searchRegistrations = jest.fn();
            component.onQueryChange('EXAMPLE');
            expect(component.registrationFilter).toEqual('EXAMPLE');
            expect(component.searchRegistrations).toHaveBeenCalled();

            component.searchRegistrations = searchFn;
        });

        it('should change sort registration data', () => {
            const searchFn = component.searchRegistrations;
            component.searchRegistrations = jest.fn();
            component.onSortRegistrationData({ active: 'name', direction: 'asc' });

            expect(component.sortBy).toEqual('name');
            expect(component.sortOrder).toEqual('asc');
            expect(component.searchRegistrations).toHaveBeenCalled();

            component.searchRegistrations = searchFn;
        });
    });

    describe('component without ActivatedRoute parameter', () => {
        const emptyParamMap = new Map([]);

        beforeEach(() => {
            TestBed.overrideProvider(ActivatedRoute, {
                useValue: {
                    snapshot: {
                        paramMap: emptyParamMap,
                    },
                },
            });
            beforeEachFn();
        });

        it('should call onUserSelectChanged methode if userId is in ActivatedRoute route', () => {
            const userSearch = component.onUserSelectChanged;
            component.onUserSelectChanged = jest.fn();
            component.ngOnInit();
            expect(component.onUserSelectChanged).not.toHaveBeenCalled();
            component.onUserSelectChanged = userSearch;
        });
    });
});
