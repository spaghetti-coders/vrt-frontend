import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { AdminUserState, ChangeUserStatusQuery, DeleteUser, LoadUsers, UpdateUserStatus } from '@vrt/admin-api';
import { AppUser, Metadata, RecoveryTokenResult, SetPageSize, UsersService } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { AppConfigState } from '@vrt/common';
import { UserStatusAdapter, UserStatusLocal } from '@vrt/utils';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { ConfirmDialogComponent } from '../../../../atomic/confirm-dialog/confirm-dialog.component';
import { UserDetailViewComponent } from '../user-detail-view/user-detail-view.component';

@Component({
    selector: 'vrt-user-overview',
    templateUrl: './user-overview.component.html',
    styleUrls: ['./user-overview.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserOverviewComponent implements OnInit {
    isDarkThemeSelected$: Observable<boolean> = this.store.select(AppConfigState.isDarkThemeSelected);

    searchForm = new UntypedFormGroup({ search: new UntypedFormControl() });

    metadata$: Observable<Metadata> = this.store
        .select(AdminUserState.metadata)
        ?.pipe(filter((metaData: Metadata | undefined): metaData is Metadata => !!metaData));

    users$?: Observable<AppUser[]>;

    selectedStatus: number | undefined | any;

    userStatus = UserStatusLocal;

    userStatuses = UserStatusAdapter.allElements();

    constructor(
        public dialog: MatDialog,
        private usersService: UsersService,
        private authenticationService: AuthenticationService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private store: Store,
    ) {}

    ngOnInit(): void {
        const params = this.activatedRoute.snapshot.paramMap;
        const query = params.get('filter');
        if (query) {
            this.initStatus(+query);
            this.changeUserView();
        }

        this.searchForm
            .get('search')
            ?.valueChanges.pipe(debounceTime(1000), distinctUntilChanged())
            .subscribe((data: string) => {
                this.searchUser(data);
            });
    }

    searchUser(data: string) {
        this.store.dispatch(new ChangeUserStatusQuery({ status: this.userStatus.Any }));
        this.selectedStatus = this.userStatus.Any;
        this.store.dispatch(
            new LoadUsers({
                page: undefined,
                pageSize: this.store.selectSnapshot(AdminUserState.metadata)?.pageSize,
                filter: data,
                sortOrder: undefined,
                sortBy: undefined,
                isAdmin: this.authenticationService.isAdmin(),
            }),
        );
    }

    deleteUser(user: AppUser): void {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {});

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                if (user.id) {
                    this.store.dispatch(new DeleteUser({ userId: user.id }));
                }
            }
        });
    }

    generateToken(user: AppUser): void {
        if (user.id != null) {
            this.usersService.generateRecoveryToken(user.id).subscribe((res: RecoveryTokenResult) => {
                if (res) {
                    this.dialog.open(UserDetailViewComponent, {
                        data: res.recoveryToken,
                    });
                }
            });
        }
    }

    selectUser(user: AppUser): void {
        if (user.id) {
            this.router.navigate([`/admin/edit/${user.id}`]);
        }
    }

    switchUserActiveState(user: AppUser): void {
        const updatedUser = {
            ...user,
            status: user.status === this.userStatus.Active ? this.userStatus.Inactive : this.userStatus.Active,
        } as AppUser;
        this.store.dispatch(new UpdateUserStatus({ user: updatedUser }));
    }

    selectStatus(status: number): void {
        this.selectedStatus = status;
        this.store.dispatch([
            new ChangeUserStatusQuery({ status: +status }),
            new LoadUsers({
                page: undefined,
                pageSize: this.store.selectSnapshot(AdminUserState.metadata)?.pageSize,
                filter: undefined,
                sortOrder: undefined,
                sortBy: undefined,
                isAdmin: this.authenticationService.isAdmin(),
            }),
        ]);
    }

    initStatus(query: number): void {
        this.selectedStatus = query;
        this.store.dispatch([
            new ChangeUserStatusQuery({ status: query }),
            new LoadUsers({
                page: undefined,
                pageSize: this.store.selectSnapshot(AdminUserState.metadata)?.pageSize,
                filter: undefined,
                sortOrder: undefined,
                sortBy: undefined,
                isAdmin: this.authenticationService.isAdmin(),
            }),
        ]);
    }

    pageOptionChanged($event: PageEvent): void {
        this.store.dispatch([
            new LoadUsers({
                page: $event.pageIndex,
                pageSize: $event.pageSize,
                filter: undefined,
                sortOrder: undefined,
                sortBy: undefined,
                isAdmin: this.authenticationService.isAdmin(),
            }),
            new SetPageSize($event.pageSize),
        ]);
    }

    private changeUserView(): void {
        this.users$ = this.store.select(AdminUserState.users).pipe(
            filter((appUsers: AppUser[] | null | undefined): appUsers is AppUser[] => !!appUsers),
            map((appUsers: AppUser[]) => {
                if (this.selectedStatus === 4) {
                    return appUsers;
                } else {
                    return appUsers?.filter((item: AppUser) => item.status === this.selectedStatus);
                }
            }),
        );
    }
}
