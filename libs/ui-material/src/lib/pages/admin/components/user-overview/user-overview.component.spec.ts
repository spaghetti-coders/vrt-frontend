import { fakeAsync, tick } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { AppUser, UsersService } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { Spy, createSpyFromClass } from 'jest-auto-spies';
import { of } from 'rxjs';

import { AdminUserState, UpdateUserStatus } from '@vrt/admin-api';
import { METADATA_EXAMPLE, USER_EXAMPLE } from '@vrt/testing';
import { ConfirmDialogComponent } from '../../../../atomic/confirm-dialog/confirm-dialog.component';
import { UserOverviewComponent } from './user-overview.component';

const paramMap = new Map([['filter', '4']]);

describe('UserOverviewComponent', () => {
    let component: UserOverviewComponent;
    const store = createSpyFromClass(Store, {
        methodsToSpyOn: ['select'],
    });
    const matDialog = createSpyFromClass(MatDialog);
    const router = createSpyFromClass(Router);
    const usersService = createSpyFromClass(UsersService);
    let activatedRoute: Spy<ActivatedRoute>;
    const authenticationService = createSpyFromClass(AuthenticationService);

    beforeEach(() => {
        activatedRoute = createSpyFromClass(ActivatedRoute, {
            gettersToSpyOn: ['snapshot'],
        });
        activatedRoute.accessorSpies.getters.snapshot.mockReturnValue({
            paramMap,
        });
        store.select.calledWith(AdminUserState.metadata as any).nextWith(of(METADATA_EXAMPLE));
        component = new UserOverviewComponent(matDialog, usersService, authenticationService, router, activatedRoute, store);
        jest.clearAllMocks();
    });

    it('should change user view', fakeAsync(() => {
        store.select.calledWith(AdminUserState.users as any).nextWith([{ id: 12, status: 4 } as AppUser]);
        component.initStatus(4);
        expect(component.selectedStatus).toEqual(4);

        component.ngOnInit();

        component.users$?.subscribe((users) => {
            expect(users).toEqual([{ id: 12, status: 4 } as AppUser]);
        });
        tick();
    }));

    it('should search user', fakeAsync(() => {
        component.searchUser('admin');
        expect(store.dispatch).toHaveBeenCalledTimes(2);
    }));

    it('should change user view and show filtered result', fakeAsync(() => {
        store.select.calledWith(AdminUserState.users as any).nextWith([{ id: 11, status: 4 } as AppUser, USER_EXAMPLE]);
        component.ngOnInit();
        component.selectedStatus = 1;

        component.users$?.subscribe((users) => {
            expect(users).toStrictEqual([USER_EXAMPLE]);
        });
        tick();
    }));

    it('should delete user', fakeAsync(() => {
        const spy = jest.spyOn(matDialog, 'open').mockReturnValue({
            afterClosed: () => of(true),
        } as MatDialogRef<ConfirmDialogComponent>);
        component.deleteUser({ id: 12, username: 'Admin' } as AppUser);

        expect(spy).toHaveBeenCalledTimes(1);
        expect(store.dispatch).toHaveBeenCalledTimes(1);
    }));

    it('should generate token for an user', fakeAsync(() => {
        usersService.generateRecoveryToken.mockReturnValue(of('TOKEN') as any);
        component.generateToken({ id: 12 } as AppUser);

        expect(usersService.generateRecoveryToken).toHaveBeenCalledTimes(1);
    }));

    it('should select user', fakeAsync(() => {
        store.select.mockReturnValue(of([{ id: 12, status: 4 } as AppUser]));
        component.selectUser({ id: 12 } as AppUser);
        expect(router.navigate).toHaveBeenCalledWith(['/admin/edit/12']);
    }));

    it('should switch user state', fakeAsync(() => {
        jest.resetAllMocks();
        const user = { id: 123, username: 'Peter', status: 1 } as AppUser;
        component.switchUserActiveState(user);
        expect(store.dispatch).toHaveBeenCalledTimes(1);
        expect(store.dispatch).toHaveBeenCalledWith(new UpdateUserStatus({ user: { ...user, status: 2 } }));
    }));

    it('should select status', fakeAsync(() => {
        jest.resetAllMocks();
        store.select.calledWith(AdminUserState.metadata as any).nextWith(of(METADATA_EXAMPLE));

        component.selectStatus(2);
        expect(store.dispatch).toHaveBeenCalledTimes(1);
        tick();
    }));

    it('should change valued and dispatch action if page event was triggered', fakeAsync(() => {
        jest.resetAllMocks();
        store.select.calledWith(AdminUserState.metadata as any).nextWith(of(METADATA_EXAMPLE));

        component.pageOptionChanged({
            pageIndex: 1,
            previousPageIndex: 1,
            pageSize: 20,
            length: 10,
        });
        expect(store.dispatch).toHaveBeenCalledTimes(1);
    }));
});
