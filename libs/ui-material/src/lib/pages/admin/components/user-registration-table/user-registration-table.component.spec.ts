import { BreakpointObserver } from '@angular/cdk/layout';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { of } from 'rxjs';

import { REGISTRATION_EXAMPLE } from '@vrt/testing';
import { UserRegistrationTableComponent } from './user-registration-table.component';

describe('UserRegistrationTableComponent', () => {
    let component: UserRegistrationTableComponent;
    let fixture: ComponentFixture<UserRegistrationTableComponent>;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [UserRegistrationTableComponent],
            providers: [
                {
                    provide: BreakpointObserver,
                    useValue: {
                        observe: jest.fn().mockReturnValue(of({ matches: true })),
                    },
                },
            ],
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UserRegistrationTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should emit page change event', () => {
        const spy = jest.spyOn(component.pagingChange, 'emit');
        const pageEvent: PageEvent = {
            pageIndex: 1,
            pageSize: 10,
            previousPageIndex: 0,
            length: 10,
        };
        component.onChangePage(pageEvent);

        expect(spy).toHaveBeenCalledWith(pageEvent);
    });

    it('should validate that Action column is in columns array', waitForAsync(() => {
        component.isHandset$.subscribe((isHandset) => {
            expect(component.isMobile).toBeTruthy();
        });
    }));

    it('should emit event registration', () => {
        component.registration.emit = jest.fn();
        component.onShowRegistrationDetails(REGISTRATION_EXAMPLE);

        expect(component.registration.emit).toHaveBeenCalled();
        expect(component.registration.emit).toHaveBeenCalledWith(REGISTRATION_EXAMPLE);
    });

    it('should emit value to show or hide user search input', () => {
        component.showUserSearch.emit = jest.fn();
        component.onShowUserSearch();

        expect(component.showUserSearch.emit).toHaveBeenCalled();
        expect(component.showUserSearch.emit).toHaveBeenCalledWith(true);

        component.isUserSearchVisible = true;
        component.onShowUserSearch();
        expect(component.showUserSearch.emit).toHaveBeenCalled();
        expect(component.showUserSearch.emit).toHaveBeenCalledWith(false);
    });

    it('should emit search input value', () => {
        component.searchTerm.emit = jest.fn();
        component.emitNewSearch('EXAMPLE');

        expect(component.searchTerm.emit).toHaveBeenCalled();
        expect(component.searchTerm.emit).toHaveBeenCalledWith('EXAMPLE');
    });

    it('should emit sort data value', () => {
        const sortExample: Sort = { active: 'Example', direction: 'asc' };
        component.sortRegistrationData.emit = jest.fn();
        component.sortData(sortExample);

        expect(component.sortRegistrationData.emit).toHaveBeenCalled();
        expect(component.sortRegistrationData.emit).toHaveBeenCalledWith(sortExample);
    });
});
