import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { AppRegistration, Metadata } from '@vrt/api';
import { fadeInOut } from '@vrt/utils';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, shareReplay, tap } from 'rxjs/operators';

@Component({
    selector: 'vrt-user-registration-table',
    templateUrl: './user-registration-table.component.html',
    styleUrls: ['./user-registration-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [fadeInOut],
})
export class UserRegistrationTableComponent {
    @Input()
    userRegistrations: AppRegistration[] | null | undefined;

    @Input()
    metadata$!: Observable<Metadata>;

    @Input()
    isUserSearchVisible = false;

    @Input()
    showSearchInput = true;

    @Output()
    sortBy: EventEmitter<string> = new EventEmitter<string>();

    @Output()
    registration: EventEmitter<AppRegistration> = new EventEmitter<AppRegistration>();

    @Output()
    showUserSearch: EventEmitter<boolean> = new EventEmitter<boolean>();

    @Output()
    searchTerm: EventEmitter<string> = new EventEmitter<string>();

    @Output()
    sortRegistrationData: EventEmitter<Sort> = new EventEmitter<Sort>();

    @Output()
    pagingChange: EventEmitter<PageEvent> = new EventEmitter<PageEvent>();

    @ViewChild(MatSort)
    sort?: MatSort;

    columns = ['UserDisplayName', 'EventName', 'EventDate', 'RegistrationDate'];

    isMobile = false;

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map(({ matches }: BreakpointState) => {
            this.isMobile = matches;
            return matches;
        }),
        shareReplay(),
    );

    search: UntypedFormControl = new UntypedFormControl();

    searchResult$: Observable<any> = this.search?.valueChanges.pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap((value) => {
            this.emitNewSearch(value);
        }),
    );

    constructor(private breakpointObserver: BreakpointObserver) {}

    onShowRegistrationDetails(appRegistration: AppRegistration) {
        this.registration.emit(appRegistration);
    }

    onShowUserSearch() {
        this.showUserSearch.emit(!this.isUserSearchVisible);
    }

    emitNewSearch(value: string) {
        this.searchTerm.emit(value);
    }

    sortData($event: Sort) {
        this.sortRegistrationData.emit($event);
    }

    onChangePage($event: PageEvent) {
        this.pagingChange.emit($event);
    }
}
