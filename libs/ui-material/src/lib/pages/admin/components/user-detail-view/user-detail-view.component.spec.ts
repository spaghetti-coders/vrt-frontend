import { ClipboardModule } from '@angular/cdk/clipboard';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { NgxsModule, Store } from '@ngxs/store';
import { AppUser, UsersService } from '@vrt/api';
import { getTranslocoModule } from '@vrt/common';
import { NotificationSnackBarService } from '@vrt/utils';
import { createSpyFromClass, Spy } from 'jest-auto-spies';
import { MockModule } from 'ng-mocks';
import { of } from 'rxjs';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UpdateUserStatus } from '@vrt/admin-api';
import { UserDetailViewComponent } from './user-detail-view.component';

const USER_EXAMPLE = {
    displayName: 'Max Muster',
    id: 1,
    username: 'muster',
    email: '',
    receiveNotifications: false,
    role: 'user',
    status: 1,
} as AppUser;

describe('UserPopupComponent', () => {
    let component: UserDetailViewComponent;
    let fixture: ComponentFixture<UserDetailViewComponent>;
    let usersService: UsersService;
    let store: Spy<Store>;
    let notificationService: Spy<NotificationSnackBarService>;

    const paramMap = new Map([['id', '123']]);

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                MockModule(MatSnackBarModule),
                MockModule(MatIconModule),
                RouterModule.forRoot([]),
                NgxsModule.forRoot(),
                getTranslocoModule(),
                FormsModule,
                ReactiveFormsModule,
                MatDialogModule,
                ClipboardModule,
                BrowserAnimationsModule,
                HttpClientTestingModule,
            ],
            declarations: [UserDetailViewComponent],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        paramMap: of(paramMap),
                    },
                },
                {
                    provide: NotificationSnackBarService,
                    useValue: createSpyFromClass(NotificationSnackBarService),
                },
                { provide: Store, useValue: createSpyFromClass(Store) },
                { provide: MAT_DIALOG_DATA, useValue: 'token' },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UserDetailViewComponent);
        usersService = TestBed.inject(UsersService);
        notificationService = TestBed.inject<any>(NotificationSnackBarService);
        store = TestBed.inject<any>(Store);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should init form', fakeAsync(() => {
        usersService.getUser = jest.fn().mockReturnValue(of(USER_EXAMPLE));
        component.ngOnInit();
        component.user$?.subscribe((res) => {
            expect(component.userForm).toBeTruthy();
            expect(component.currentStatus).toEqual(1);
        });
        tick();
    }));

    it('should switch user status', fakeAsync(() => {
        component.currentStatus = 1;
        component.switchUserActiveState(USER_EXAMPLE);

        expect(store.dispatch).toHaveBeenCalledTimes(1);
        expect(USER_EXAMPLE.status).toEqual(2);
    }));

    it('should call notfication toast', fakeAsync(() => {
        component.showToast();
        expect(notificationService.showSuccess).toHaveBeenCalledTimes(1);
    }));

    it('should update user status', fakeAsync(() => {
        component.currentStatus = 1;
        component.updateUser();
        expect(store.dispatch).toHaveBeenCalledTimes(1);
    }));

    it('should switch user status 333', fakeAsync(() => {
        component.switchUserActiveState({ ...USER_EXAMPLE, status: 2 });
        expect(store.dispatch).toHaveBeenCalledWith(new UpdateUserStatus({ user: { ...USER_EXAMPLE, status: 1 } }));
    }));

    it('should init form without user stats', fakeAsync(() => {
        usersService.getUser = jest.fn().mockReturnValue(of({ ...USER_EXAMPLE, status: undefined }));
        component.ngOnInit();
        component.user$?.subscribe((res) => {
            expect(component.userForm).toBeTruthy();
            expect(component.currentStatus).toEqual(0);
        });
        tick();
    }));
});
