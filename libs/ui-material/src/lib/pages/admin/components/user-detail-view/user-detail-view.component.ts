import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit, Optional } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { UpdateUser, UpdateUserStatus } from '@vrt/admin-api';
import { AppUser, UsersService } from '@vrt/api';
import { NotificationSnackBarService, UserStatusLocal } from '@vrt/utils';
import { EMPTY, Observable, switchMap } from 'rxjs';
import { filter, tap } from 'rxjs/operators';

@Component({
    selector: 'vrt-user-detail-view',
    templateUrl: './user-detail-view.component.html',
    styleUrls: ['./user-detail-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserDetailViewComponent implements OnInit {
    currentStatus = 0;

    token = '';

    userForm?: UntypedFormGroup;

    userStatus = UserStatusLocal;

    user$?: Observable<AppUser>;

    constructor(
        @Optional() @Inject(MAT_DIALOG_DATA) public dialogData: string,
        private formBuilder: UntypedFormBuilder,
        private notificationSnackBarService: NotificationSnackBarService,
        private changeDetectorRef: ChangeDetectorRef,
        private activatedRoute: ActivatedRoute,
        private usersService: UsersService,
        private store: Store,
    ) {}

    ngOnInit(): void {
        this.user$ = this.activatedRoute.paramMap.pipe(
            switchMap((params) => {
                const userId = params.get('id');
                if (userId) {
                    return this.usersService.getUser(+userId).pipe(
                        filter((user: AppUser): user is AppUser => !!user),
                        tap((data: AppUser) => {
                            this.userForm = this.formBuilder.group({
                                id: [data.id],
                                displayName: [data.displayName, [Validators.required, Validators.minLength(3)]],
                                username: [data.username],
                                email: [data.email, [Validators.email]],
                                receiveNotifications: [data.receiveNotifications],
                                role: [data.role],
                            });
                            this.currentStatus = data.status ?? 0;
                        }),
                    );
                } else {
                    return EMPTY;
                }
            }),
        );

        if (typeof this.dialogData === 'string') {
            this.token = this.dialogData;
        }
        this.changeDetectorRef.markForCheck();
    }

    switchUserActiveState(user: AppUser): void {
        user = {
            ...user,
            status:
                user.status === this.userStatus.Active ? (user.status = this.userStatus.Inactive) : (user.status = this.userStatus.Active),
        };

        this.store.dispatch(new UpdateUserStatus({ user }));
    }

    showToast(): void {
        this.notificationSnackBarService.showSuccess('In Zwischenablage kopiert.');
    }

    updateUser(): void {
        const status = this.currentStatus;
        this.store.dispatch(new UpdateUser({ user: this.userForm?.value, currentStatus: status }));
    }
}
