import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { NgxsModule } from '@ngxs/store';
import { AppLogEntry } from '@vrt/api';
import { getTranslocoModule } from '@vrt/common';

import { AdminAuditDetailComponent } from './admin-audit-detail.component';

describe('AdminAuditDetailComponent', () => {
    let component: AdminAuditDetailComponent;
    let fixture: ComponentFixture<AdminAuditDetailComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AdminAuditDetailComponent],
            imports: [MatCardModule, MatDialogModule, getTranslocoModule(), NgxsModule.forRoot()],
            providers: [
                {
                    provide: MatDialogRef,
                    useValue: {},
                },
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: { audit: { action: 'delete', id: 12 } as AppLogEntry },
                },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AdminAuditDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should match snapshot', () => {
        component.audit = {} as AppLogEntry;
        expect(fixture).toMatchSnapshot();
    });

    it('should validate that audit exists', waitForAsync(() => {
        expect(component.audit).toEqual({
            action: 'delete',
            id: 12,
        } as AppLogEntry);
        expect(component.audit).toBeTruthy();
    }));
});
