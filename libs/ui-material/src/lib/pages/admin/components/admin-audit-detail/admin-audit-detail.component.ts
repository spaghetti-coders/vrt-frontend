import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppLogEntry } from '@vrt/api';

@Component({
    selector: 'vrt-admin-audit-detail',
    templateUrl: './admin-audit-detail.component.html',
    styleUrls: ['./admin-audit-detail.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminAuditDetailComponent {
    audit?: AppLogEntry;

    constructor(@Inject(MAT_DIALOG_DATA) dialogConfig: any) {
        if (dialogConfig.audit) {
            this.audit = dialogConfig.audit;
        }
    }
}
