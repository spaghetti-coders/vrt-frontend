import { ClipboardModule } from '@angular/cdk/clipboard';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { TranslocoModule } from '@ngneat/transloco';
import { NgxsModule } from '@ngxs/store';
import { PaginatorState } from '@vrt/api';
import { GetRegistrationStatusNameModule } from '@vrt/common';
import { ConfirmDialogModule, NavigationModule, PagingModule, UserSearchModule } from '../../atomic/index';

import { AdminUserState } from '@vrt/admin-api';
import { AuditState } from '@vrt/audit-api';
import { AdminAreaRoutingModule } from './admin-area-routing.module';
import { AdminAuditDetailComponent } from './components/admin-audit-detail/admin-audit-detail.component';
import { AdminAuditComponent } from './components/admin-audit/admin-audit.component';
import { UserDetailViewComponent } from './components/user-detail-view/user-detail-view.component';
import { UserOverviewComponent } from './components/user-overview/user-overview.component';
import { UserRegistrationDialogComponent } from './components/user-registration-dialog/user-registration-dialog.component';
import { UserRegistrationTableComponent } from './components/user-registration-table/user-registration-table.component';
import { UserRegistrationViewComponent } from './components/user-registration-view/user-registration-view.component';

@NgModule({
    declarations: [
        UserOverviewComponent,
        UserDetailViewComponent,
        AdminAuditComponent,
        AdminAuditDetailComponent,
        UserRegistrationViewComponent,
        UserRegistrationTableComponent,
        UserRegistrationDialogComponent,
    ],
    imports: [
        AdminAreaRoutingModule,
        ClipboardModule,
        CommonModule,
        ConfirmDialogModule,
        FormsModule,
        GetRegistrationStatusNameModule,
        MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatRadioModule,
        MatSelectModule,
        MatSortModule,
        MatTableModule,
        NavigationModule,
        NgxsModule.forFeature([AuditState, AdminUserState, PaginatorState]),
        PagingModule,
        ReactiveFormsModule,
        ScrollingModule,
        TranslocoModule,
        UserSearchModule,
    ],
})
export class AdminAreaModule {}
