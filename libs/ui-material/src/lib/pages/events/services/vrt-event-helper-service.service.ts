import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AppEvent, Metadata } from '@vrt/api';
import { SetToggleState } from '@vrt/common';
import {
    AddCurrentUserToAnEvent,
    EventsState,
    LoadFutureEvents,
    LoadPastEvents,
    RemoveCurrentUserFromEvent,
    RemoveEvent,
} from '@vrt/event-api';
import { ApiCallParameters, EventForm } from '@vrt/utils';
import dayjs from 'dayjs';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { VrtEventService } from './vrt-event.service';

@Injectable()
export class VrtEventHelperServiceService {
    metaData$: Observable<Metadata> = this.store
        .select(EventsState.eventMetadata)
        ?.pipe(filter((metaData: Metadata | undefined): metaData is Metadata => !!metaData));

    constructor(
        private vrtEventService: VrtEventService,
        private formBuilder: FormBuilder,
        private store: Store,
    ) {}

    getPastEvents(parameters: ApiCallParameters): void {
        this.store.dispatch(
            new LoadPastEvents({
                page: parameters.page,
                pageSize: parameters.pageSize,
                filter: parameters.filter,
                sortBy: 'Date',
                sortOrder: 'desc',
            }),
        );
    }

    getFutureEvents(parameters: ApiCallParameters): void {
        this.store.dispatch(new LoadFutureEvents({ apiCallParameters: parameters }));
    }

    deleteEvent(vrtEvent: AppEvent): void {
        this.store.dispatch(new RemoveEvent({ id: vrtEvent.id as number }));
    }

    downloadEventLog(vrtEvent: AppEvent): void {
        this.vrtEventService.downloadLog(vrtEvent);
    }

    registerForEvent(vrtEvent: AppEvent): void {
        this.store.dispatch(new AddCurrentUserToAnEvent({ eventId: vrtEvent.id as number }));
    }

    prepareEventForm(event: AppEvent | undefined): FormGroup<EventForm> {
        return this.formBuilder.nonNullable.group({
            id: [event?.id ?? undefined],
            name: [event?.name ?? '', [Validators.required]],
            date: [event?.date ?? dayjs().toDate().toDateString(), [Validators.required]],
            time: [event?.date ? new Date(event.date).toLocaleTimeString() : '12:00'],
            isOpenForRegistration: [event?.isOpenForRegistration ?? false],
            description: [event?.description ?? '', [Validators.maxLength(256)]],
            capacity: [event?.capacity ?? 10, [Validators.required, Validators.min(1)]],
            venueId: [event?.venueId ?? 0],
        });
    }

    unregisterFromEvent(vrtEvent: AppEvent, userId: number): void {
        if (vrtEvent.id !== undefined) {
            this.store.dispatch(new RemoveCurrentUserFromEvent({ eventId: vrtEvent.id, userId }));
        }
    }

    setToggleSwitchState(toggleSwitch: boolean): void {
        this.store.dispatch(new SetToggleState({ toggleSwitch }));
    }
}
