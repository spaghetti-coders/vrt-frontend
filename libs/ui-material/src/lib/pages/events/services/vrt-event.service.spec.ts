import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LOCALE_ID } from '@angular/core';
import { TestBed, TestModuleMetadata, waitForAsync } from '@angular/core/testing';
import { EventsService } from '@vrt/api';
import { getTranslocoModule } from '@vrt/common';
import { EVENT_EXAMPLE, createFileFromMockFile } from '@vrt/testing';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { Spy, createSpyFromClass } from 'jest-auto-spies';
import { of } from 'rxjs';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { VrtEventService } from './vrt-event.service';

describe('VertEventService', () => {
    let service: VrtEventService;
    let eventsService: EventsService;
    const providers = [
        { provide: IS_DEMO_BUILD, useValue: false },
        { provide: LOCALE_ID, useValue: 'de-DE' },
    ];

    const setup = (config: TestModuleMetadata = {}) => {
        TestBed.configureTestingModule({
            ...{
                imports: [MatSnackBarModule, HttpClientTestingModule, getTranslocoModule()],
                providers,
                teardown: { destroyAfterEach: false },
            },
            ...config,
        });
    };

    describe('Normal App', () => {
        beforeEach(() => {
            setup();
            service = TestBed.inject(VrtEventService);
            eventsService = TestBed.inject(EventsService);
        });

        it('should be created', () => {
            expect(service).toBeTruthy();
        });

        it('should download log file', waitForAsync(() => {
            window.URL.createObjectURL = jest.fn();
            const file = createFileFromMockFile({
                body: 'test',
                mimeType: 'text/plain',
                name: 'test.txt',
            });
            eventsService.exportEventDetails = jest.fn().mockReturnValue(of());
            service.downloadLog(EVENT_EXAMPLE);

            expect(eventsService.exportEventDetails).toHaveBeenCalled();
        }));
    });

    describe('Demo App', () => {
        let notificationSnackBarService: Spy<NotificationSnackBarService>;
        beforeEach(() => {
            setup({
                providers: [
                    ...providers,
                    { provide: IS_DEMO_BUILD, useValue: true },
                    {
                        provide: NotificationSnackBarService,
                        useValue: createSpyFromClass(NotificationSnackBarService),
                    },
                ],
            });
            service = TestBed.inject(VrtEventService);
            notificationSnackBarService = TestBed.inject<any>(NotificationSnackBarService);
        });
        it('should show warning on demo version', () => {
            service.downloadLog(EVENT_EXAMPLE);
            expect(notificationSnackBarService.showWarning).toHaveBeenCalled();
        });
    });
});
