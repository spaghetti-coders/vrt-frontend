import { Inject, Injectable } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { AppEvent, EventsService } from '@vrt/api';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';

@Injectable({
    providedIn: 'root',
})
export class VrtEventService {
    constructor(
        @Inject(IS_DEMO_BUILD) private isDemoBuild: boolean,
        private eventsService: EventsService,
        private snackBarService: NotificationSnackBarService,
        private translocoService: TranslocoService,
    ) {}

    /**
     * This method retrieves the event log for the given event and triggers a file download on the user's device.
     * The method will raise an error if it is called for an event that has not yet started (event.date is in the future).
     *
     * @param event - The event whose log to download
     */
    downloadLog(event: AppEvent): void {
        if (this.isDemoBuild) {
            this.snackBarService.showWarning(this.translocoService.translate('demo.file_download'));
        }
        if (event.id && !this.isDemoBuild) {
            this.eventsService.exportEventDetails(event.id).subscribe((response) => {
                // Filename should actually be retrieved form response's 'Content-Disposition' header (however, it's somehow missing)
                const filename = `Event - ${event.name} - Log.txt`;

                const downloadLink = document.createElement('a');
                downloadLink.href = window.URL.createObjectURL(response);
                downloadLink.setAttribute('download', filename);

                document.body.appendChild(downloadLink);
                downloadLink.click();
            });
        }
    }
}
