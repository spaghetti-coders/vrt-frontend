import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, UntypedFormBuilder } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgxsModule, Store } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { DEFAULTPARAMETERS, IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { Spy, createSpyFromClass } from 'jest-auto-spies';
import { MockModule } from 'ng-mocks';

import { RouterModule } from '@angular/router';
import { AppEvent } from '@vrt/api';
import { AddCurrentUserToAnEvent, LoadFutureEvents, LoadPastEvents, RemoveCurrentUserFromEvent, RemoveEvent } from '@vrt/event-api';
import { VrtEventHelperServiceService } from './vrt-event-helper-service.service';

describe('VrtEventHelperServiceService', () => {
    let service: VrtEventHelperServiceService;
    let store: Spy<Store> = createSpyFromClass(Store, {
        methodsToSpyOn: ['select'],
    });

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                MockModule(MatSnackBarModule),
                MockModule(MatDialogModule),
                RouterModule.forRoot([]),
                getTranslocoModule(),
                MockModule(NgxsModule),
                HttpClientTestingModule,
            ],
            providers: [
                FormBuilder,
                VrtEventHelperServiceService,
                { provide: IS_DEMO_BUILD, useValue: false },
                { provide: Store, useValue: store },
                {
                    provide: NotificationSnackBarService,
                    useValue: createSpyFromClass(NotificationSnackBarService),
                },
                {
                    provide: UntypedFormBuilder,
                    useValue: createSpyFromClass(UntypedFormBuilder),
                },
            ],
            teardown: { destroyAfterEach: false },
        });
        service = TestBed.inject(VrtEventHelperServiceService);
        store = TestBed.inject<any>(Store);
    });

    it('should be created', waitForAsync(() => {
        expect(service).toBeTruthy();
    }));

    it('should call get past events action', () => {
        const { page, pageSize, filter, sortBy, sortOrder } = DEFAULTPARAMETERS;
        service.getPastEvents(DEFAULTPARAMETERS);
        expect(store.dispatch).toHaveBeenCalledWith(
            new LoadPastEvents({
                page,
                pageSize,
                filter,
                sortBy: 'Date',
                sortOrder: 'desc',
            }),
        );
    });

    it('should call get future events action', () => {
        service.getFutureEvents(DEFAULTPARAMETERS);
        expect(store.dispatch).toHaveBeenCalledWith(new LoadFutureEvents({ apiCallParameters: DEFAULTPARAMETERS }));
    });

    it('should delete an event', () => {
        service.deleteEvent({ id: 12 } as AppEvent);
        expect(store.dispatch).toHaveBeenCalledWith(new RemoveEvent({ id: 12 }));
    });

    it('should register for an event', () => {
        service.registerForEvent({ id: 12 } as AppEvent);
        expect(store.dispatch).toHaveBeenCalledWith(new AddCurrentUserToAnEvent({ eventId: 12 }));
    });

    it('should unregister for an event', () => {
        service.unregisterFromEvent({ id: 12 } as AppEvent, 10);
        expect(store.dispatch).toHaveBeenCalledWith(new RemoveCurrentUserFromEvent({ eventId: 12, userId: 10 }));
    });
});
