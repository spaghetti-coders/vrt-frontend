import { Pipe, PipeTransform } from '@angular/core';
import { AppEvent } from '@vrt/api';

@Pipe({
    name: 'activeRegistrationCount',
})
export class ActiveRegistrationCountPipe implements PipeTransform {
    transform(appEvent: AppEvent): number {
        if (appEvent.registrationCount !== undefined && appEvent.waitingListCount !== undefined) {
            return appEvent.registrationCount - appEvent.waitingListCount;
        }
        return appEvent.registrationCount ? appEvent.registrationCount : 0;
    }
}
