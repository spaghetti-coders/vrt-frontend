import { AppEvent } from '@vrt/api';
import { EVENT_EXAMPLE } from '@vrt/testing';
import { ActiveRegistrationCountPipe } from './active-registration-count.pipe';

describe('ActiveRegistrationCountPipe', () => {
    let pipe: ActiveRegistrationCountPipe;

    beforeEach(() => {
        pipe = new ActiveRegistrationCountPipe();
    });

    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });

    it('should transform value', () => {
        const event: AppEvent = {
            ...EVENT_EXAMPLE,
            registrationCount: 10,
            waitingListCount: 2,
        };
        expect(pipe.transform(event)).toEqual(8);
    });

    it('should return default value', () => {
        const event: AppEvent = { ...EVENT_EXAMPLE, registrationCount: undefined };
        expect(pipe.transform(event)).toEqual(0);
    });

    it('should return registrationCount value', () => {
        const event: AppEvent = {
            ...EVENT_EXAMPLE,
            registrationCount: 2,
            waitingListCount: undefined,
        };
        expect(pipe.transform(event)).toEqual(2);
    });
});
