import { Pipe, PipeTransform } from '@angular/core';
import { AppEvent } from '@vrt/api';

@Pipe({
    name: 'isEventInPast',
})
export class IsEventInPastPipe implements PipeTransform {
    transform(event: AppEvent | undefined): boolean {
        return event ? new Date(event.date) < new Date() : false;
    }
}
