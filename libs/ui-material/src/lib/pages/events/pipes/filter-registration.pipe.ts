import { Pipe, PipeTransform } from '@angular/core';
import { AppRegistration, RegistrationStatus } from '@vrt/api';

@Pipe({
    name: 'filterRegistration',
})
export class FilterRegistrationPipe implements PipeTransform {
    transform(filter: RegistrationStatus, registrations: AppRegistration[]): AppRegistration[] {
        return registrations.filter((item) => item.status === filter);
    }
}
