import { AppEvent } from '@vrt/api';
import { IsEventInPastPipe } from './is-event-passed.pipe';

describe('IsEventPassedPipe', () => {
    it('transform', () => {
        // Arrange
        const event = { date: new Date(2019, 12, 25).toISOString() } as AppEvent;

        // Act
        const result = new IsEventInPastPipe().transform(event);

        // Assert
        expect(result).toBe(true);
    });
});
