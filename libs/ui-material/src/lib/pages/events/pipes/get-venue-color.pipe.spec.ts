import { AppVenue } from '@vrt/api';
import { GetVenueColorPipe } from './get-venue-color.pipe';

const venuesMock = [
    { colorCode: '#000000', id: 22 },
    { colorCode: '#C3C3C3', id: 1 },
] as AppVenue[];

describe('GetVenueColorPipe', () => {
    it('should find venue and transform color', () => {
        // Arrange
        const venueId = 1;

        // Act
        const result = new GetVenueColorPipe().transform(venuesMock, venueId);

        // Assert
        expect(result).toEqual(venuesMock[1].colorCode);
    });

    it('should not find venue and transform color to fallback', () => {
        // Arrange
        const venueId = 23;

        // Act
        const result = new GetVenueColorPipe().transform(venuesMock, venueId);

        // Assert
        expect(result).toEqual('#fff');
    });
});
