import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'sortColumnToColor',
})
export class SortColumnToColorPipe implements PipeTransform {
    transform(value: string, sortColumn: string): string {
        return value === sortColumn ? 'accent' : 'primary';
    }
}
