import { Pipe, PipeTransform } from '@angular/core';
import { AppVenue } from '@vrt/api';

@Pipe({
    name: 'getVenueColor',
})
export class GetVenueColorPipe implements PipeTransform {
    transform(venues: AppVenue[] | undefined | null, venueId: number | undefined): string {
        const venue = venues?.find((value) => value.id === venueId);
        return venue ? venue?.colorCode : '#fff';
    }
}
