import { AppRegistration, RegistrationStatus } from '@vrt/api';
import { FilterRegistrationPipe } from './filter-registration.pipe';

// Act
const appRegistrations = [
    {
        userId: 1,
        registrationDate: new Date(Date.now()).toISOString(),
        eventId: 2,
        status: RegistrationStatus.NUMBER_1,
        userDisplayName: 'Peter',
    },
    {
        userId: 2,
        registrationDate: new Date(Date.now()).toISOString(),
        eventId: 2,
        status: RegistrationStatus.NUMBER_0,
        userDisplayName: 'Dirk',
    },
] as AppRegistration[];

const regStatus = RegistrationStatus.NUMBER_1;

describe('WaitingListPipe', () => {
    it('transform', () => {
        // Act
        const result = new FilterRegistrationPipe().transform(regStatus, appRegistrations);

        // Assert
        expect(result).toEqual([appRegistrations[0]]);
    });
});
