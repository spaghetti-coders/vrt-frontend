import { SortColumnToColorPipe } from './sort-column-to-color.pipe';

describe('SortColumnToColorPipe', () => {
    it('transform', () => {
        // Arrange
        const value = 'Test';
        const sortColumn = 'Test';

        // Act
        const result = new SortColumnToColorPipe().transform(value, sortColumn);

        // Assert
        expect(result).toBe('accent');
    });
});
