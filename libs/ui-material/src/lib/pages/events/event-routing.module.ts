import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventResolver, EventsResolver, RegistrationsResolver } from '@vrt/common';
import { NavigationComponent } from '../../atomic/navigation/navigation.component';
import { EventDetailViewComponent } from './components/event-detail-view/event-detail-view.component';
import { EventOverviewComponent } from './components/event-overview/event-overview.component';
import { EventRegistrationsComponent } from './components/event-registrations/event-registrations.component';
import { EventWrapperComponent } from './components/event-wrapper/event-wrapper.component';

const routes: Routes = [
    {
        path: '',
        component: NavigationComponent,
        children: [
            {
                path: '',
                component: EventOverviewComponent,
            },
            {
                path: 'create',
                component: EventWrapperComponent,
            },
            {
                path: 'edit/:id',
                component: EventWrapperComponent,
                resolve: {
                    event: EventResolver,
                },
            },
            {
                path: 'detail',
                component: EventDetailViewComponent,
                resolve: {
                    event: EventResolver,
                },
            },
            {
                path: 'participants/:eventId',
                component: EventRegistrationsComponent,
                resolve: {
                    events: EventsResolver,
                    event: EventResolver,
                    registrations: RegistrationsResolver,
                },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class EventRoutingModule {}
