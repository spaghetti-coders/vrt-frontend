import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';
import { TranslocoModule } from '@ngneat/transloco';
import { NgxsModule } from '@ngxs/store';
import { AdminUserState } from '@vrt/admin-api';
import { PaginatorState } from '@vrt/api';
import { EventsState } from '@vrt/event-api';
import { HasPermissionModule, SharedModule } from '@vrt/shared';
import { VenuesState } from '@vrt/venue-api';
import { ConfirmDialogModule, NavigationModule, PagingModule, UserSearchModule } from '../../atomic/index';
import { EventCardComponent } from './components/event-card/event-card.component';
import { EventDetailViewComponent } from './components/event-detail-view/event-detail-view.component';
import { EventEditViewComponent } from './components/event-edit-view/event-edit-view.component';
import { EventFilterButtonComponent } from './components/event-filter-button/event-filter-button.component';
import { CheckTotalEventCountPipe, EventOverviewComponent } from './components/event-overview/event-overview.component';
import { EventRegistrationsComponent } from './components/event-registrations/event-registrations.component';
import { EventViewComponent } from './components/event-view/event-view.component';
import { EventWrapperComponent } from './components/event-wrapper/event-wrapper.component';
import { RegistrationQuickMenuComponent } from './components/registration-quick-menu/registration-quick-menu.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { EventRoutingModule } from './event-routing.module';
import { ActiveRegistrationCountPipe } from './pipes/active-registration-count.pipe';
import { FilterRegistrationPipe } from './pipes/filter-registration.pipe';
import { GetVenueColorPipe } from './pipes/get-venue-color.pipe';
import { IsEventInPastPipe } from './pipes/is-event-passed.pipe';
import { SortColumnToColorPipe } from './pipes/sort-column-to-color.pipe';
import { VrtEventHelperServiceService } from './services/vrt-event-helper-service.service';

@NgModule({
    declarations: [
        EventOverviewComponent,
        EventDetailViewComponent,
        SignInComponent,
        EventEditViewComponent,
        EventViewComponent,
        GetVenueColorPipe,
        IsEventInPastPipe,
        EventRegistrationsComponent,
        SortColumnToColorPipe,
        FilterRegistrationPipe,
        RegistrationQuickMenuComponent,
        EventCardComponent,
        EventFilterButtonComponent,
        EventWrapperComponent,
        ActiveRegistrationCountPipe,
    ],
    imports: [
        CommonModule,
        ConfirmDialogModule,
        EventRoutingModule,
        FormsModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatSlideToggleModule,
        MatStepperModule,
        NavigationModule,
        NgxsModule.forFeature([AdminUserState, EventsState, VenuesState, PaginatorState]),
        PagingModule,
        ReactiveFormsModule,
        ScrollingModule,
        SharedModule,
        TranslocoModule,
        UserSearchModule,
        HasPermissionModule,
        CheckTotalEventCountPipe,
    ],
    providers: [VrtEventHelperServiceService],
})
export class EventModule {}
