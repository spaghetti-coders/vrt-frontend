import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Store } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { createSpyFromClass, provideAutoSpy, Spy } from 'jest-auto-spies';
import { MockComponent, MockModule } from 'ng-mocks';
import { VrtEventHelperServiceService } from '../../services/vrt-event-helper-service.service';
import { EventEditViewComponent } from '../event-edit-view/event-edit-view.component';

import { RouterModule } from '@angular/router';
import { EventModel } from '@vrt/api';
import { AddEvent, UpdateEvent } from '@vrt/event-api';
import { EVENT_EXAMPLE, VENUE_EXAMPLE } from '@vrt/testing';
import { EventData, EventWrapperComponent } from './event-wrapper.component';

describe('EventWrapperComponent', () => {
    let component: EventWrapperComponent;
    let fixture: ComponentFixture<EventWrapperComponent>;
    let store: Spy<Store>;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                getTranslocoModule(),
                FormsModule,
                ReactiveFormsModule,
                BrowserAnimationsModule,
                HttpClientTestingModule,
                RouterModule.forRoot([]),
                MockModule(MatDialogModule),
            ],
            declarations: [EventWrapperComponent, MockComponent(EventEditViewComponent)],
            providers: [
                provideAutoSpy(NotificationSnackBarService),
                VrtEventHelperServiceService,
                { provide: IS_DEMO_BUILD, useValue: false },
                {
                    provide: Store,
                    useValue: createSpyFromClass(Store, {
                        methodsToSpyOn: ['select'],
                    }),
                },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        store = TestBed.inject<any>(Store);
        store.select.nextWith();
        fixture = TestBed.createComponent(EventWrapperComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should create an event', () => {
        component.onCreateEvent(EVENT_EXAMPLE as EventModel);
        expect(store.dispatch).toHaveBeenCalledWith(new AddEvent({ eventModel: EVENT_EXAMPLE as EventModel }));
    });

    it('should update an event', () => {
        fixture.detectChanges();
        component.updateEvent(EVENT_EXAMPLE);
        expect(store.dispatch).toHaveBeenCalledWith(new UpdateEvent({ appEvent: EVENT_EXAMPLE }));
    });

    it('should init form with event from state', fakeAsync(() => {
        const initFormSpy = jest.spyOn(component, 'initForm');
        store.select.nextOneTimeWith(EVENT_EXAMPLE);
        store.select.nextOneTimeWith(VENUE_EXAMPLE);

        component.data$.subscribe(({ appEvent, venues }: EventData) => {
            expect(appEvent).toEqual(EVENT_EXAMPLE);
            expect(initFormSpy).toHaveBeenCalled();
        });
        tick();
    }));
});
