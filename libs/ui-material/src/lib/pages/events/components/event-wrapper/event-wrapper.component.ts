import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AppEvent, AppVenue, EventModel } from '@vrt/api';
import { AddEvent, EventsState, UpdateEvent } from '@vrt/event-api';
import { EventForm } from '@vrt/utils';
import { VenuesState } from '@vrt/venue-api';
import { Observable, combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { VrtEventHelperServiceService } from '../../services/vrt-event-helper-service.service';

export interface EventData {
    appEvent: AppEvent | undefined;
    venues: AppVenue[] | undefined | null;
}

@Component({
    selector: 'vrt-event-wrapper',
    templateUrl: './event-wrapper.component.html',
    styleUrls: ['./event-wrapper.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventWrapperComponent {
    eventForm?: FormGroup<EventForm>;

    vrtEvent$: Observable<AppEvent | undefined> = this.store.select(EventsState.selectedEvent).pipe(
        tap((event: AppEvent | undefined) => {
            this.initForm(event);
        }),
    );

    venues$: Observable<AppVenue[] | null | undefined> = this.store.select(VenuesState.venues).pipe();

    data$: Observable<EventData> = combineLatest([this.vrtEvent$, this.venues$]).pipe(
        map(([appEvent, venues]) => {
            return {
                appEvent,
                venues,
            };
        }),
    );

    constructor(
        private eventHelperServiceService: VrtEventHelperServiceService,
        private store: Store,
    ) {}

    onCreateEvent(eventModel: EventModel): void {
        this.store.dispatch(new AddEvent({ eventModel }));
    }

    initForm(appEvent: AppEvent | undefined) {
        this.eventForm = this.eventHelperServiceService.prepareEventForm(appEvent);
        if (appEvent === undefined) {
            this.eventForm.get('capacity')?.setValue(0);
            this.eventForm.get('capacity')?.updateValueAndValidity();
        }
    }

    updateEvent(appEvent: AppEvent): void {
        this.store.dispatch(new UpdateEvent({ appEvent }));
    }
}
