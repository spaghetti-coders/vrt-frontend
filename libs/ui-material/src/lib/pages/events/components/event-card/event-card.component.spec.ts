import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LOCALE_ID } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { UserRole } from '@vrt/utils';
import { of } from 'rxjs';
import { ConfirmDialogModule } from '../../../../atomic/index';
import { GetVenueColorPipe } from '../../pipes/get-venue-color.pipe';
import { IsEventInPastPipe } from '../../pipes/is-event-passed.pipe';
import { RegistrationQuickMenuComponent } from '../registration-quick-menu/registration-quick-menu.component';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { EVENT_EXAMPLE, USER_EXAMPLE } from '@vrt/testing';
import { EventCardComponent } from './event-card.component';
import spyOn = jest.spyOn;

describe('EventCardComponent', () => {
    let component: EventCardComponent;
    let fixture: ComponentFixture<EventCardComponent>;
    let dialog: MatDialog;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                getTranslocoModule(),
                HttpClientTestingModule,
                NgxsModule.forRoot(),
                NoopAnimationsModule,
                RouterModule.forRoot([]),
                ConfirmDialogModule,
                CommonModule,
            ],
            declarations: [EventCardComponent, GetVenueColorPipe, RegistrationQuickMenuComponent, IsEventInPastPipe],
            providers: [
                { provide: MatDialog, useValue: { open: jest.fn() } },
                {
                    provide: MatDialogRef,
                    useValue: {
                        close: jest.fn(),
                        afterClosed: jest.fn().mockReturnValue(of(true)),
                    },
                },
                { provide: UntypedFormBuilder, useValue: jest.fn() },
                { provide: LOCALE_ID, useValue: 'de-DE' },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(EventCardComponent);
        dialog = TestBed.inject(MatDialog);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should show popup to delete an event', waitForAsync(() => {
        const spy = jest.spyOn(component.deleteEvent, 'emit');
        dialog.open = jest.fn().mockReturnValue({ afterClosed: () => of(true) });

        component.onDeleteEvent(EVENT_EXAMPLE);

        expect(spy).toHaveBeenCalledTimes(1);
    }));

    it('should hide popup', waitForAsync(() => {
        const spy = jest.spyOn(component.deleteEvent, 'emit');
        dialog.open = jest.fn().mockReturnValue({ afterClosed: () => of() });

        component.onDeleteEvent(EVENT_EXAMPLE);

        expect(spy).toHaveBeenCalledTimes(0);
    }));

    it('should download log if user has role admin', () => {
        const spy = jest.spyOn(component.downloadLogFile, 'emit');
        component.user = { ...USER_EXAMPLE, role: UserRole.ADMIN };

        component.onDownloadEventLog(EVENT_EXAMPLE);

        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(EVENT_EXAMPLE);
    });

    it('should not download log if user has role manager or user', () => {
        const spy = jest.spyOn(component.downloadLogFile, 'emit');
        component.user = { ...USER_EXAMPLE, role: UserRole.MANAGER };

        component.onDownloadEventLog(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledTimes(0);

        component.user = { ...USER_EXAMPLE, role: UserRole.USER };
        fixture.detectChanges();
        component.onDownloadEventLog(EVENT_EXAMPLE);

        expect(spy).toHaveBeenCalledTimes(0);
    });

    it('should register to an event', () => {
        const spy = spyOn(component.registerToEvent, 'emit');
        component.onRegister(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledWith(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should unregister from an event', () => {
        const spy = spyOn(component.unregisterFromEvent, 'emit');
        component.onUnregister(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledWith(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledTimes(1);
    });
});
