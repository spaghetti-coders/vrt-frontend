import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AppEvent, AppUser, AppVenue } from '@vrt/api';
import { UserRole } from '@vrt/utils';
import { ConfirmDialogComponent } from '../../../../atomic/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'vrt-event-card',
    templateUrl: './event-card.component.html',
    styleUrls: ['./event-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventCardComponent {
    @Input()
    isEventInPast = false;

    @Input()
    user: AppUser | null = {} as AppUser;

    @Input()
    venues: AppVenue[] | undefined | null = [];

    @Input()
    vrtEvent: AppEvent | null = null;

    @Output()
    deleteEvent: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    @Output()
    downloadLogFile: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    @Output()
    unregisterFromEvent: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    @Output()
    registerToEvent: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    role = UserRole;

    constructor(private matDialog: MatDialog) {}

    onDeleteEvent(vrtEvent: AppEvent): void {
        const dialogRef = this.matDialog.open(ConfirmDialogComponent, {});
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.deleteEvent.emit(vrtEvent);
            }
        });
    }

    onDownloadEventLog(vrtEvent: AppEvent): void {
        if (this.user?.role === UserRole.ADMIN) {
            this.downloadLogFile.emit(vrtEvent);
        }
    }

    onRegister($event: AppEvent) {
        this.registerToEvent.emit($event);
    }

    onUnregister($event: AppEvent) {
        this.unregisterFromEvent.emit($event);
    }
}
