import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, UntypedFormGroup } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
import { AppEvent } from '@vrt/api';
import { getTranslocoModule } from '@vrt/common';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import dayjs from 'dayjs';
import { provideAutoSpy } from 'jest-auto-spies';
import { MockModule } from 'ng-mocks';
import { VrtEventHelperServiceService } from '../../services/vrt-event-helper-service.service';

import { RouterModule } from '@angular/router';
import { EVENT_EXAMPLE, VENUE_EXAMPLE } from '@vrt/testing';
import { EventEditViewComponent } from './event-edit-view.component';

describe('EventEditViewComponent', () => {
    let component: EventEditViewComponent;
    let fixture: ComponentFixture<EventEditViewComponent>;
    let eventHelperServiceService: VrtEventHelperServiceService;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                getTranslocoModule(),
                FormsModule,
                ReactiveFormsModule,
                BrowserAnimationsModule,
                HttpClientTestingModule,
                RouterModule.forRoot([]),
                MockModule(MatDialogModule),
                NgxsModule.forRoot(),
            ],
            providers: [
                provideAutoSpy(NotificationSnackBarService),
                VrtEventHelperServiceService,
                { provide: IS_DEMO_BUILD, useValue: false },
            ],
            declarations: [EventEditViewComponent],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(EventEditViewComponent);
        eventHelperServiceService = TestBed.inject(VrtEventHelperServiceService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    /*    it('should match snapshot', () => {
            expect(fixture).toMatchSnapshot();
        });*/

    it('should submit new event to state', () => {
        const spy = jest.spyOn(component.createEvent, 'emit');
        const form = eventHelperServiceService.prepareEventForm(EVENT_EXAMPLE) as UntypedFormGroup;
        form.removeControl('id');
        component.eventForm = form;

        component.onSubmit();

        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(form.value);
    });

    it('should update an existing event', () => {
        const spy = jest.spyOn(component.updateEvent, 'emit');
        const form = eventHelperServiceService.prepareEventForm(EVENT_EXAMPLE);
        component.eventForm = form;

        component.onSubmit();

        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(form.value);
    });

    it('should use event id for loop index', () => {
        expect(component.trackByFn(EVENT_EXAMPLE)).toBe(12);
    });

    it('should add venue to an event', () => {
        const form = eventHelperServiceService.prepareEventForm(EVENT_EXAMPLE);
        component.eventForm = form;

        component.selectedVenue(VENUE_EXAMPLE);

        expect(form.get('venueId')?.value).toEqual(11);
        expect(form.get('capacity')?.value).toEqual(23);
    });

    it('should display', () => {
        component.venues = [{ ...VENUE_EXAMPLE }, { ...VENUE_EXAMPLE, id: 987, name: 'Example Venue 123' }];

        expect(component.displayFn(11)).toEqual(VENUE_EXAMPLE.name);
        expect(component.displayFn(VENUE_EXAMPLE)).toEqual(VENUE_EXAMPLE.name);
    });

    it('should set new date from date and time input', () => {
        component.eventForm = eventHelperServiceService.prepareEventForm({} as AppEvent);
        fixture.detectChanges();

        const date = new Date();
        const time = '12:00';
        component.eventForm?.get('date')?.setValue(date.toDateString());
        component.eventForm?.get('time')?.setValue(time);
        fixture.detectChanges();

        component.onChangeTime();
        expect(component.eventForm?.get('date')?.value).toEqual(dayjs(`${new Date(date).toDateString()} ${time}`).toISOString());
    });
});
