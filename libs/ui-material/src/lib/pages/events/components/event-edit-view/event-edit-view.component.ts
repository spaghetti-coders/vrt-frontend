import { animate, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { AppEvent, AppVenue, EventModel } from '@vrt/api';
import dayjs from 'dayjs';

@Component({
    selector: 'vrt-event-edit-view',
    templateUrl: './event-edit-view.component.html',
    styleUrls: ['./event-edit-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('fadeSlideInOut', [
            transition(':enter', [
                style({ opacity: 0, transform: 'translateY(10px)' }),
                animate('500ms', style({ opacity: 1, transform: 'translateY(0)' })),
            ]),
            transition(':leave', [animate('500ms', style({ opacity: 0, transform: 'translateY(10px)' }))]),
        ]),
    ],
})
export class EventEditViewComponent {
    @Input()
    eventForm?: UntypedFormGroup;

    @Input()
    venues: AppVenue[] | null | undefined = [];

    @Output()
    createEvent: EventEmitter<EventModel> = new EventEmitter<EventModel>();

    @Output()
    updateEvent: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    displayFn(element: number | AppVenue): string {
        let venue: AppVenue | undefined;
        if (typeof element === 'number') {
            venue = this.venues?.find((value) => value.id === element);
        } else {
            venue = element;
        }
        return venue && venue?.name ? venue.name : '';
    }

    selectedVenue(venue: AppVenue): void {
        this.eventForm?.get('venueId')?.setValue(venue.id);
        this.eventForm?.get('capacity')?.setValue(venue.capacity);
    }

    trackByFn(item: any): any {
        return item.id;
    }

    onChangeTime() {
        this.setTime();
        this.eventForm?.updateValueAndValidity();
    }

    onSubmit() {
        this.setTime();
        if (this.eventForm?.get('id')?.value) {
            this.updateEvent.emit(this.eventForm?.value);
        } else {
            this.createEvent.emit(this.eventForm?.value);
        }
    }

    private setTime() {
        const date: Date = this.eventForm?.get('date')?.value;
        const time: Date = this.eventForm?.get('time')?.value;
        const newDate = dayjs(`${new Date(date).toDateString()} ${time}`).toISOString();
        this.eventForm?.get('date')?.setValue(newDate);
    }
}
