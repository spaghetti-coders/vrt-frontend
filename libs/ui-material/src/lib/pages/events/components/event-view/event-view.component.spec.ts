import { EVENT_EXAMPLE, USER_EXAMPLE, VENUE_EXAMPLE } from '@vrt/testing';
import { EventViewComponent } from './event-view.component';
import spyOn = jest.spyOn;

describe('EventRoleViewComponent', () => {
    let component: EventViewComponent;

    beforeEach(() => {
        component = new EventViewComponent();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should display events', () => {
        component.events = [{ ...EVENT_EXAMPLE }, { ...EVENT_EXAMPLE, id: 123 }];
        component.venues = [{ ...VENUE_EXAMPLE }];
        component.user = { ...USER_EXAMPLE };

        expect(component.events.length).toBeGreaterThanOrEqual(2);
    });

    it('should emit an event to event', () => {
        const spy = spyOn(component.deleteEvent, 'emit');
        component.onDeleteEvent(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledWith(EVENT_EXAMPLE);
    });

    it('should emit an event to download the event log', () => {
        const spy = spyOn(component.downloadLog, 'emit');
        component.onDownloadLog(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledWith(EVENT_EXAMPLE);
    });

    it('should track lists and return item id', () => {
        expect(component.trackByFn(1, EVENT_EXAMPLE)).toEqual(12);
    });

    it('should emit event if a user unregister from an event', () => {
        const spy = jest.spyOn(component.unregister, 'emit');
        component.onUnregister(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledWith(EVENT_EXAMPLE);
    });

    it('should emit event if a user register to an event', () => {
        const spy = jest.spyOn(component.register, 'emit');
        component.onRegister(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledWith(EVENT_EXAMPLE);
    });
});
