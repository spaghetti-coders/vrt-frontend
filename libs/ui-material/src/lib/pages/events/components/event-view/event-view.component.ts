import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AppEvent, AppUser, AppVenue } from '@vrt/api';

@Component({
    selector: 'vrt-event-view',
    templateUrl: './event-view.component.html',
    styleUrls: ['./event-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventViewComponent {
    @Input()
    events: AppEvent[] | undefined = [];

    @Input()
    user: AppUser | null = {} as AppUser;

    @Input()
    venues: AppVenue[] | undefined | null = [];

    @Output()
    deleteEvent: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    @Output()
    downloadLog: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    @Output()
    unregister: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    @Output()
    register: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    trackByFn(index: number, item: any): any {
        return item.id;
    }

    onDeleteEvent($event: AppEvent) {
        this.deleteEvent.emit($event);
    }

    onDownloadLog($event: AppEvent) {
        this.downloadLog.emit($event);
    }

    onUnregister($event: AppEvent) {
        this.unregister.emit($event);
    }

    onRegister($event: AppEvent) {
        this.register.emit($event);
    }
}
