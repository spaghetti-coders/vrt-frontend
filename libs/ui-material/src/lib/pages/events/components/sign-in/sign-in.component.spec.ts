import { ComponentFixture, TestBed } from '@angular/core/testing';
import { getTranslocoModule } from '@vrt/common';

import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { SignInComponent } from './sign-in.component';

describe('SignInComponent', () => {
    let component: SignInComponent;
    let fixture: ComponentFixture<SignInComponent>;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [SignInComponent],
            imports: [MatDialogModule, getTranslocoModule()],
            providers: [{ provide: MAT_DIALOG_DATA, useValue: { data: 'anmelden' } }],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(SignInComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
