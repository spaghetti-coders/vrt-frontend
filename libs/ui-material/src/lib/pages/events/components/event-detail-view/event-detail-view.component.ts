import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngxs/store';
import { AppEvent, AppRegistration, AppUser } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { EventsState, RemoveEvent } from '@vrt/event-api';
import { Observable } from 'rxjs';
import { ConfirmDialogComponent } from '../../../../atomic/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'vrt-event-detail-view',
    templateUrl: './event-detail-view.component.html',
    styleUrls: ['./event-detail-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventDetailViewComponent {
    selectedAppEvent$: Observable<AppEvent | undefined> = this.store.select(EventsState.selectedEvent);

    registrations$: Observable<AppRegistration[] | undefined | null> = this.store.select(EventsState.registrations);

    currentUser$: Observable<AppUser | null> = this.authenticationService.currentUser$;

    constructor(
        private matDialog: MatDialog,
        private authenticationService: AuthenticationService,
        private store: Store,
    ) {}

    deleteEvent(vrtEvent: AppEvent): void {
        const dialogRef = this.matDialog.open(ConfirmDialogComponent, {});
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.store.dispatch(new RemoveEvent({ id: vrtEvent.id as number }));
            }
        });
    }
}
