import { waitForAsync } from '@angular/core/testing';
import { Store } from '@ngxs/store';
import { AuthenticationService } from '@vrt/auth';
import { createSpyFromClass, Spy } from 'jest-auto-spies';
import { of } from 'rxjs';

import { MatDialog } from '@angular/material/dialog';
import { RemoveEvent } from '@vrt/event-api';
import { EVENT_EXAMPLE } from '@vrt/testing';
import { EventDetailViewComponent } from './event-detail-view.component';

describe('EventDetailViewComponent', () => {
    let component: EventDetailViewComponent;
    let authService: Spy<AuthenticationService>;
    let dialog: Spy<MatDialog>;
    let store: Spy<Store>;

    beforeEach(() => {
        dialog = createSpyFromClass(MatDialog);
        store = createSpyFromClass(Store);
        authService = createSpyFromClass(AuthenticationService);
        component = new EventDetailViewComponent(dialog, authService, store);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should delete an event from state', waitForAsync(() => {
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    items: [{ ...EVENT_EXAMPLE }, { ...EVENT_EXAMPLE, id: 15 }],
                },
            },
        });
        dialog.open.mockReturnValue({ afterClosed: () => of(true) } as any);
        component.deleteEvent(EVENT_EXAMPLE);

        expect(store.dispatch).toHaveBeenCalledWith(new RemoveEvent({ id: 12 }));
    }));
});
