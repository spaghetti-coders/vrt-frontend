import { CommonModule, Location } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, UntypedFormControl } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { PageEvent } from '@angular/material/paginator';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule, Store } from '@ngxs/store';
import { AppRegistration, EventRegistrationsService, RegistrationStatus } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { getTranslocoModule } from '@vrt/common';
import { HasPermissionModule } from '@vrt/shared';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { provideAutoSpy } from 'jest-auto-spies';
import { MockComponent, MockModule, MockPipe } from 'ng-mocks';
import { of } from 'rxjs';
import { PagingModule } from '../../../../atomic/index';
import { IsEventInPastPipe } from '../../pipes/is-event-passed.pipe';
import { SortColumnToColorPipe } from '../../pipes/sort-column-to-color.pipe';
import { VrtEventHelperServiceService } from '../../services/vrt-event-helper-service.service';
import { RegistrationQuickMenuComponent } from '../registration-quick-menu/registration-quick-menu.component';

import { RouterModule } from '@angular/router';
import { AddUserToAnEvent, EventsState, FindRegistration, RemoveCurrentUserFromEvent, RemoveUserFromEvent } from '@vrt/event-api';
import { EVENT_EXAMPLE, EXAMPLE_REGISTRATIONS, USER_EXAMPLE } from '@vrt/testing';
import { EventRegistrationsComponent } from './event-registrations.component';
import fn = jest.fn;
import spyOn = jest.spyOn;

describe('EventRegistrationsComponent', () => {
    let component: EventRegistrationsComponent;
    let fixture: ComponentFixture<EventRegistrationsComponent>;
    let dialog: MatDialog;
    let location: Location;
    let store: Store;
    let changeDetectorRef: ChangeDetectorRef;
    let registrationsService: EventRegistrationsService;
    let authService: AuthenticationService;
    let eventHelperServiceService: VrtEventHelperServiceService;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                CommonModule,
                ReactiveFormsModule,
                RouterModule.forRoot([]),
                getTranslocoModule(),
                HttpClientTestingModule,
                BrowserAnimationsModule,
                MockModule(MatSnackBarModule),
                MockModule(HasPermissionModule),
                MockModule(PagingModule),
                MockModule(MatMenuModule),
                MockModule(MatFormFieldModule),
                MockModule(MatIconModule),
                MockModule(MatDialogModule),
                MockModule(MatButtonModule),
                MockModule(MatCardModule),
                NgxsModule.forRoot([EventsState], { developmentMode: true }),
            ],
            declarations: [
                EventRegistrationsComponent,
                MockPipe(SortColumnToColorPipe),
                MockPipe(IsEventInPastPipe),
                MockComponent(RegistrationQuickMenuComponent),
            ],
            providers: [
                provideAutoSpy(NotificationSnackBarService),
                VrtEventHelperServiceService,
                { provide: IS_DEMO_BUILD, useValue: false },
                { provide: MatDialog, useValue: { open: jest.fn() } },
                { provide: ChangeDetectorRef, useValue: jest.fn() },
                {
                    provide: MatDialogRef,
                    useValue: {
                        close: jest.fn(),
                        afterClosed: jest.fn().mockReturnValue(of(true)),
                    },
                },
                {
                    provide: UntypedFormControl,
                    useValue: { valueChanges: of('Example') },
                },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(EventRegistrationsComponent);
        location = TestBed.inject(Location);
        store = TestBed.inject(Store);
        authService = TestBed.inject(AuthenticationService);
        dialog = TestBed.inject(MatDialog);
        changeDetectorRef = TestBed.inject(ChangeDetectorRef);
        registrationsService = TestBed.inject(EventRegistrationsService);
        eventHelperServiceService = TestBed.inject(VrtEventHelperServiceService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should add an user to an event', waitForAsync(() => {
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    items: [{ ...EVENT_EXAMPLE }],
                },
                appRegistrationPagedResult: {
                    items: [...EXAMPLE_REGISTRATIONS],
                },
            },
        });
        registrationsService.addRegistration = fn().mockReturnValue(of(USER_EXAMPLE));
        const spy = spyOn(store, 'dispatch');
        component.vrtEvent = { ...EVENT_EXAMPLE };
        component.selectedUser = { ...USER_EXAMPLE };
        component.addUserToEvent();

        expect(spy).toHaveBeenCalledWith(
            new AddUserToAnEvent({
                eventId: EVENT_EXAMPLE.id ?? 0,
                userId: USER_EXAMPLE.id ?? 0,
            }),
        );
        expect(store.selectSnapshot(EventsState.registrations)?.includes(USER_EXAMPLE as AppRegistration)).toBeTruthy();
    }));

    it('should return current user', waitForAsync(() => {
        authService.currentUser$ = of({ ...USER_EXAMPLE });

        component.ngOnInit();
        component.data$?.subscribe(() => {
            expect(component.currentUser).toEqual(USER_EXAMPLE);
        });
    }));

    it('should destroy component', () => {
        const destroyReset = component.destroy.complete;
        component.destroy.complete = jest.fn();
        component.ngOnDestroy();
        expect(component.destroy.complete).toHaveBeenCalledTimes(1);
        component.destroy.subscribe((value) => expect(value).toHaveBeenCalledWith(true));
        component.destroy.complete = destroyReset;
    });

    it('should navigate back to previous page', () => {
        const spy = spyOn(location, 'back');

        component.back();

        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should remove user from an event', () => {
        const spy = spyOn(store, 'dispatch');
        const registration = { ...EXAMPLE_REGISTRATIONS[0] };
        changeDetectorRef.markForCheck = fn();
        dialog.open = jest.fn().mockReturnValue({ afterClosed: () => of(true) });
        component.vrtEvent = { ...EVENT_EXAMPLE };

        component.removeUserFromEvent(registration);

        expect(spy).toHaveBeenCalledWith(
            new RemoveUserFromEvent({
                eventId: EVENT_EXAMPLE.id ?? 0,
                userId: registration.userId ?? 0,
            }),
        );
    });

    it('should add user to an event', () => {
        store.dispatch = jest.fn();
        component.vrtEvent = EVENT_EXAMPLE;
        component.selectedUser = USER_EXAMPLE;
        component.addUserToEvent();

        expect(store.dispatch).toHaveBeenCalledWith(
            new AddUserToAnEvent({
                eventId: EVENT_EXAMPLE.id ?? 0,
                userId: USER_EXAMPLE.id ?? 0,
            }),
        );
        jest.restoreAllMocks();
    });

    it('should remove current user from an event', () => {
        const spy = spyOn(store, 'dispatch');
        const registration = { ...EXAMPLE_REGISTRATIONS[0] };
        component.currentUser = { ...USER_EXAMPLE, id: 10 };
        changeDetectorRef.markForCheck = fn();
        dialog.open = jest.fn().mockReturnValue({ afterClosed: () => of(true) });
        component.vrtEvent = { ...EVENT_EXAMPLE };

        component.removeUserFromEvent(registration);

        expect(spy).toHaveBeenCalledWith(
            new RemoveCurrentUserFromEvent({
                eventId: EVENT_EXAMPLE.id ?? 0,
                userId: registration.userId ?? 0,
            }),
        );
    });

    it('should trigger search methode on page change', () => {
        const search = component.search;
        component.search = fn();
        const pageEvent = {
            pageIndex: 1,
            pageSize: 10,
            previousPageIndex: 0,
            length: 99,
        } as PageEvent;

        component.onPageChange(pageEvent);

        expect(component.search).toHaveBeenCalledWith(pageEvent);

        component.search = search;
    });

    it('should query new events after search input', () => {
        component.vrtEvent = { ...EVENT_EXAMPLE };
        const spy = spyOn(store, 'dispatch');
        const pageEvent = {
            pageIndex: 1,
            pageSize: 10,
            previousPageIndex: 0,
            length: 99,
        } as PageEvent;

        component.regStatus = RegistrationStatus.NUMBER_0;
        component.filter = '';

        component.search(pageEvent);

        expect(spy).toHaveBeenCalledWith(
            new FindRegistration({
                registrationsApiParameters: {
                    eventId: EVENT_EXAMPLE.id ?? 0,
                    page: pageEvent.pageIndex,
                    pageSize: pageEvent.pageSize,
                    status: RegistrationStatus.NUMBER_0,
                    filter: '',
                    sortBy: component.sortBy,
                    sortOrder: component.sortOrders.get(component.sortBy),
                },
            }),
        );
    });

    it('should query select registration status', () => {
        const search = component.search;
        component.search = fn();

        component.querySelectRegStatus(RegistrationStatus.NUMBER_1);

        expect(component.regStatus).toBe(RegistrationStatus.NUMBER_1);
        expect(component.search).toHaveBeenCalledTimes(1);

        component.search = search;
    });

    it('should toggle sort order', () => {
        const search = component.search;

        component.search = fn();

        component.toggleSortOrder('RegistrationDate');
        expect(component.sortOrders.get(component.sortBy)).toBe('desc');

        component.toggleSortOrder('RegistrationDate');
        expect(component.sortOrders.get(component.sortBy)).toBe('asc');

        component.sortBy = 'Status';
        component.toggleSortOrder('RegistrationDate');

        expect(component.sortOrders.get(component.sortBy)).toBe('asc');
        expect(component.sortBy).toBe('RegistrationDate');

        component.search = search;
    });

    it('should register to an event', () => {
        eventHelperServiceService.registerForEvent = jest.fn();

        component.onRegister(EVENT_EXAMPLE);

        expect(eventHelperServiceService.registerForEvent).toHaveBeenCalledWith(EVENT_EXAMPLE);
        expect(eventHelperServiceService.registerForEvent).toHaveBeenCalledTimes(1);
    });

    it('should unregister from an event', () => {
        const user = { ...USER_EXAMPLE };
        eventHelperServiceService.unregisterFromEvent = jest.fn();
        component.currentUser = { ...user };

        component.onUnregister(EVENT_EXAMPLE);

        expect(eventHelperServiceService.unregisterFromEvent).toHaveBeenCalledTimes(1);
        expect(eventHelperServiceService.unregisterFromEvent).toHaveBeenCalledWith(EVENT_EXAMPLE, user.id);
    });

    it('should show user search', () => {
        component.onShowUserSearch(true);
        expect(component.isUserSearchActive).toBeTruthy();
    });

    it('should select user on input change', () => {
        component.onUserSelectChanged(USER_EXAMPLE);
        expect(component.selectedUser).toStrictEqual(USER_EXAMPLE);
    });

    it('should search a new user', () => {
        const spy = spyOn(store, 'dispatch');

        component.onUserSearch('Peter');

        expect(spy).toHaveBeenCalledWith({
            payload: {
                filter: 'Peter',
                isAdmin: false,
                page: undefined,
                pageSize: 100,
                sortBy: undefined,
                sortOrder: undefined,
            },
        });
    });

    it('should reset user state', () => {
        const spy = spyOn(store, 'dispatch');

        component.onUserSearch('de');
        expect(spy).toHaveBeenCalledWith({ payload: { status: 1 } });
    });
});
