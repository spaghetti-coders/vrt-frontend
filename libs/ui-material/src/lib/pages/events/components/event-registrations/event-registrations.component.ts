import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Location } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Select, Store } from '@ngxs/store';
import { AdminUserState, ChangeUserStatusQuery, LoadUsers, ResetUserState } from '@vrt/admin-api';
import { AppEvent, AppRegistration, AppUser, Metadata, RegistrationStatus, SetPageSize, UserStatus } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { FeatureState } from '@vrt/common';
import { AddUserToAnEvent, EventsState, FindRegistration, RemoveCurrentUserFromEvent, RemoveUserFromEvent } from '@vrt/event-api';
import { fadeInOut } from '@vrt/utils';
import { Observable, Subject, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { VrtEventHelperServiceService } from '../../services/vrt-event-helper-service.service';
import { SignInComponent } from '../sign-in/sign-in.component';

interface RegistrationData {
    isHandset: boolean;
    registrations: AppRegistration[] | undefined | null;
    user: AppUser | null;
    vrtEvent: AppEvent | undefined;
    users: AppUser[] | undefined | null;
}

@Component({
    selector: 'vrt-event-registrations',
    templateUrl: './event-registrations.component.html',
    styleUrls: ['./event-registrations.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [fadeInOut],
})
export class EventRegistrationsComponent implements OnInit, OnDestroy {
    @Select(FeatureState.isLoading)
    isLoading$!: Observable<boolean>;

    currentUser?: AppUser;

    data$?: Observable<RegistrationData>;

    metaData$: Observable<Metadata> = this.store
        .select(EventsState.registrationMetadata)
        .pipe(filter((metaData: Metadata | undefined): metaData is Metadata => !!metaData));

    destroy: Subject<boolean> = new Subject<boolean>();

    filter: string | undefined = undefined;

    isUserSearchActive = false;

    regStatus: RegistrationStatus | undefined;

    searchResult$?: Observable<any>;

    searchForm: UntypedFormControl = new UntypedFormControl();

    sortBy = 'RegistrationDate';

    sortOrders = new Map([
        ['UserDisplayName', 'asc'],
        ['RegistrationDate', 'asc'],
        ['Status', 'asc'],
    ]);

    vrtEvent?: AppEvent;

    selectedUser: AppUser | undefined;

    constructor(
        private location: Location,
        private matDialog: MatDialog,
        private authenticationService: AuthenticationService,
        private changeDetectorRef: ChangeDetectorRef,
        private breakpointObserver: BreakpointObserver,
        private store: Store,
        private eventHelperServiceService: VrtEventHelperServiceService,
    ) {}

    ngOnDestroy(): void {
        this.destroy.next(true);
        this.destroy.complete();
    }

    ngOnInit(): void {
        this.searchResult$ = this.searchForm?.valueChanges.pipe(
            debounceTime(500),
            distinctUntilChanged(),
            tap((value) => {
                if (this.vrtEvent?.id) {
                    this.filter = value;
                    this.search();
                }
            }),
        );

        const vrtEvent$: Observable<AppEvent | undefined> = this.store.select(EventsState.selectedEvent).pipe(
            tap((vrtEvent) => {
                if (vrtEvent) {
                    this.vrtEvent = { ...vrtEvent };
                    this.changeDetectorRef.detectChanges();
                }
            }),
        );
        const isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
            map((result) => result.matches),
            shareReplay(),
        );
        const registrations$: Observable<AppRegistration[] | undefined | null> = this.store.select(EventsState.registrations);
        const user$: Observable<AppUser | null> = this.authenticationService.currentUser$.pipe(
            tap((user) => {
                if (user) {
                    this.currentUser = { ...user };
                }
            }),
        );

        const users$: Observable<AppUser[] | undefined | null> = this.store.select(AdminUserState.users);

        this.data$ = combineLatest([vrtEvent$, isHandset$, registrations$, user$, users$]).pipe(
            map(([vrtEvent, isHandset, registrations, user, users]) => {
                return { vrtEvent, isHandset, registrations, user, users };
            }),
        );
    }

    back(): void {
        this.location.back();
    }

    removeUserFromEvent(registration: AppRegistration): void {
        const dialogRef = this.matDialog.open(SignInComponent, {
            data: { text: 'admin' },
        });
        dialogRef
            .afterClosed()
            .pipe(takeUntil(this.destroy))
            .subscribe((result) => {
                if (result) {
                    if (registration.userId && this.vrtEvent?.id) {
                        if (this.currentUser?.id === registration.userId) {
                            this.store.dispatch(
                                new RemoveCurrentUserFromEvent({
                                    eventId: this.vrtEvent.id,
                                    userId: registration.userId,
                                }),
                            );
                        } else {
                            this.store.dispatch(
                                new RemoveUserFromEvent({
                                    eventId: this.vrtEvent.id,
                                    userId: registration.userId,
                                }),
                            );
                        }
                        this.changeDetectorRef.markForCheck();
                    }
                }
            });
    }

    addUserToEvent(): void {
        if (this.vrtEvent?.id && this.selectedUser?.id) {
            this.store.dispatch(
                new AddUserToAnEvent({
                    eventId: this.vrtEvent.id,
                    userId: this.selectedUser.id,
                }),
            );
            this.selectedUser = undefined;
            this.isUserSearchActive = false;
        }
    }

    onShowUserSearch(show: boolean) {
        this.isUserSearchActive = show;
    }

    onUserSelectChanged(user: AppUser): void {
        this.selectedUser = { ...user };
    }

    sortOrderIcon(column: string): string {
        return this.sortOrders.get(column) ?? 'asc';
    }

    onPageChange($event: PageEvent): void {
        this.search($event);
    }

    search(pageEvent?: PageEvent): void {
        if (this.vrtEvent?.id) {
            this.store.dispatch(
                new FindRegistration({
                    registrationsApiParameters: {
                        eventId: this.vrtEvent.id,
                        page: pageEvent ? pageEvent.pageIndex : undefined,
                        pageSize: pageEvent ? pageEvent.pageSize : undefined,
                        status: this.regStatus,
                        filter: this.filter,
                        sortBy: this.sortBy,
                        sortOrder: this.sortOrders.get(this.sortBy),
                    },
                }),
            );
            this.store.dispatch(new SetPageSize(pageEvent?.pageSize ?? 10));
        }
    }

    onUserSearch(value: string) {
        if (value.length >= 3 && value !== this.currentUser?.displayName) {
            this.store.dispatch(
                new LoadUsers({
                    page: undefined,
                    pageSize: 100,
                    filter: value,
                    sortBy: undefined,
                    sortOrder: undefined,
                    isAdmin: this.authenticationService.isAdmin(),
                }),
            );
        } else {
            this.store.dispatch(new ResetUserState());
            this._setUserStateToActiveUser();
        }
    }

    querySelectRegStatus(regStatusFilter: RegistrationStatus): void {
        this.regStatus = regStatusFilter;
        this.search();
    }

    toggleSortOrder(column: string): void {
        if (this.sortBy === column) {
            const sortOrder = this.sortOrders.get(column);
            this.sortOrders.set(column, sortOrder === 'asc' ? 'desc' : 'asc');
        } else {
            this.sortOrders.set(this.sortBy, 'asc');
            this.sortBy = column;
        }
        this.search();
    }

    onRegister($event: AppEvent) {
        this.eventHelperServiceService.registerForEvent($event);
    }

    onUnregister($event: AppEvent) {
        if (this.currentUser?.id) {
            this.eventHelperServiceService.unregisterFromEvent($event, this.currentUser.id);
        }
    }

    private _setUserStateToActiveUser(): void {
        this.store.dispatch(new ChangeUserStatusQuery({ status: UserStatus.NUMBER_1 }));
    }
}
