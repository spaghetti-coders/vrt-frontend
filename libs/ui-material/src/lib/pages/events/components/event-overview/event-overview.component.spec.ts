import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, UntypedFormBuilder } from '@angular/forms';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule, Store } from '@ngxs/store';
import { render } from '@testing-library/angular';
import { RenderComponentOptions } from '@testing-library/angular/src/lib/models';
import { EventsService } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { FeatureState, getTranslocoModule } from '@vrt/common';
import { HasPermissionModule } from '@vrt/shared';
import { DEFAULTPARAMETERS, IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { provideAutoSpy } from 'jest-auto-spies';
import { MockModule } from 'ng-mocks';
import { of } from 'rxjs';
import { PagingComponent } from '../../../../atomic/paging/paging.component';
import { GetVenueColorPipe } from '../../pipes/get-venue-color.pipe';
import { IsEventInPastPipe } from '../../pipes/is-event-passed.pipe';
import { VrtEventHelperServiceService } from '../../services/vrt-event-helper-service.service';
import { EventCardComponent } from '../event-card/event-card.component';
import { EventFilterButtonComponent } from '../event-filter-button/event-filter-button.component';
import { EventViewComponent } from '../event-view/event-view.component';
import { RegistrationQuickMenuComponent } from '../registration-quick-menu/registration-quick-menu.component';

import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { ChangeFilteredState, EventsState, LoadAvailableEvents, LoadFutureEvents, SetEvent } from '@vrt/event-api';
import { createFileFromMockFile, EVENT_EXAMPLE, USER_EXAMPLE } from '@vrt/testing';
import { CheckTotalEventCountPipe, EventOverviewComponent } from './event-overview.component';

describe('EventOverviewComponent', () => {
    const setup = async (renderOptions?: RenderComponentOptions<EventOverviewComponent>) => render(EventOverviewComponent, renderOptions);

    console.error = jest.fn();

    describe('Integration Test', () => {
        let component: EventOverviewComponent;
        let fixture: ComponentFixture<EventOverviewComponent>;
        let helperService: VrtEventHelperServiceService;
        let eventsService: EventsService;
        let authenticationService: AuthenticationService;
        let store: Store;

        beforeEach(async () => {
            const renderResult = await setup({
                declarations: [
                    EventOverviewComponent,
                    EventViewComponent,
                    PagingComponent,
                    EventCardComponent,
                    GetVenueColorPipe,
                    RegistrationQuickMenuComponent,
                    IsEventInPastPipe,
                    EventFilterButtonComponent,
                ],
                imports: [
                    NoopAnimationsModule,
                    LayoutModule,
                    HttpClientTestingModule,
                    CheckTotalEventCountPipe,
                    MockModule(MatDialogModule),
                    MockModule(MatSlideToggleModule),
                    MockModule(HasPermissionModule),
                    MockModule(MatPaginatorModule),
                    getTranslocoModule(),
                    RouterModule.forRoot([]),
                    NgxsModule.forRoot([EventsState, FeatureState], {
                        developmentMode: true,
                    }),
                    CommonModule,
                ],
                providers: [
                    FormBuilder,
                    VrtEventHelperServiceService,
                    provideAutoSpy(NotificationSnackBarService),
                    { provide: IS_DEMO_BUILD, useValue: false },
                    { provide: UntypedFormBuilder, useValue: jest.fn() },
                ],
            });

            fixture = renderResult.fixture;
            component = renderResult.fixture.componentInstance;
        });

        beforeEach(() => {
            helperService = TestBed.inject(VrtEventHelperServiceService);
            eventsService = TestBed.inject(EventsService);
            store = TestBed.inject(Store);
            authenticationService = TestBed.inject(AuthenticationService);
            component.venueFilter = undefined;
        });

        afterEach(() => {
            jest.resetAllMocks();
        });

        it('should call the methode to download the log file for a given event', waitForAsync(() => {
            const file = createFileFromMockFile({
                body: 'test',
                mimeType: 'text/plain',
                name: 'test.txt',
            });
            const spy = jest.spyOn(helperService, 'downloadEventLog');
            window.URL.createObjectURL = jest.fn();
            jest.spyOn(eventsService, 'exportEventDetails').mockReturnValue(of(file));

            component.onDownloadLog(EVENT_EXAMPLE);

            expect(spy).toHaveBeenCalledWith(EVENT_EXAMPLE);
            expect(eventsService.exportEventDetails).toHaveBeenCalledTimes(1);
        }));

        it('should load events on venue change for role users', () => {
            const spy = jest.spyOn(store, 'dispatch');
            jest.spyOn(authenticationService, 'isInRole').mockReturnValue(true);

            component.onVenueSelectionChange('Example');
            expect(component.venueFilter).toEqual('Example');
            expect(spy).toHaveBeenCalledWith(
                new LoadFutureEvents({
                    apiCallParameters: { ...DEFAULTPARAMETERS, filter: 'Example' },
                }),
            );
        });

        it('should load events on venue change for normal users', () => {
            const spy = jest.spyOn(store, 'dispatch');
            jest.spyOn(authenticationService, 'isInRole').mockReturnValue(false);

            component.onVenueSelectionChange('Example');

            expect(spy).toHaveBeenCalledWith(
                new LoadAvailableEvents({
                    apiCallParameters: { ...DEFAULTPARAMETERS, filter: 'Example' },
                }),
            );
        });

        it('should load fast past events on toggle switch', () => {
            jest.spyOn(helperService, 'getPastEvents').mockReturnValue();
            store.reset({
                appEvents: {
                    loaded: true,
                },
            });
            component.changeToggleState(false);

            expect(helperService.getPastEvents).toHaveBeenCalledTimes(1);
            expect(helperService.getPastEvents).toHaveBeenCalledWith({
                page: 1,
                pageSize: 10,
                filter: undefined,
            });
        });

        it('should load future events on toggle switch', () => {
            jest.spyOn(helperService, 'getFutureEvents').mockReturnValue();
            jest.spyOn(helperService, 'getPastEvents').mockReturnValue();
            store.reset({
                appEvents: {
                    loaded: true,
                },
            });
            component.changeToggleState(true);

            expect(helperService.getFutureEvents).toHaveBeenCalledTimes(1);
            expect(helperService.getFutureEvents).toHaveBeenCalledWith({
                ...DEFAULTPARAMETERS,
                page: 1,
                pageSize: 10,
            });
        });

        it('should load available events on page change', () => {
            jest.spyOn(authenticationService, 'isInRole').mockReturnValue(true);
            const spy = jest.spyOn(store, 'dispatch');

            component.pageOptionChanged({ pageSize: 25, pageIndex: 2, previousPageIndex: 1, length: 99 }, true);

            expect(spy).toHaveBeenCalledTimes(2);
            expect(spy).toHaveBeenCalledWith(
                new LoadAvailableEvents({
                    apiCallParameters: {
                        pageSize: 25,
                        page: 2,
                        sortOrder: 'asc',
                        filter: undefined,
                        sortBy: undefined,
                    },
                }),
            );
        });

        it('should load past events on page change', () => {
            const getPastEventsSpy = jest.spyOn(helperService, 'getPastEvents').mockReturnValue();
            jest.spyOn(authenticationService, 'isInRole').mockReturnValue(false);
            store.reset({
                appEvents: {
                    loaded: true,
                },
            });

            component.pageOptionChanged({ pageSize: 25, pageIndex: 2, previousPageIndex: 1, length: 99 }, false);

            expect(getPastEventsSpy).toHaveBeenCalledTimes(1);
            expect(getPastEventsSpy).toHaveBeenCalledWith({
                pageSize: 25,
                page: 2,
                filter: undefined,
                sortColumn: undefined,
                sortOrder: undefined,
            });
        });

        it('should load future events on page change', () => {
            const getFutureEventsSpy = jest.spyOn(helperService, 'getFutureEvents').mockReturnValue();
            jest.spyOn(authenticationService, 'isInRole').mockReturnValue(false);

            component.pageOptionChanged({ pageSize: 25, pageIndex: 2, previousPageIndex: 1 } as PageEvent, true);

            expect(getFutureEventsSpy).toHaveBeenCalledTimes(1);
            expect(getFutureEventsSpy).toHaveBeenCalledWith({
                ...DEFAULTPARAMETERS,
                page: 2,
                pageSize: 25,
            });
        });

        it('should change toggle state to false after view init', () => {
            const changeToggleStateSpy = jest.spyOn(component, 'changeToggleState');
            jest.spyOn(store, 'selectSnapshot').mockReturnValue(false);
            jest.spyOn(authenticationService, 'isInRole').mockReturnValue(true);
            component.ngAfterViewInit();

            expect(changeToggleStateSpy).toHaveBeenCalledWith(false);
            expect(changeToggleStateSpy).toHaveBeenCalledTimes(1);
        });

        it('should delete an event', () => {
            const deleteEventSpy = jest.spyOn(helperService, 'deleteEvent').mockReturnValue();

            component.onDeleteEvent(EVENT_EXAMPLE);

            expect(deleteEventSpy).toHaveBeenCalledWith(EVENT_EXAMPLE);
            expect(deleteEventSpy).toHaveBeenCalledTimes(1);
        });

        it('should reset store', () => {
            const dispatchSpy = jest.spyOn(store, 'dispatch');

            component.resetEventStore();

            expect(dispatchSpy).toHaveBeenCalledWith(new SetEvent({ appEvent: undefined }));
            expect(dispatchSpy).toHaveBeenCalledTimes(1);
        });

        it('should register to an event', () => {
            const registerForEventSpy = jest.spyOn(helperService, 'registerForEvent');
            component.onRegister(EVENT_EXAMPLE);
            expect(registerForEventSpy).toHaveBeenCalledWith(EVENT_EXAMPLE);
            expect(registerForEventSpy).toHaveBeenCalledTimes(1);
        });

        it('should unregister from an event', () => {
            const unregisterFromEventSpy = jest.spyOn(helperService, 'unregisterFromEvent');
            component.currentUser = { ...USER_EXAMPLE };
            component.onUnregister(EVENT_EXAMPLE);
            expect(unregisterFromEventSpy).toHaveBeenCalledWith(EVENT_EXAMPLE, USER_EXAMPLE.id);
            expect(unregisterFromEventSpy).toHaveBeenCalledTimes(1);
        });

        it('should load past events via venue selection', () => {
            const dispatchSpy = jest.spyOn(store, 'dispatch');
            jest.spyOn(authenticationService, 'isInRole').mockReturnValue(true);
            store.reset({
                feature: {
                    toggleSwitch: false,
                },
            });
            component.onVenueSelectionChange('Example');

            expect(dispatchSpy).toHaveBeenCalledWith(
                new ChangeFilteredState({
                    filterValue: 'Example',
                    isFiltered: true,
                }),
            );
        });

        it('should init filter value as undefined', fakeAsync(() => {
            expect(component.venueFilter).toBe(undefined);
            expect(store.selectSnapshot(EventsState.events)).toStrictEqual([]);
        }));

        it('should change filter value', fakeAsync(() => {
            store.reset({
                appEvents: {
                    filterValue: 'Example',
                },
            });
            component.filterValue$.subscribe((filter: string | undefined) => {
                expect(filter).toBeTruthy();
                expect(component.venueFilter).toBe('Example');
            });
        }));
    });
});
