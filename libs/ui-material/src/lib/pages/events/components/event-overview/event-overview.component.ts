import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Pipe, PipeTransform } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Store } from '@ngxs/store';
import { AppEvent, AppUser, AppVenue, Metadata, SetPageSize } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { FeatureState } from '@vrt/common';
import { ChangeFilteredState, EventsState, LoadAvailableEvents, LoadFutureEvents, LoadPastEvents, SetEvent } from '@vrt/event-api';
import { ApiCallParameters, DEFAULTPARAMETERS } from '@vrt/utils';
import { VenuesState } from '@vrt/venue-api';
import { Observable, combineLatest } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { VrtEventHelperServiceService } from '../../services/vrt-event-helper-service.service';

interface EventData {
    user: AppUser | null;
    pastEvents: AppEvent[];
    toggleSwitch: boolean;
    upcomingEvents: AppEvent[];
    venues: AppVenue[] | undefined | null;
    isFiltered: boolean;
    filterValue: string | undefined;
}

@Pipe({
    name: 'checkTotalEventCount',
    standalone: true,
})
export class CheckTotalEventCountPipe implements PipeTransform {
    transform(metadata: Metadata): boolean {
        if (metadata.totalCount) {
            return metadata.totalCount > 5;
        }
        return false;
    }
}

@Component({
    selector: 'vrt-event-overview',
    templateUrl: './event-overview.component.html',
    styleUrls: ['./event-overview.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventOverviewComponent implements AfterViewInit {
    date = new Date();

    metaData$: Observable<Metadata> = this.eventHelperServiceService.metaData$;

    currentUser: AppUser | null = null;

    venueFilter: string | undefined;

    upcomingEvents$ = this.store.select(EventsState.events);

    pastEvents$ = this.store.select(EventsState.inactiveEvents);

    venues$ = this.store.select(VenuesState.venues);

    isFiltered$ = this.store.select(EventsState.isFiltered);

    user$ = this.authenticationService.currentUser$.pipe(startWith(null)).pipe(map((user: AppUser | null) => (this.currentUser = user)));

    toggleSwitch$ = this.store.select(FeatureState.toggleSwitch);

    filterValue$ = this.store.select(EventsState.filterValue).pipe(map((filer: string | undefined) => (this.venueFilter = filer)));

    data$: Observable<EventData> = combineLatest([
        this.upcomingEvents$,
        this.pastEvents$,
        this.toggleSwitch$,
        this.venues$,
        this.user$,
        this.isFiltered$,
        this.filterValue$,
    ]).pipe(
        map(([upcomingEvents, pastEvents, toggleSwitch, venues, user, isFiltered, filterValue]) => {
            return {
                upcomingEvents,
                pastEvents,
                toggleSwitch,
                venues,
                user,
                isFiltered,
                filterValue,
            };
        }),
    );

    constructor(
        private matDialog: MatDialog,
        private authenticationService: AuthenticationService,
        private changeDetectorRef: ChangeDetectorRef,
        private eventHelperServiceService: VrtEventHelperServiceService,
        private store: Store,
    ) {}

    onVenueSelectionChange(name: string | undefined) {
        if (name !== '') {
            this.store.dispatch(new ChangeFilteredState({ isFiltered: true, filterValue: name }));
        } else {
            this.store.dispatch(new ChangeFilteredState({ isFiltered: false, filterValue: undefined }));
        }
        if (this.authenticationService.isInRole(['admin', 'manager'])) {
            if (this.store.selectSnapshot(FeatureState.toggleSwitch)) {
                this.store.dispatch(
                    new LoadFutureEvents({
                        apiCallParameters: { ...DEFAULTPARAMETERS, filter: name },
                    }),
                );
            } else {
                this.store.dispatch(
                    new LoadPastEvents({
                        filter: name,
                        sortBy: 'Date',
                        sortOrder: 'desc',
                    }),
                );
            }
        } else {
            this.store.dispatch(
                new LoadAvailableEvents({
                    apiCallParameters: { ...DEFAULTPARAMETERS, filter: name },
                }),
            );
        }

        this.venueFilter = name;
    }

    changeToggleState(toggleState: boolean): void {
        const parameters = {
            page: 1,
            pageSize: 10,
            filter: this.venueFilter,
        } as ApiCallParameters;
        this.eventHelperServiceService.setToggleSwitchState(toggleState);
        if (!toggleState) {
            this.eventHelperServiceService.getPastEvents(parameters);
        } else {
            this.eventHelperServiceService.getFutureEvents(parameters);
        }
    }

    pageOptionChanged($event: PageEvent, toggleState: boolean): void {
        this.store.dispatch(new SetPageSize($event.pageSize));

        if (this.authenticationService.isInRole(['user'])) {
            this.store.dispatch(
                new LoadAvailableEvents({
                    apiCallParameters: {
                        ...DEFAULTPARAMETERS,
                        filter: this.venueFilter,
                        page: $event.pageIndex,
                        pageSize: $event.pageSize,
                        sortOrder: 'asc',
                    },
                }),
            );
        } else {
            if (!toggleState) {
                const parameters = {
                    ...DEFAULTPARAMETERS,
                    page: $event.pageIndex,
                    pageSize: $event.pageSize,
                    filter: this.venueFilter,
                } as ApiCallParameters;
                this.eventHelperServiceService.getPastEvents(parameters);
            } else {
                this.eventHelperServiceService.getFutureEvents({
                    ...DEFAULTPARAMETERS,
                    page: $event.pageIndex,
                    pageSize: $event.pageSize,
                    filter: this.venueFilter,
                });
            }
        }
    }

    ngAfterViewInit(): void {
        if (!this.store.selectSnapshot(FeatureState.toggleSwitch) && this.authenticationService.isInRole(['manager', 'admin'])) {
            this.changeToggleState(false);
        }
    }

    onDeleteEvent($event: AppEvent) {
        this.eventHelperServiceService.deleteEvent($event);
    }

    onDownloadLog($event: AppEvent) {
        this.eventHelperServiceService.downloadEventLog($event);
    }

    resetEventStore() {
        this.store.dispatch(new SetEvent({ appEvent: undefined }));
    }

    onRegister($event: AppEvent) {
        this.eventHelperServiceService.registerForEvent($event);
    }

    onUnregister($event: AppEvent) {
        if (this.currentUser?.id) {
            this.eventHelperServiceService.unregisterFromEvent($event, this.currentUser.id);
        }
    }
}
