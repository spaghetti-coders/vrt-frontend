import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AppEvent, AppUser } from '@vrt/api';
import { SignInComponent } from '../sign-in/sign-in.component';

@Component({
    selector: 'vrt-registration-quick-menu',
    templateUrl: './registration-quick-menu.component.html',
    styleUrls: ['./registration-quick-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationQuickMenuComponent {
    @Input()
    vrtEvent!: AppEvent;

    @Input()
    user?: AppUser | undefined | null;

    @Output()
    downloadLog: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    @Output()
    registerToEvent: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    @Output()
    unregisterFromEvent: EventEmitter<AppEvent> = new EventEmitter<AppEvent>();

    constructor(private matDialog: MatDialog) {}

    onRegisterForEvent(vrtEvent: AppEvent): void {
        this.registerToEvent.emit(vrtEvent);
    }

    onUnregisterFromEvent(vrtEvent: AppEvent): void {
        const dialogRef = this.matDialog.open(SignInComponent, {
            data: { text: 'aldermen' },
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.unregisterFromEvent.emit(vrtEvent);
            }
        });
    }
}
