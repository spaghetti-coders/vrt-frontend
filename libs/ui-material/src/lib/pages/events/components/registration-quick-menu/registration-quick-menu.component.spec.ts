import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { HasPermissionModule } from '@vrt/shared';
import { MockModule } from 'ng-mocks';
import { of } from 'rxjs';
import { IsEventInPastPipe } from '../../pipes/is-event-passed.pipe';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { EVENT_EXAMPLE } from '@vrt/testing';
import { RegistrationQuickMenuComponent } from './registration-quick-menu.component';
import spyOn = jest.spyOn;

describe('RegistrationQuickMenuComponent', () => {
    let component: RegistrationQuickMenuComponent;
    let fixture: ComponentFixture<RegistrationQuickMenuComponent>;
    let dialog: MatDialog;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                getTranslocoModule(),
                HttpClientTestingModule,
                RouterModule.forRoot([]),
                FormsModule,
                ReactiveFormsModule,
                BrowserAnimationsModule,
                MockModule(MatIconModule),
                MockModule(MatCardModule),
                MockModule(HasPermissionModule),
                NgxsModule.forRoot(),
            ],
            declarations: [RegistrationQuickMenuComponent, IsEventInPastPipe],
            providers: [
                { provide: MatDialog, useValue: { open: jest.fn() } },
                {
                    provide: MatDialogRef,
                    useValue: {
                        close: jest.fn(),
                        afterClosed: jest.fn().mockReturnValue(of(true)),
                    },
                },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(RegistrationQuickMenuComponent);
        component = fixture.componentInstance;
        dialog = TestBed.inject(MatDialog);
        fixture.detectChanges();
    });

    it('should match snapshot', () => {
        expect(fixture).toMatchSnapshot();
    });

    it('should register to an event', () => {
        const spy = spyOn(component.registerToEvent, 'emit');
        component.onRegisterForEvent(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledWith(EVENT_EXAMPLE);
    });

    it('should unregister from an event', () => {
        const spy = spyOn(component.unregisterFromEvent, 'emit');
        dialog.open = jest.fn().mockReturnValue({ afterClosed: () => of(true) });
        component.onUnregisterFromEvent(EVENT_EXAMPLE);
        expect(spy).toHaveBeenCalledWith(EVENT_EXAMPLE);
    });
});
