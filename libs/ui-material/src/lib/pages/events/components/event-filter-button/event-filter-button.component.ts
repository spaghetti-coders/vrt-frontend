import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AppVenue } from '@vrt/api';

@Component({
    selector: 'vrt-event-filter-button',
    templateUrl: './event-filter-button.component.html',
    styleUrls: ['./event-filter-button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventFilterButtonComponent {
    @Input()
    venues: AppVenue[] | undefined | null = [];

    @Input()
    isFiltered = false;

    @Input()
    venueFilter: string | undefined = '';

    @Output()
    venueName: EventEmitter<string> = new EventEmitter<string>();

    onSelectionChange(name: string | undefined) {
        this.venueName.emit(name);
    }
}
