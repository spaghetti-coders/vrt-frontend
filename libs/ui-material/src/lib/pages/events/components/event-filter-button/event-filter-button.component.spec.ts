import { ComponentFixture, TestBed } from '@angular/core/testing';
import { getTranslocoModule } from '@vrt/common';

import { VENUE_EXAMPLE } from '@vrt/testing';
import { EventFilterButtonComponent } from './event-filter-button.component';

describe('EventFilterButtonComponent', () => {
    let component: EventFilterButtonComponent;
    let fixture: ComponentFixture<EventFilterButtonComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [getTranslocoModule()],
            declarations: [EventFilterButtonComponent],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(EventFilterButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should match snapshot', () => {
        expect(fixture).toMatchSnapshot();
    });

    it('should have venues', () => {
        component.venues = [VENUE_EXAMPLE];
        expect(component.venues).toEqual([VENUE_EXAMPLE]);
    });

    it('should emit venue name', () => {
        const spy = jest.spyOn(component.venueName, 'emit');

        component.onSelectionChange(VENUE_EXAMPLE.name);

        expect(spy).toHaveBeenCalledWith(VENUE_EXAMPLE.name);
        expect(spy).toHaveBeenCalledTimes(1);
    });
});
