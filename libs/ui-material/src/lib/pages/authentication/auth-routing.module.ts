import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthPageComponent } from './auth-page.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { RegistrationConfirmViewComponent } from './components/registration-confirm-view/registration-confirm-view.component';
import { RequestTokenViewComponent } from './components/request-token-view/request-token-view.component';

const routes: Routes = [
    {
        path: '',
        component: AuthPageComponent,
        children: [
            {
                path: 'confirm',
                component: RegistrationConfirmViewComponent,
            },
            {
                path: 'login',
                component: LoginComponent,
            },
            {
                path: 'register',
                component: RegisterComponent,
            },
            {
                path: 'reset',
                component: RequestTokenViewComponent,
            },
            {
                path: 'reset/:token',
                component: RequestTokenViewComponent,
            },
        ],
    },
    { path: '**', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AuthRoutingModule {}
