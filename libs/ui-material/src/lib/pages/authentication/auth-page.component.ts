import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { AppConfigState, SwitchLang } from '@vrt/common';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'vrt-auth-page',
    templateUrl: 'auth-page.component.html',
    styleUrls: ['auth-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthPageComponent {
    isDarkThemeSelected = false;

    isDarkThemeSelected$ = this.store
        .select(AppConfigState.isDarkThemeSelected)
        .pipe(tap((isDarkThemeSelected) => (this.isDarkThemeSelected = isDarkThemeSelected)));

    availableLanguages = ['de', 'en'];

    constructor(private store: Store) {}

    switchLanguage(language: string) {
        this.store.dispatch(new SwitchLang({ language }));
    }
}
