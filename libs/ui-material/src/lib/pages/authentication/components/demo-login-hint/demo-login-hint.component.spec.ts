import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { getTranslocoModule } from '@vrt/common';
import { MockModule } from 'ng-mocks';

import { DemoLoginHintComponent } from './demo-login-hint.component';

describe('DemoLoginHintComponent', () => {
    let component: DemoLoginHintComponent;
    let fixture: ComponentFixture<DemoLoginHintComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [MockModule(MatDialogModule), getTranslocoModule()],
            declarations: [DemoLoginHintComponent],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(DemoLoginHintComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
