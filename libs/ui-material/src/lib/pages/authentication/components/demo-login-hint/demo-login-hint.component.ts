import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'vrt-demo-login-hint',
    templateUrl: './demo-login-hint.component.html',
    styleUrls: ['./demo-login-hint.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DemoLoginHintComponent {}
