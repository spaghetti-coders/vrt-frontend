import { ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { LoginModel } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { AppConfigState } from '@vrt/common';
import { IS_DEMO_BUILD } from '@vrt/utils';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, takeUntil, tap } from 'rxjs/operators';
import { DemoLoginHintComponent } from '../demo-login-hint/demo-login-hint.component';

@Component({
    selector: 'vrt-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit, OnDestroy {
    destroy$: Subject<boolean> = new Subject<boolean>();

    hide = true;

    isDarkThemeSelected = false;

    isDarkThemeSelected$: Observable<boolean> = this.store
        .select(AppConfigState.isDarkThemeSelected)
        .pipe(tap((value) => (this.isDarkThemeSelected = value)));

    loginForm?: FormGroup<{
        username: FormControl<string>;
        password: FormControl<string>;
    }>;

    returnUrl = '';

    showHint = false;

    constructor(
        @Inject(IS_DEMO_BUILD) private isDemoBuild: boolean,
        private formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        private route: ActivatedRoute,
        private store: Store,
        private dialog: MatDialog,
    ) {}

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    ngOnInit(): void {
        this.loginForm = this.formBuilder.nonNullable.group({
            username: [this.isDemoBuild ? 'User' : '', Validators.required],
            password: [this.isDemoBuild ? 'MyTopSecurePassword' : '', Validators.required],
        });

        this.showHint = this.isDemoBuild;

        this.route.queryParams
            .pipe(
                catchError((err) => {
                    return throwError(err);
                }),
                tap((item) => {
                    this.returnUrl = item['returnUrl'];
                }),
                takeUntil(this.destroy$),
            )
            .subscribe();
    }

    login(): void {
        if (this.loginForm?.valid) {
            this.authenticationService.login(this.loginForm.value as LoginModel, this.returnUrl);
        }
    }

    showLoginMsg() {
        this.dialog.open(DemoLoginHintComponent);
    }
}
