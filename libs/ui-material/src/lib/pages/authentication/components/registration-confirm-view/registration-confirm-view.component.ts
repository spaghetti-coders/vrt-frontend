import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'vrt-registration-confirm-view',
    templateUrl: './registration-confirm-view.component.html',
    styleUrls: ['./registration-confirm-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationConfirmViewComponent implements OnInit {
    isDarkThemeSelected = false;

    ngOnInit(): void {
        const theme = localStorage.getItem('theme');
        this.isDarkThemeSelected = theme === 'Dark';
    }
}
