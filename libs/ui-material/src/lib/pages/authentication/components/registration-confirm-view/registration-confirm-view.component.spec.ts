import { ComponentFixture, TestBed } from '@angular/core/testing';
import { getTranslocoModule } from '@vrt/common';

import { RegistrationConfirmViewComponent } from './registration-confirm-view.component';

describe('RegistrationConfirmViewComponent', () => {
    let component: RegistrationConfirmViewComponent;
    let fixture: ComponentFixture<RegistrationConfirmViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [RegistrationConfirmViewComponent],
            imports: [getTranslocoModule()],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(RegistrationConfirmViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
