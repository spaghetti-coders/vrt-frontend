import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '@vrt/auth';
import { confirmedValidator } from '../../services/confirmedValidator';

@Component({
    selector: 'vrt-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterComponent implements OnInit {
    hide = true;

    registerForm?: UntypedFormGroup;

    isDarkThemeSelected = false;

    constructor(
        private formBuilder: UntypedFormBuilder,
        private authenticationService: AuthenticationService,
    ) {}

    ngOnInit(): void {
        const theme = localStorage.getItem('theme');

        this.isDarkThemeSelected = theme === 'Dark';

        this.registerForm = this.formBuilder.group(
            {
                username: ['', Validators.required],
                displayName: ['', Validators.required],
                password: ['', [Validators.required, Validators.minLength(8)]],
                confirmPassword: ['', [Validators.required, Validators.minLength(8)]],
                receiveNotifications: [false],
                email: [null, Validators.email],
            },
            {
                validator: confirmedValidator('password', 'confirmPassword'),
            },
        );
    }

    register(): void {
        if (this.registerForm?.value.email === '') {
            this.registerForm.value.email = null;
        }
        this.authenticationService.register(this.registerForm?.value);
    }

    checkNotificationCheckbox(): void {
        const state = this.registerForm?.get('receiveNotifications')?.value;
        if (state === true) {
            this.registerForm?.get('email')?.setValidators([Validators.required, Validators.email]);
        }
        if (state === false) {
            this.registerForm?.get('email')?.setValidators(Validators.email);
        }
        this.registerForm?.get('email')?.updateValueAndValidity();
    }
}
