import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';
import { AuthenticationService } from '@vrt/auth';
import { getTranslocoModule } from '@vrt/common';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { provideAutoSpy } from 'jest-auto-spies';
import { MockModule } from 'ng-mocks';
import { PasswordFormModule } from '../../../../atomic/index';

import { RequestTokenViewComponent } from './request-token-view.component';

describe('RequestTokenViewComponent', () => {
    let component: RequestTokenViewComponent;
    let fixture: ComponentFixture<RequestTokenViewComponent>;
    let authenticationService: AuthenticationService;

    const paramMap = new Map([['token', 'EXAMPLE']]);

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                FormsModule,
                getTranslocoModule(),
                ReactiveFormsModule,
                PasswordFormModule,
                HttpClientTestingModule,
                RouterTestingModule,
                BrowserAnimationsModule,
                NgxsModule.forRoot(),
                MockModule(MatCardModule),
                MockModule(MatFormFieldModule),
            ],
            declarations: [RequestTokenViewComponent],
            providers: [
                provideAutoSpy(NotificationSnackBarService),
                { provide: IS_DEMO_BUILD, useValue: false },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            paramMap,
                        },
                    },
                },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(RequestTokenViewComponent);
        authenticationService = TestBed.inject(AuthenticationService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should should set token value from url in form', () => {
        expect(component.resetForm?.get('recoveryToken')?.value).toEqual('EXAMPLE');
    });

    it('should request token from api', () => {
        authenticationService.requestPasswordToken = jest.fn();

        component.requestForm?.setValue({ email: 'example@gmail.com' });
        component.requestTokenFromApi();

        expect(component.requestForm?.valid).toBeTruthy();
        expect(authenticationService.requestPasswordToken).toHaveBeenCalled();
    });

    it('should request reset password', () => {
        authenticationService.resetPassword = jest.fn();
        component.resetForm?.setValue({
            recoveryToken: 'TOKEN',
            password: 'PASSWORD',
            confirmPassword: 'PASSWORD',
        });

        component.resetPassword();
        expect(component.resetForm?.valid).toBeTruthy();
        expect(authenticationService.resetPassword).toHaveBeenCalled();
    });
});
