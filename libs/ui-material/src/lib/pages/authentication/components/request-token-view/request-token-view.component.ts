import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '@vrt/auth';
import { confirmedValidator } from '../../services/confirmedValidator';

@Component({
    selector: 'vrt-request-token-view',
    templateUrl: './request-token-view.component.html',
    styleUrls: ['./request-token-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequestTokenViewComponent implements OnInit {
    hide = true;

    isDarkThemeSelected = false;

    requestForm?: UntypedFormGroup;

    resetForm?: UntypedFormGroup;

    tokenExists = false;

    tokenViaUrl = false;

    constructor(
        private formBuilder: UntypedFormBuilder,
        private authenticationService: AuthenticationService,
        private activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        const theme = localStorage.getItem('theme');

        this.isDarkThemeSelected = theme === 'Dark';

        this.requestForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
        });

        this.resetForm = this.formBuilder.group(
            {
                recoveryToken: ['', Validators.required],
                password: ['', [Validators.required, Validators.minLength(8)]],
                confirmPassword: ['', [Validators.required, Validators.minLength(8)]],
            },
            {
                validator: confirmedValidator('password', 'confirmPassword'),
            },
        );

        const params = this.activatedRoute.snapshot.paramMap;
        const token = params.get('token');
        if (token) {
            this.tokenViaUrl = true;
            this.resetForm.get('recoveryToken')?.setValue(token);
            this.resetForm.get('recoveryToken')?.updateValueAndValidity();
        }
    }

    requestTokenFromApi(): void {
        if (this.requestForm?.valid) {
            this.authenticationService.requestPasswordToken(this.requestForm.value);
        }
    }

    resetPassword(): void {
        this.authenticationService.resetPassword(this.resetForm?.value);
    }
}
