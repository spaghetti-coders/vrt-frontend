import { UntypedFormBuilder, Validators } from '@angular/forms';
import { confirmedValidator } from './confirmedValidator';

describe('Confirmed Validator', () => {
    it('should return confirmedValidator true', () => {
        const form = new UntypedFormBuilder().group(
            {
                password: ['12345678', [Validators.required, Validators.minLength(8)]],
                confirmPassword: ['12345678', [Validators.required, Validators.minLength(8)]],
            },
            {
                validator: confirmedValidator('password', 'confirmPassword'),
            },
        );
        expect(form.status).toBe('VALID');
    });

    it('should return confirmedValidator false', () => {
        const form = new UntypedFormBuilder().group(
            {
                password: ['12345678', [Validators.required, Validators.minLength(8)]],
                confirmPassword: ['123456789', [Validators.required, Validators.minLength(8)]],
            },
            {
                validator: confirmedValidator('password', 'confirmPassword'),
            },
        );
        expect(form.status).toBe('INVALID');
    });
});
