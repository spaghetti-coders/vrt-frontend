import { UntypedFormGroup } from '@angular/forms';

export function confirmedValidator(controlName: string, matchingControlName: string): (formGroup: UntypedFormGroup) => void {
    return (formGroup: UntypedFormGroup) => {
        const control = formGroup.controls[controlName];

        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl && matchingControl.errors && !matchingControl.errors['confirmedValidator']) {
            return;
        }

        if (matchingControl && control?.value !== matchingControl?.value) {
            matchingControl?.setErrors({ confirmedValidator: true });
        } else {
            matchingControl?.setErrors(null);
        }
    };
}
