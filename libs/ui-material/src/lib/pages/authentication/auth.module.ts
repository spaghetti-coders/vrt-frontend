import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { TranslocoModule } from '@ngneat/transloco';
import { FooterModule, NavigationModule, PasswordFormModule } from '../../atomic/index';
import { AuthPageComponent } from './auth-page.component';
import { AuthRoutingModule } from './auth-routing.module';
import { DemoLoginHintComponent } from './components/demo-login-hint/demo-login-hint.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { RegistrationConfirmViewComponent } from './components/registration-confirm-view/registration-confirm-view.component';
import { RequestTokenViewComponent } from './components/request-token-view/request-token-view.component';

@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
        RequestTokenViewComponent,
        RegistrationConfirmViewComponent,
        AuthPageComponent,
        DemoLoginHintComponent,
    ],
    imports: [
        CommonModule,
        FooterModule,
        FormsModule,
        ReactiveFormsModule,
        AuthRoutingModule,
        NavigationModule,
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatButtonModule,
        MatCheckboxModule,
        PasswordFormModule,
        TranslocoModule,
        MatMenuModule,
        MatDialogModule,
    ],
    exports: [],
})
export class AuthModule {}
