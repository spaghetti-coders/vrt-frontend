import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AppUser } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { WebNotificationService } from '@vrt/shared';
import { Observable } from 'rxjs';
import { ConfirmDialogComponent } from '../../../../atomic/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'vrt-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserProfileComponent implements OnInit {
    permission: NotificationPermission | null = null;

    user$: Observable<AppUser | null> = this.authenticationService.currentUser$;

    constructor(
        private authenticationService: AuthenticationService,
        private webNotificationService: WebNotificationService,
        private matDialog: MatDialog,
    ) {}

    ngOnInit(): void {
        this.permission = this.webNotificationService.isEnabled ? Notification.permission : null;
        if (this.permission === 'default') {
            this.unSubscribeToNotifications();
        }
    }

    deleteAccount(): void {
        const dialogRef = this.matDialog.open(ConfirmDialogComponent, {});
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.authenticationService.deleteAccount();
            }
        });
    }

    onSubscribeToNotifications(): void {
        this.webNotificationService.subscribeToNotifications().then(() => {
            this.permission = Notification.permission;
            window.location.reload();
        });
    }

    unSubscribeToNotifications(): void {
        this.webNotificationService.unSubscribeToNotifications();
    }
}
