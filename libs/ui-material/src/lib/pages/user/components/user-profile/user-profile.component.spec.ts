import { fakeAsync, tick } from '@angular/core/testing';
import { AuthenticationService } from '@vrt/auth';
import { WebNotificationService } from '@vrt/shared';
import { Spy, createSpyFromClass } from 'jest-auto-spies';
import { of } from 'rxjs';

import { MatDialog } from '@angular/material/dialog';
import { UserProfileComponent } from './user-profile.component';

describe('UserProfileComponent', () => {
    globalThis.Notification = {
        requestPermission: jest.fn(),
        permission: 'default',
    } as unknown as jest.Mocked<typeof Notification>;

    let component: UserProfileComponent;
    let authService: Spy<AuthenticationService>;
    let webNotificationService: Spy<WebNotificationService>;
    let matDialog: Spy<MatDialog>;

    beforeEach(async () => {
        authService = createSpyFromClass(AuthenticationService);
        webNotificationService = createSpyFromClass(WebNotificationService, {
            gettersToSpyOn: ['isEnabled'],
        });
        matDialog = createSpyFromClass(MatDialog);
        component = new UserProfileComponent(authService, webNotificationService, matDialog);
    });

    it('should create', () => {
        webNotificationService.accessorSpies.getters.isEnabled.mockReturnValue(true);
        component.ngOnInit();
        expect(component).toBeTruthy();
    });

    it('should delete user account', fakeAsync(() => {
        matDialog.open.mockReturnValue({
            afterClosed: jest.fn().mockReturnValue(of(true)),
        } as any);
        component.deleteAccount();
        tick();
        expect(authService.deleteAccount).toHaveBeenCalled();
    }));

    it('should subscribe to notifications', fakeAsync(() => {
        Object.defineProperty(window, 'location', {
            writable: true,
            value: { reload: jest.fn() },
        });

        webNotificationService.subscribeToNotifications.resolveWith();

        component.onSubscribeToNotifications();
        tick(50);
        expect(window.location.reload).toHaveBeenCalled();
    }));
});
