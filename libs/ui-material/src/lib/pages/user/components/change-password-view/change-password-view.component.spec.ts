import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChangeDetectionStrategy } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, UntypedFormBuilder, Validators } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';
import { AuthenticationService } from '@vrt/auth';
import { getTranslocoModule } from '@vrt/common';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { provideAutoSpy } from 'jest-auto-spies';
import { MockModule } from 'ng-mocks';
import { PasswordFormModule } from '../../../../atomic/index';
import { confirmedValidator } from '../../../authentication/index';

import { ChangePasswordViewComponent } from './change-password-view.component';

describe('ChangePasswordViewComponent', () => {
    let component: ChangePasswordViewComponent;
    let fixture: ComponentFixture<ChangePasswordViewComponent>;
    let authenticationService: AuthenticationService;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                getTranslocoModule(),
                PasswordFormModule,
                HttpClientTestingModule,
                BrowserAnimationsModule,
                RouterTestingModule,
                MockModule(MatCardModule),
                NgxsModule.forRoot(),
            ],
            declarations: [ChangePasswordViewComponent],
            providers: [provideAutoSpy(NotificationSnackBarService), { provide: IS_DEMO_BUILD, useValue: false }],
            teardown: { destroyAfterEach: false },
        }).overrideComponent(ChangePasswordViewComponent, {
            set: { changeDetection: ChangeDetectionStrategy.Default },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ChangePasswordViewComponent);
        authenticationService = TestBed.inject(AuthenticationService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should match snapshot', () => {
        expect(fixture).toMatchSnapshot();
    });

    it('should change password', () => {
        const spy = jest.spyOn(authenticationService, 'changePassword');
        component.resetForm = new UntypedFormBuilder().group(
            {
                password: ['12345678', [Validators.required, Validators.minLength(8)]],
                confirmPassword: ['12345678', [Validators.required, Validators.minLength(8)]],
            },
            {
                validator: confirmedValidator('password', 'confirmPassword'),
            },
        );
        component.changePassword();

        expect(component.resetForm.status).toBe('VALID');
        expect(spy).toHaveBeenCalledWith({ password: '12345678' });
    });
});
