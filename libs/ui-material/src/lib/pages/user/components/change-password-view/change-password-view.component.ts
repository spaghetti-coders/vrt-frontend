import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { UpdatePasswordModel } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { confirmedValidator } from '../../../authentication';

@Component({
    selector: 'vrt-change-password-view',
    templateUrl: './change-password-view.component.html',
    styleUrls: ['./change-password-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChangePasswordViewComponent implements OnInit {
    resetForm?: UntypedFormGroup;

    hide = true;

    constructor(
        private authenticationService: AuthenticationService,
        private formBuilder: UntypedFormBuilder,
    ) {}

    ngOnInit(): void {
        this.resetForm = this.formBuilder.group(
            {
                password: ['', [Validators.required, Validators.minLength(8)]],
                confirmPassword: ['', [Validators.required, Validators.minLength(8)]],
            },
            {
                validator: confirmedValidator('password', 'confirmPassword'),
            },
        );
    }

    changePassword(): void {
        this.authenticationService.changePassword({
            password: this.resetForm?.get('password')?.value,
        } as UpdatePasswordModel);
    }
}
