import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AppUser } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { Observable } from 'rxjs';
import { filter, tap } from 'rxjs/operators';

@Component({
    selector: 'vrt-user-edit-profile',
    templateUrl: './user-edit-profile.component.html',
    styleUrls: ['./user-edit-profile.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserEditProfileComponent {
    userForm?: UntypedFormGroup;

    currentUser$: Observable<AppUser> = this.authenticationService.currentUser$.pipe(
        filter((user): user is AppUser => !!user),
        tap(({ displayName, email, receiveNotifications }: AppUser) => {
            this.prepareForm(displayName, email as string, receiveNotifications as boolean);
        }),
    );

    constructor(
        private authenticationService: AuthenticationService,
        private formBuilder: UntypedFormBuilder,
    ) {}

    prepareForm(displayName: string, email: string, receiveNotifications: boolean) {
        this.userForm = this.formBuilder.group({
            displayName: [displayName, Validators.required],
            email: [email, Validators.email],
            receiveNotifications: [receiveNotifications],
        });
    }

    submit(): void {
        if (this.userForm?.get('email')?.value?.length === 0) {
            this.userForm?.removeControl('email');
        }
        this.authenticationService.updateUserProfile(this.userForm?.value);
    }
}
