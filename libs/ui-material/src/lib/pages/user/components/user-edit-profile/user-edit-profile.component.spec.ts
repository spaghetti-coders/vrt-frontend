import { waitForAsync } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { AuthenticationService } from '@vrt/auth';
import { USER_EXAMPLE } from '@vrt/testing';
import { createSpyFromClass, Spy } from 'jest-auto-spies';
import { UserEditProfileComponent } from './user-edit-profile.component';

describe('UserEditProfileComponent', () => {
    let component: UserEditProfileComponent;
    let authService: Spy<AuthenticationService>;
    const formBuilder = new UntypedFormBuilder();

    beforeEach(() => {
        authService = createSpyFromClass(AuthenticationService, {
            observablePropsToSpyOn: ['currentUser$'],
        });
        component = new UserEditProfileComponent(authService, formBuilder);
    });

    it('should create', () => {
        authService.currentUser$.nextWith(USER_EXAMPLE);
        expect(component).toBeTruthy();
    });

    it('should call prepare formgroup function', waitForAsync(() => {
        const { displayName, email, receiveNotifications } = USER_EXAMPLE;
        const prepareForm = component.prepareForm;
        component.prepareForm = jest.fn();
        authService.currentUser$.nextWith(USER_EXAMPLE);
        component.currentUser$.subscribe((user) => {
            expect(user).toBe(USER_EXAMPLE);
        });
        expect(component.prepareForm).toHaveBeenCalledWith(displayName, email, receiveNotifications);
        component.prepareForm = prepareForm;
    }));

    it('should prepare form', () => {
        const { displayName, email, receiveNotifications } = USER_EXAMPLE;
        component.prepareForm(displayName, email as string, !!receiveNotifications);
        expect(component.userForm).toBeTruthy();
        expect(component.userForm?.get('displayName')?.value).toEqual(displayName);
    });

    it('should submit userForm', waitForAsync(() => {
        authService.currentUser$.nextWith(USER_EXAMPLE);
        component.currentUser$.subscribe();
        component.submit();
        expect(authService.updateUserProfile).toHaveBeenCalledWith({
            displayName: 'Max Muster the 1',
            receiveNotifications: false,
        });
    }));
});
