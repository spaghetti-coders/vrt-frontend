import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavigationComponent } from '../../atomic/navigation/navigation.component';
import { ChangePasswordViewComponent } from './components/change-password-view/change-password-view.component';
import { UserEditProfileComponent } from './components/user-edit-profile/user-edit-profile.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';

const routes: Routes = [
    {
        path: '',
        component: NavigationComponent,
        children: [
            {
                path: '',
                component: UserProfileComponent,
            },
            {
                path: 'edit',
                component: UserEditProfileComponent,
            },
            {
                path: 'password',
                component: ChangePasswordViewComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserAreaRoutingModule {}
