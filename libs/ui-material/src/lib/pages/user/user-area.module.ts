import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { TranslocoModule } from '@ngneat/transloco';
import { ConfirmDialogModule, NavigationModule, PasswordFormModule, ScrollTopButtonModule } from '../../atomic';
import { ChangePasswordViewComponent } from './components/change-password-view/change-password-view.component';
import { UserEditProfileComponent } from './components/user-edit-profile/user-edit-profile.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';

import { UserAreaRoutingModule } from './user-area-routing.module';

@NgModule({
    declarations: [UserProfileComponent, UserEditProfileComponent, ChangePasswordViewComponent],
    imports: [
        CommonModule,
        ConfirmDialogModule,
        MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatTabsModule,
        NavigationModule,
        PasswordFormModule,
        ReactiveFormsModule,
        ScrollTopButtonModule,
        TranslocoModule,
        UserAreaRoutingModule,
    ],
})
export class UserAreaModule {}
