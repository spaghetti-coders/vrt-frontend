import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MockModule } from 'ng-mocks';
import { ScrollTopButtonComponent } from './scroll-top-button.component';

describe('ScrollTopButtonComponent', () => {
    let component: ScrollTopButtonComponent;
    let fixture: ComponentFixture<ScrollTopButtonComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ScrollTopButtonComponent],
            imports: [MockModule(MatIconModule)],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ScrollTopButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should scroll back to top', () => {
        window.scroll = jest.fn();
        component.onScrollTop();
        expect(window.scroll).toHaveBeenCalledWith(0, 0);
    });
});
