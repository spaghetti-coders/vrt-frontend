import { Component } from '@angular/core';

@Component({
    selector: 'vrt-scroll-top-button',
    templateUrl: './scroll-top-button.component.html',
    styleUrls: ['./scroll-top-button.component.scss'],
})
export class ScrollTopButtonComponent {
    onScrollTop() {
        window.scroll(0, 0);
    }
}
