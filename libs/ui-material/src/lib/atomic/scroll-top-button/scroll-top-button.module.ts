import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ScrollTopButtonComponent } from './scroll-top-button/scroll-top-button.component';

@NgModule({
    declarations: [ScrollTopButtonComponent],
    imports: [CommonModule, MatIconModule, MatButtonModule],
    exports: [ScrollTopButtonComponent],
})
export class ScrollTopButtonModule {}
