import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { AppUser } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { AppConfigState } from '@vrt/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'vrt-privacy',
    templateUrl: './privacy.component.html',
    styleUrls: ['./privacy.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrivacyComponent {
    currentUser$: Observable<AppUser | null> = this.authenticationService.currentUser$;

    isDarkThemeSelected = false;

    isDarkThemeSelected$ = this.store.select(AppConfigState.isDarkThemeSelected).pipe(tap((value) => (this.isDarkThemeSelected = value)));

    constructor(
        private authenticationService: AuthenticationService,
        private location: Location,
        private store: Store,
    ) {}

    navigateBack(): void {
        this.location.back();
    }
}
