import { Location } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { Spy, createSpyFromClass, provideAutoSpy } from 'jest-auto-spies';
import { MockModule } from 'ng-mocks';
import { ScrollTopButtonComponent } from '../scroll-top-button/scroll-top-button/scroll-top-button.component';

import { PrivacyComponent } from './privacy.component';

describe('PrivacyComponent', () => {
    let component: PrivacyComponent;
    let fixture: ComponentFixture<PrivacyComponent>;
    let location: Spy<Location> = createSpyFromClass(Location);

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [PrivacyComponent, ScrollTopButtonComponent],
            imports: [
                MockModule(MatSnackBarModule),
                MockModule(MatIconModule),
                RouterTestingModule,
                NgxsModule.forRoot(),
                getTranslocoModule(),
                HttpClientTestingModule,
            ],
            providers: [
                provideAutoSpy(NotificationSnackBarService),
                { provide: IS_DEMO_BUILD, useValue: false },
                { provide: Location, useValue: location },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PrivacyComponent);
        component = fixture.componentInstance;
        location = TestBed.inject<any>(Location);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call back function on button click', () => {
        component.navigateBack();
        expect(location.back).toHaveBeenCalled();
    });
});
