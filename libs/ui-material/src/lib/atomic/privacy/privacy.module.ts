import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { TranslocoModule } from '@ngneat/transloco';
import { ScrollTopButtonModule } from '../scroll-top-button/scroll-top-button.module';

import { PrivacyComponent } from './privacy.component';

@NgModule({
    declarations: [PrivacyComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: PrivacyComponent,
            },
        ]),
        TranslocoModule,
        ScrollTopButtonModule,
        MatButtonModule,
    ],
})
export class PrivacyModule {}
