import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { getTranslocoModule } from '@vrt/common';
import { WhiteSpaceComponent } from '../../white-space/white-space.component';
import { PasswordFormComponent } from './password-form.component';

describe('PasswordFormComponent', () => {
    let component: PasswordFormComponent;
    let fixture: ComponentFixture<PasswordFormComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [PasswordFormComponent, WhiteSpaceComponent],
            imports: [FormsModule, ReactiveFormsModule, MatInputModule, MatIconModule, getTranslocoModule()],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PasswordFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
