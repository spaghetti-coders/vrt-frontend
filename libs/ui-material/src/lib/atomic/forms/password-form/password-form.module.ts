import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { TranslocoModule } from '@ngneat/transloco';
import { WhiteSpaceModule } from '../../white-space/white-space.module';
import { PasswordFormComponent } from './password-form.component';

@NgModule({
    declarations: [PasswordFormComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatIconModule,
        WhiteSpaceModule,
        MatInputModule,
        MatButtonModule,
        TranslocoModule,
    ],
    exports: [PasswordFormComponent],
})
export class PasswordFormModule {}
