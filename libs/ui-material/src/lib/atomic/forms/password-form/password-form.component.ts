import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'vrt-password-form',
    templateUrl: './password-form.component.html',
    styleUrls: ['./password-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PasswordFormComponent implements OnInit {
    @Input() passwordGroup?: UntypedFormGroup;

    confirm$?: Observable<boolean>;

    hide = true;

    password$?: Observable<boolean>;

    private static _isWhiteSpaceInPassword(input: string): boolean {
        const result = input?.match(/\s/g);
        return !!result;
    }

    ngOnInit(): void {
        this.password$ = this.passwordGroup?.get('password')?.valueChanges.pipe(
            map((input) => {
                return PasswordFormComponent._isWhiteSpaceInPassword(input);
            }),
        );

        this.confirm$ = this.passwordGroup?.get('confirm_password')?.valueChanges.pipe(
            map((input) => {
                return PasswordFormComponent._isWhiteSpaceInPassword(input);
            }),
        );
    }
}
