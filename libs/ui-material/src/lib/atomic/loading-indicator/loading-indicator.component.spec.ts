import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';
import { FeatureState } from '@vrt/common';
import { LoadingIndicatorComponent } from './loading-indicator.component';

describe('LoadingIndicatorComponent', () => {
    let fixture: ComponentFixture<LoadingIndicatorComponent>;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [LoadingIndicatorComponent],
            imports: [NgxsModule.forRoot([FeatureState])],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LoadingIndicatorComponent);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(fixture).toMatchSnapshot();
    });
});
