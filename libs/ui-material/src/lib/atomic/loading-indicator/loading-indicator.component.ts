import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Select } from '@ngxs/store';
import { FeatureState } from '@vrt/common';
import { Observable } from 'rxjs';

@Component({
    selector: 'vrt-loading-indicator',
    templateUrl: './loading-indicator.component.html',
    styleUrls: ['./loading-indicator.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingIndicatorComponent {
    @Input()
    isLoading?: boolean;

    @Select(FeatureState.isLoading)
    loading$?: Observable<boolean>;
}
