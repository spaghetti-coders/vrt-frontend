import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LoadingIndicatorComponent } from './loading-indicator.component';

@NgModule({
    declarations: [LoadingIndicatorComponent],
    imports: [CommonModule, MatSnackBarModule],
    exports: [LoadingIndicatorComponent],
})
export class LoadingIndicatorModule {}
