import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { AppUser } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { AppConfigState, SwitchLang, SwitchTheme } from '@vrt/common';
import { NotificationSnackBarService } from '@vrt/utils';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
    selector: 'vrt-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationComponent implements OnInit {
    language$: Observable<string> = this.store.select(AppConfigState.language);

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map((result) => result.matches),
        shareReplay(),
    );

    isLoggedIn$ = this.authenticationService.isLoggedIn$;

    isDarkThemeSelected = false;

    user$: Observable<AppUser | null> = this.authenticationService.currentUser$;

    constructor(
        private breakpointObserver: BreakpointObserver,
        private authenticationService: AuthenticationService,
        private notificationService: NotificationSnackBarService,
        private overlayContainer: OverlayContainer,
        private store: Store,
    ) {}

    ngOnInit(): void {
        this.isDarkThemeSelected = localStorage.getItem('theme') === 'Dark';
        this.authenticationService.setLoggedInState(true);
        this.store.dispatch(new SwitchTheme({ isDarkThemeSelected: this.isDarkThemeSelected }));
    }

    switchTheme(): void {
        const darkThemeMode = 'dark-theme-mode';
        this.isDarkThemeSelected = !this.isDarkThemeSelected;
        this.store.dispatch(new SwitchTheme({ isDarkThemeSelected: this.isDarkThemeSelected }));
        localStorage.setItem('theme', this.isDarkThemeSelected ? 'Dark' : 'Light');

        if (this.isDarkThemeSelected) {
            this.overlayContainer.getContainerElement().classList.add(darkThemeMode);
        } else {
            this.overlayContainer.getContainerElement().classList.remove(darkThemeMode);
        }
    }

    logout(): void {
        this.authenticationService.setLoggedInState(false);
        this.authenticationService.logout();
        this.notificationService.showSuccess('Du hast dich abgemeldet.');
    }

    refresh(): void {
        location.reload();
    }

    switchLang(language: string) {
        this.store.dispatch(new SwitchLang({ language }));
    }
}
