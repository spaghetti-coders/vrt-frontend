import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { TranslocoModule } from '@ngneat/transloco';
import { HasPermissionModule, SharedModule } from '@vrt/shared';
import { FooterModule } from '../footer/footer.module';
import { LoadingIndicatorModule } from '../loading-indicator/loading-indicator.module';
import { NavigationComponent } from './navigation.component';

@NgModule({
    declarations: [NavigationComponent],
    imports: [
        CommonModule,
        RouterModule,
        FooterModule,
        MatSidenavModule,
        MatListModule,
        MatIconModule,
        MatToolbarModule,
        MatMenuModule,
        MatButtonModule,
        SharedModule,
        TranslocoModule,
        LoadingIndicatorModule,
        HasPermissionModule,
    ],
})
export class NavigationModule {}
