import { BreakpointObserver, BreakpointState, LayoutModule } from '@angular/cdk/layout';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChangeDetectionStrategy } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { AuthenticationService } from '@vrt/auth';
import { FeatureState, SwitchLang, SwitchTheme, getTranslocoModule } from '@vrt/common';
import { HasPermissionModule } from '@vrt/shared';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { provideAutoSpy } from 'jest-auto-spies';
import { MockModule } from 'ng-mocks';
import { Observable, of } from 'rxjs';
import { FooterModule } from '../footer/footer.module';
import { NavigationComponent } from './navigation.component';

describe('NavigationComponent', () => {
    const consoleErr = console.error;
    console.error = jest.fn();
    console.warn = jest.fn();

    let component: NavigationComponent;
    let fixture: ComponentFixture<NavigationComponent>;
    let store: Store;
    let authService: AuthenticationService;
    let notificationService: NotificationSnackBarService;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [NavigationComponent],
            imports: [
                MockModule(HasPermissionModule),
                NoopAnimationsModule,
                LayoutModule,
                getTranslocoModule(),
                RouterTestingModule,
                HttpClientTestingModule,
                FooterModule,
                NgxsModule.forRoot([FeatureState]),
            ],
            providers: [
                provideAutoSpy(NotificationSnackBarService),
                { provide: IS_DEMO_BUILD, useValue: false },
                {
                    provide: BreakpointObserver,
                    useValue: {
                        observe(value: string | readonly string[]): Observable<BreakpointState> {
                            return of({
                                matches: true,
                                breakpoints: {},
                            });
                        },
                    },
                },
            ],
            teardown: { destroyAfterEach: false },
        }).overrideComponent(NavigationComponent, {
            set: { changeDetection: ChangeDetectionStrategy.Default },
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NavigationComponent);
        store = TestBed.inject(Store);
        authService = TestBed.inject(AuthenticationService);
        notificationService = TestBed.inject(NotificationSnackBarService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should match snapshot', () => {
        expect(fixture).toMatchSnapshot();
    });

    it('should switch theme', () => {
        const dispatch = jest.spyOn(store, 'dispatch');

        component.switchTheme();

        expect(dispatch).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalledWith(new SwitchTheme({ isDarkThemeSelected: component.isDarkThemeSelected }));

        component.switchTheme();
        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(dispatch).toHaveBeenCalledWith(new SwitchTheme({ isDarkThemeSelected: component.isDarkThemeSelected }));
    });

    it('should logout', () => {
        const showSuccess = jest.spyOn(notificationService, 'showSuccess');
        const logout = jest.spyOn(authService, 'logout');
        const setLoggedInState = jest.spyOn(authService, 'setLoggedInState');

        component.logout();

        expect(showSuccess).toHaveBeenCalledTimes(1);
        expect(logout).toHaveBeenCalledTimes(1);
        expect(setLoggedInState).toHaveBeenCalledTimes(1);
        expect(setLoggedInState).toHaveBeenCalledWith(false);
    });

    it('should refresh the current page', () => {
        const location: Location = window.location;
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        delete window.location;
        window.location = {
            ...location,
            reload: jest.fn(),
        };

        component.refresh();

        expect(window.location.reload).toHaveBeenCalledTimes(1);
    });

    it('should switch language', () => {
        const dispatch = jest.spyOn(store, 'dispatch');

        component.switchLang('de');

        expect(dispatch).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalledWith(new SwitchLang({ language: 'de' }));

        jest.resetAllMocks();
    });

    it('should match breakpoint', waitForAsync(() => {
        component.isHandset$.subscribe((result: boolean) => {
            expect(result).toBe(true);
        });
    }));
});
