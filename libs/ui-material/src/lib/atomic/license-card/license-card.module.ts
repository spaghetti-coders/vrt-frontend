import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';

import { LicenseCardComponent } from './license-card.component';

@NgModule({
    declarations: [LicenseCardComponent],
    imports: [CommonModule, MatCardModule],
    exports: [LicenseCardComponent],
})
export class LicenseCardModule {}
