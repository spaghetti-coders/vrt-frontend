import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { License } from '@vrt/shared';

@Component({
    selector: 'vrt-license-card',
    templateUrl: './license-card.component.html',
    styleUrls: ['./license-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LicenseCardComponent {
    @Input() license?: License;
}
