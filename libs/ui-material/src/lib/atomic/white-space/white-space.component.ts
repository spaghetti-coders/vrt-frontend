import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'vrt-white-space',
    templateUrl: './white-space.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./white-space.component.scss'],
})
export class WhiteSpaceComponent {}
