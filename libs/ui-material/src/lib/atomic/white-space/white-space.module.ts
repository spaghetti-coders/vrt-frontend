import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslocoModule } from '@ngneat/transloco';
import { WhiteSpaceComponent } from './white-space.component';

@NgModule({
    declarations: [WhiteSpaceComponent],
    imports: [CommonModule, TranslocoModule],
    exports: [WhiteSpaceComponent],
})
export class WhiteSpaceModule {}
