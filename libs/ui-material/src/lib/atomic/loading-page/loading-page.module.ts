import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule } from '@angular/router';
import { TranslocoModule } from '@ngneat/transloco';
import { LoadingPageComponent } from './loading-page.component';

@NgModule({
    declarations: [LoadingPageComponent],
    imports: [
        MatSnackBarModule,
        TranslocoModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: LoadingPageComponent,
            },
        ]),
    ],
})
export class LoadingPageModule {}
