import { ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppUser } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { IS_DEMO_BUILD, UrlParser } from '@vrt/utils';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'vrt-loading-page',
    templateUrl: './loading-page.component.html',
    styleUrls: ['./loading-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingPageComponent implements OnInit, OnDestroy {
    returnUrl = '';

    currentUser$?: Observable<AppUser | null>;

    destroy$: Subject<boolean> = new Subject<boolean>();

    constructor(
        @Inject(IS_DEMO_BUILD) private isDemoBuild: boolean,
        private router: Router,
        private authenticationService: AuthenticationService,
        private activatedRoute: ActivatedRoute,
    ) {}

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    ngOnInit(): void {
        this.getRoute();
        const navigateTo = this.returnUrl === '/' ? '/events' : this.returnUrl;
        if (this.authenticationService.getCurrentState() === null && this.authenticationService.getJwtToken() && !this.isDemoBuild) {
            this.authenticationService.requestCurrentUser(navigateTo);
        } else {
            this.router.navigate(['/auth/login'], {
                queryParams: { returnUrl: navigateTo },
            });
        }

        if (this.isDemoBuild) {
            const user = JSON.parse(sessionStorage.getItem('user') ?? '') as AppUser;
            if (user) {
                this.authenticationService.login({
                    username: user.username,
                    password: '123',
                });
            }
        }

        this.initUser();
    }

    getRoute() {
        this.activatedRoute.queryParams
            .pipe(
                catchError((err) => {
                    return throwError(err);
                }),
                map((item: Params) => {
                    this.returnUrl = item['returnUrl'];
                    return item;
                }),
                takeUntil(this.destroy$),
            )
            .subscribe();
    }

    initUser() {
        this.currentUser$ = this.authenticationService.currentUser$.pipe(
            map((user: AppUser | null) => {
                if (user) {
                    this.router.navigate([UrlParser.returnUrl ?? '/events'], {
                        queryParams: UrlParser.queryParams ?? '',
                    });
                }
                return user;
            }),
        );
    }
}
