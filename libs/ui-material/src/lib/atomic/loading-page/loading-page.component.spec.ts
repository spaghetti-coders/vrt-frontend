import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';
import { AuthenticationService } from '@vrt/auth';
import { IS_DEMO_BUILD } from '@vrt/utils';
import { LoadingPageComponent } from './loading-page.component';

describe('LoadingPageComponent', () => {
    const consoleErr = console.error;
    console.error = jest.fn();

    let component: LoadingPageComponent;
    let fixture: ComponentFixture<LoadingPageComponent>;
    let authService: AuthenticationService;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [LoadingPageComponent],
            imports: [RouterModule.forRoot([]), MatSnackBarModule, NoopAnimationsModule, NgxsModule.forRoot(), HttpClientTestingModule],
            providers: [{ provide: IS_DEMO_BUILD, useValue: false }],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LoadingPageComponent);
        authService = TestBed.inject(AuthenticationService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should match snapshot', () => {
        expect(fixture).toMatchSnapshot();
    });

    it('should destroy the component', () => {
        const next = jest.spyOn(component.destroy$, 'next');
        const complete = jest.spyOn(component.destroy$, 'complete');

        component.ngOnDestroy();

        expect(next).toHaveBeenCalledTimes(1);
        expect(next).toHaveBeenCalledWith(true);
        expect(complete).toHaveBeenCalledTimes(1);
    });

    it('should request current user and navigate to events page', () => {
        const requestCurrentUser = jest.spyOn(authService, 'requestCurrentUser');
        jest.spyOn(authService, 'getCurrentState').mockReturnValue(null);
        authService.getJwtToken = jest.fn(() => 'dsfdsf');

        component.ngOnInit();

        expect(requestCurrentUser).toHaveBeenCalledTimes(1);
    });
});
