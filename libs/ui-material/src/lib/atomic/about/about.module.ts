import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { RouterModule } from '@angular/router';
import { TranslocoModule } from '@ngneat/transloco';
import { LicenseViewModule } from '../license-view/license-view.module';
import { ReleaseCardModule } from '../release-card/release-card.module';

import { AboutComponent } from './about.component';

@NgModule({
    declarations: [AboutComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: AboutComponent,
            },
        ]),
        TranslocoModule,
        MatTabsModule,
        ReleaseCardModule,
        LicenseViewModule,
        MatButtonModule,
    ],
})
export class AboutModule {}
