import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { AppUser } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { AppConfigState } from '@vrt/common';
import { Release, ReleaseHistoryService } from '@vrt/shared';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
    selector: 'vrt-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AboutComponent implements OnInit {
    currentUser$: Observable<AppUser | null> = this._authenticationService.currentUser$;

    isDarkThemeSelected$ = this.store.select(AppConfigState.isDarkThemeSelected);

    currentVersion: Release = {} as Release;

    releases$: Observable<Release[]> = this._releaseHistoryService.releaseHistory$.pipe(
        map((value) => {
            this.currentVersion = value[0];
            return value;
        }),
    );

    isSmallDevice = false;

    isHandset$: Observable<boolean> = this._breakpointObserver.observe(Breakpoints.Handset).pipe(
        map((result) => (this.isSmallDevice = result.matches)),
        shareReplay(),
    );

    constructor(
        private _authenticationService: AuthenticationService,
        private _location: Location,
        private _releaseHistoryService: ReleaseHistoryService,
        private _breakpointObserver: BreakpointObserver,
        private store: Store,
    ) {}

    ngOnInit(): void {
        this._releaseHistoryService.loadHistory();
    }

    navigateBack(): void {
        this._location.back();
    }
}
