import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { IS_DEMO_BUILD } from '@vrt/utils';
import { LicenseCardComponent } from '../license-card/license-card.component';
import { LicenseViewComponent } from '../license-view/license-view.component';
import { ReleaseCardComponent } from '../release-card/release-card.component';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule } from '@angular/router';
import { AboutComponent } from './about.component';

describe('AboutComponent', () => {
    let component: AboutComponent;
    let fixture: ComponentFixture<AboutComponent>;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [AboutComponent, ReleaseCardComponent, LicenseViewComponent, LicenseCardComponent],
            imports: [
                MatTabsModule,
                MatCardModule,
                getTranslocoModule(),
                MatSnackBarModule,
                RouterModule.forRoot([]),
                HttpClientTestingModule,
                BrowserAnimationsModule,
                NgxsModule.forRoot(),
            ],
            providers: [{ provide: IS_DEMO_BUILD, useValue: false }],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AboutComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
