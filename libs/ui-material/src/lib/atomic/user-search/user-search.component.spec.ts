import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';
import { AppUser } from '@vrt/api';
import { getTranslocoModule } from '@vrt/common';
import { MockModule } from 'ng-mocks';
import { UserSearchComponent } from './user-search.component';

describe('UserSearchComponent', () => {
    let component: UserSearchComponent;
    let fixture: ComponentFixture<UserSearchComponent>;
    let dialogRef: MatDialogRef<UserSearchComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [UserSearchComponent],
            imports: [
                RouterTestingModule,
                BrowserAnimationsModule,
                getTranslocoModule(),
                ReactiveFormsModule,
                FormsModule,
                MockModule(MatAutocompleteModule),
                MockModule(MatDialogModule),
                MockModule(MatFormFieldModule),
                NgxsModule.forRoot([], { developmentMode: true }),
                HttpClientTestingModule,
            ],
            providers: [{ provide: MatDialogRef, useValue: jest.fn() }],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UserSearchComponent);
        dialogRef = TestBed.inject(MatDialogRef);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call select function', () => {
        component.user.emit = jest.fn();
        const user = { displayName: 'Cypress' } as AppUser;
        component.selectUser(user);
        expect(component.user.emit).toHaveBeenCalledWith(user);
    });

    it('should search for a new user', () => {
        const spy = jest.spyOn(component.searchTerm, 'emit');
        component.onSearch('EXAMPLE');
        expect(spy).toHaveBeenCalledWith('EXAMPLE');
    });
});
