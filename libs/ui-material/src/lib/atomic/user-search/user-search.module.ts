import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { TranslocoModule } from '@ngneat/transloco';
import { NgxsModule } from '@ngxs/store';
import { AdminUserState } from '@vrt/admin-api';
import { UserSearchComponent } from './user-search.component';

@NgModule({
    declarations: [UserSearchComponent],
    imports: [
        CommonModule,
        MatDialogModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatProgressBarModule,
        MatButtonModule,
        MatInputModule,
        NgxsModule.forFeature([AdminUserState]),
        TranslocoModule,
    ],
    exports: [UserSearchComponent],
})
export class UserSearchModule {}
