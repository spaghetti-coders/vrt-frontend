import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { AppUser } from '@vrt/api';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';

@Component({
    selector: 'vrt-user-search',
    templateUrl: './user-search.component.html',
    styleUrls: ['./user-search.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserSearchComponent implements OnInit {
    @Input() isLoading: boolean | null = false;

    @Input() users: AppUser[] | undefined | null = [];

    @Input() appearance: MatFormFieldAppearance = 'outline';

    @Output() searchTerm: EventEmitter<string> = new EventEmitter<string>();

    @Output() user: EventEmitter<AppUser> = new EventEmitter<AppUser>();

    searchResult$?: Observable<any>;

    search: UntypedFormControl = new UntypedFormControl();

    ngOnInit(): void {
        this.searchResult$ = this.search?.valueChanges.pipe(
            debounceTime(500),
            distinctUntilChanged(),
            tap((value) => {
                this.onSearch(value);
            }),
        );
    }

    selectUser(user: AppUser): void {
        this.user.emit(user);
    }

    onSearch(value: string) {
        this.searchTerm.emit(value);
    }
}
