import { Location } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { IS_DEMO_BUILD } from '@vrt/utils';
import { Spy, createSpyFromClass } from 'jest-auto-spies';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule } from '@angular/router';
import { ImpressumComponent } from './impressum.component';

describe('ImpressumComponent', () => {
    let component: ImpressumComponent;
    let fixture: ComponentFixture<ImpressumComponent>;
    let location: Spy<Location> = createSpyFromClass(Location);

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [MatSnackBarModule, RouterModule.forRoot([]), getTranslocoModule(), NgxsModule.forRoot(), HttpClientTestingModule],
            declarations: [ImpressumComponent],
            providers: [
                { provide: IS_DEMO_BUILD, useValue: false },
                { provide: Location, useValue: location },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ImpressumComponent);
        component = fixture.componentInstance;
        location = TestBed.inject<any>(Location);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call back function on button click', () => {
        component.navigateBack();
        expect(location.back).toHaveBeenCalled();
    });
});
