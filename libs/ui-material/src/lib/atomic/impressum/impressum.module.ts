import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { TranslocoModule } from '@ngneat/transloco';

import { ImpressumComponent } from './impressum.component';

@NgModule({
    declarations: [ImpressumComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ImpressumComponent,
            },
        ]),
        TranslocoModule,
        MatButtonModule,
    ],
})
export class ImpressumModule {}
