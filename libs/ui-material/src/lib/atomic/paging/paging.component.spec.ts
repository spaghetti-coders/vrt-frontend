import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { PagingComponent } from './paging.component';

describe('PagingComponent', () => {
    let component: PagingComponent;
    let fixture: ComponentFixture<PagingComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [PagingComponent],
            imports: [MatPaginatorModule],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PagingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(fixture).toMatchSnapshot();
    });

    it('should init default pageSizes', () => {
        expect(component.pageSizeOptions).toEqual([5, 10, 25]);
    });

    it('should change page option', () => {
        const spy = jest.spyOn(component.changePage, 'emit');
        const pageEvent: PageEvent = {
            pageIndex: 1,
            pageSize: 10,
            previousPageIndex: 0,
            length: 10,
        };
        component.pageOptionChanged(pageEvent);
        expect(spy).toHaveBeenCalledWith({
            length: 10,
            pageIndex: 2,
            pageSize: 10,
            previousPageIndex: 0,
        });
    });
});
