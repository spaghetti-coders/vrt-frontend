import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Metadata } from '@vrt/api';
import { Observable } from 'rxjs';

@Component({
    selector: 'vrt-paging',
    templateUrl: './paging.component.html',
    styleUrls: ['./paging.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PagingComponent implements AfterViewInit {
    @Input() metadata$!: Observable<Metadata>;

    @ViewChild(MatPaginator) paginator?: MatPaginator;

    @Output() changePage: EventEmitter<PageEvent> = new EventEmitter<PageEvent>();

    pageSizeOptions: number[] = [5, 10, 25];

    pageOptionChanged($event: PageEvent): void {
        $event.pageIndex = $event.pageIndex + 1;
        this.changePage.emit($event);
    }

    ngAfterViewInit(): void {
        if (this.paginator?._intl) {
            this.paginator._intl.itemsPerPageLabel = 'Items';
        }
    }
}
