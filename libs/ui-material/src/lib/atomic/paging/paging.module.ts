import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PagingComponent } from './paging.component';

@NgModule({
    declarations: [PagingComponent],
    imports: [CommonModule, MatPaginatorModule],
    exports: [PagingComponent],
})
export class PagingModule {}
