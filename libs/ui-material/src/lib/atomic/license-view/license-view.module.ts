import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LicenseCardModule } from '../license-card/license-card.module';

import { LicenseViewComponent } from './license-view.component';

@NgModule({
    declarations: [LicenseViewComponent],
    imports: [CommonModule, LicenseCardModule],
    exports: [LicenseViewComponent],
})
export class LicenseViewModule {}
