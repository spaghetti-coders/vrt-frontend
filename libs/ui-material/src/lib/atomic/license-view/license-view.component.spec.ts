import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LicenseService } from '@vrt/shared';
import { LicenseCardComponent } from '../license-card/license-card.component';

import { LicenseViewComponent } from './license-view.component';

describe('LicenseViewComponent', () => {
    let component: LicenseViewComponent;
    let fixture: ComponentFixture<LicenseViewComponent>;
    let licenseService: LicenseService;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            declarations: [LicenseViewComponent, LicenseCardComponent],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LicenseViewComponent);
        licenseService = TestBed.inject(LicenseService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call init license', () => {
        jest.spyOn(licenseService, 'loadLicense');
        component.ngOnInit();
        expect(licenseService.loadLicense).toHaveBeenCalledTimes(1);
    });
});
