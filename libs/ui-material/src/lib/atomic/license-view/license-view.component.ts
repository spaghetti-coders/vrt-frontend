import { BreakpointObserver } from '@angular/cdk/layout';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { License, LicenseService } from '@vrt/shared';
import { Observable } from 'rxjs';

@Component({
    selector: 'vrt-license-view',
    templateUrl: './license-view.component.html',
    styleUrls: ['./license-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LicenseViewComponent implements OnInit {
    @Input() isSmallDevice = false;

    backendLicenses$: Observable<License[]> = this.licenseService.backendLicenses$;

    frontendLicenses$: Observable<License[]> = this.licenseService.frontendLicenses$;

    constructor(
        private licenseService: LicenseService,
        private _breakpointObserver: BreakpointObserver,
    ) {}

    ngOnInit(): void {
        this.initLicense();
    }

    initLicense(): void {
        this.licenseService.loadLicense();
    }
}
