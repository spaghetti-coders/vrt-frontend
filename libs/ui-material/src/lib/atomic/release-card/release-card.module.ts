import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { TranslocoModule } from '@ngneat/transloco';

import { ReleaseCardComponent } from './release-card.component';

@NgModule({
    declarations: [ReleaseCardComponent],
    imports: [CommonModule, MatCardModule, TranslocoModule],
    exports: [ReleaseCardComponent],
})
export class ReleaseCardModule {}
