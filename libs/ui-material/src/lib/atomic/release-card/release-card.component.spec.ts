import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { getTranslocoModule } from '@vrt/common';
import { MockModule } from 'ng-mocks';

import { ReleaseCardComponent } from './release-card.component';

describe('ReleaseCardComponent', () => {
    let component: ReleaseCardComponent;
    let fixture: ComponentFixture<ReleaseCardComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [MockModule(MatIconModule), MockModule(MatCardModule), getTranslocoModule()],
            declarations: [ReleaseCardComponent],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ReleaseCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
