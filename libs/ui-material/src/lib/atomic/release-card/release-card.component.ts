import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Release } from '@vrt/shared';

@Component({
    selector: 'vrt-release-card',
    templateUrl: './release-card.component.html',
    styleUrls: ['./release-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReleaseCardComponent {
    @Input() release: Release = {} as Release;

    @Input() currentVersion = false;
}
