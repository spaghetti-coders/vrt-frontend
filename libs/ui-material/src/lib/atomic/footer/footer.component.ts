import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'vrt-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent {
    @Input()
    isDarkThemeSelected = false;

    year = new Date().getFullYear();
}
