import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { NgxsModule } from '@ngxs/store';
import { getTranslocoModule } from '@vrt/common';
import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
    let fixture: ComponentFixture<FooterComponent>;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [FooterComponent],
            imports: [MatButtonModule, NgxsModule.forRoot(), getTranslocoModule()],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(FooterComponent);
        fixture.detectChanges();
    });

    it('should match snapshot', () => {
        expect(fixture).toMatchSnapshot();
    });
});
