import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { TranslocoModule } from '@ngneat/transloco';
import { ConfirmDialogComponent } from './confirm-dialog.component';

@NgModule({
    declarations: [ConfirmDialogComponent],
    imports: [MatDialogModule, MatButtonModule, TranslocoModule, CommonModule],
    exports: [ConfirmDialogComponent],
})
export class ConfirmDialogModule {}
