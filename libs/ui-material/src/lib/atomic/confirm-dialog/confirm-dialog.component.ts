import { Component } from '@angular/core';

@Component({
    selector: 'vrt-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent {}
