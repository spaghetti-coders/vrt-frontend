import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AppUser } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';

@Directive({
    selector: '[vrtHasPermission]',
})
export class HasPermissionDirective implements OnInit {
    private currentUser?: AppUser | null;

    private permissions: string[] = [];

    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private authenticationService: AuthenticationService,
    ) {}

    @Input()
    set vrtHasPermission(roles: string[]) {
        this.permissions = [...roles];
        this.updateView();
    }

    ngOnInit(): void {
        this.currentUser = this.authenticationService.getCurrentState();
        this.updateView();
    }

    private updateView(): void {
        if (this.checkPermission()) {
            this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
            this.viewContainer.clear();
        }
    }

    private checkPermission(): boolean {
        let hasPermission = false;
        if (this.currentUser && this.currentUser.role) {
            hasPermission = this.permissions.includes(this.currentUser.role);
        }
        return hasPermission;
    }
}
