import { TemplateRef, ViewContainerRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { AuthenticationService } from '@vrt/auth';
import { USER_EXAMPLE } from '@vrt/testing';
import { HasPermissionDirective } from './has-permission.directive';

describe('HasPermissionDirective', () => {
    let authService: AuthenticationService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: AuthenticationService,
                    useValue: {
                        getCurrentState: () => USER_EXAMPLE,
                    },
                },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    it('should create an instance', () => {
        authService = TestBed.inject(AuthenticationService);
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        const directive = new HasPermissionDirective(
            {} as TemplateRef<any>,
            {
                clear() {},
                createEmbeddedView: (templateRef) => {},
            } as ViewContainerRef,
            authService,
        );
        directive.vrtHasPermission = ['user'];
        directive.ngOnInit();
        expect(directive).toBeTruthy();
    });
});
