import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { NotificationsService, PushSubscriptionModel } from '@vrt/api';
import { EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class WebNotificationService {
    readonly VAPID_PUBLIC_KEY = 'BNpGqIAIEH8sUAr7mHxH2j96LOOj0A5OY6B7GycTqx1sE14OzztajLL8b2dWAzCYAEnlkJ5wTgwV5psEwc9_9wY';

    constructor(
        private httpClient: HttpClient,
        private swPush: SwPush,
        private notificationsService: NotificationsService,
    ) {
        this.onNewEvent();
    }

    get isEnabled(): boolean {
        return this.swPush.isEnabled;
    }

    onNewEvent() {
        this.swPush.notificationClicks.subscribe((event: { action: string; notification: NotificationOptions & { title: string } }) => {
            const url = event.notification.data.url;
            if (url) {
                window.open(url, '_blank');
            }
        });
    }

    subscribeToNotifications(): Promise<void> {
        return this.swPush
            .requestSubscription({
                serverPublicKey: this.VAPID_PUBLIC_KEY,
            })
            .then((sub) => {
                this.sendToServer(sub);
            })
            .catch((err) => console.error('Could not subscribe to notifications', err));
    }

    unSubscribeToNotifications(): void {
        this.notificationsService.unsubscribeFromPush().subscribe();
    }

    private sendToServer({ keys, endpoint }: PushSubscriptionJSON): void {
        if (!!keys && !!endpoint) {
            const pushSubscriptionModel: PushSubscriptionModel = {
                endpoint,
                p256dh: keys['p256dh'],
                auth: keys['auth'],
            };
            this.notificationsService
                .subscribeToPush(pushSubscriptionModel)
                .pipe(
                    catchError((err) => {
                        console.error(err);
                        return EMPTY;
                    }),
                )
                .subscribe();
        }
    }
}
