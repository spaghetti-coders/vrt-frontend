import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { LicenseService } from './license.service';

describe('LicenseService', () => {
    let service: LicenseService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            teardown: { destroyAfterEach: false },
        });
        service = TestBed.inject(LicenseService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
