import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ServiceWorkerModule, SwPush } from '@angular/service-worker';
import { NotificationsService } from '@vrt/api';
import { NotificationSnackBarService } from '@vrt/utils';
import { of, throwError } from 'rxjs';
import { WebNotificationService } from './web-notification.service';

describe('WebNotificationService', () => {
    let service: WebNotificationService;
    let swPush: SwPush;
    let notificationsService: NotificationsService;
    let notificationSnackBarService: NotificationSnackBarService;
    console.error = jest.fn();

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, ServiceWorkerModule.register('', { enabled: false }), MatSnackBarModule],
            providers: [
                NotificationSnackBarService,
                {
                    provide: SwPush,
                    useValue: {
                        isEnabled: true,
                        notificationClicks: of({
                            action: '',
                            notification: {
                                data: {
                                    url: 'EXAMPLE',
                                },
                            },
                        }),
                        requestSubscription: jest.fn(() => {
                            return Promise.resolve({
                                endpoint: 'ENDPOINT',
                                keys: { p256dh: 'nkjsndfjknsf', auth: 'nskdjnfjksnfk' },
                            });
                        }),
                    },
                },
            ],
            teardown: { destroyAfterEach: false },
        });
        service = TestBed.inject(WebNotificationService);
        swPush = TestBed.inject(SwPush);
        notificationsService = TestBed.inject(NotificationsService);
        notificationSnackBarService = TestBed.inject(NotificationSnackBarService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should open new window tab', () => {
        window.open = jest.fn();
        service.onNewEvent();
        expect(window.open).toHaveBeenCalled();
        expect(window.open).toHaveBeenCalledWith('EXAMPLE', '_blank');
    });

    it('should return swPush is enabled status', () => {
        expect(service.isEnabled).toBeTruthy();
    });

    it('should unsubscribe to notifications', () => {
        notificationsService.unsubscribeFromPush = jest.fn().mockReturnValue(of({}));
        service.unSubscribeToNotifications();
        expect(notificationsService.unsubscribeFromPush).toHaveBeenCalled();
    });

    it('should subscribe to notifications', fakeAsync(() => {
        notificationsService.subscribeToPush = jest.fn();
        const requestSubscription = jest.spyOn(swPush, 'requestSubscription');
        service.subscribeToNotifications().then(() => {
            expect(requestSubscription).toHaveBeenCalled();
            expect(notificationsService.subscribeToPush).toHaveBeenCalled();
            expect(notificationsService.subscribeToPush).toHaveBeenCalledWith({
                auth: 'nskdjnfjksnfk',
                endpoint: 'ENDPOINT',
                p256dh: 'nkjsndfjknsf',
            });
        });
        tick();
    }));

    it('should throw an erroe when subscribe to notifications', fakeAsync(() => {
        const requestSubscription = jest.spyOn(swPush, 'requestSubscription');

        notificationSnackBarService.showError = jest.fn();
        notificationsService.subscribeToPush = jest.fn(() => throwError(() => new Error('ERROR')));
        console.error = jest.fn();

        service.subscribeToNotifications().then(() => {
            expect(requestSubscription).toHaveBeenCalled();
            expect(notificationsService.subscribeToPush).toHaveBeenCalled();
            expect(notificationsService.subscribeToPush).toHaveBeenCalledWith({
                auth: 'nskdjnfjksnfk',
                endpoint: 'ENDPOINT',
                p256dh: 'nkjsndfjknsf',
            });
            expect(console.error).toHaveBeenCalled();
            expect(console.error).toHaveBeenCalledWith(new Error('ERROR'));
        });
        tick();
    }));
});
