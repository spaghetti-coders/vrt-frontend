import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { ReleaseHistoryService } from './release-history.service';

describe('ReleaseService', () => {
    let service: ReleaseHistoryService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            teardown: { destroyAfterEach: false },
        });
        service = TestBed.inject(ReleaseHistoryService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
