import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Release } from '../models';

@Injectable({
    providedIn: 'root',
})
export class ReleaseHistoryService {
    private _releaseHistory: BehaviorSubject<Release[]> = new BehaviorSubject<Release[]>([]);

    // eslint-disable-next-line @typescript-eslint/member-ordering
    releaseHistory$: Observable<Release[]> = this._releaseHistory.asObservable();

    constructor(private httpClient: HttpClient) {}

    loadHistory(): void {
        this.httpClient.get<any>('assets/data/history.json').subscribe((res) => {
            this._releaseHistory.next(res.releases);
        });
    }
}
