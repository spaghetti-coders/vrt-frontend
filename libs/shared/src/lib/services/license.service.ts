import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { License } from '../models';

@Injectable({
    providedIn: 'root',
})
export class LicenseService {
    private _backendLicenses: BehaviorSubject<License[]> = new BehaviorSubject<License[]>([]);

    // eslint-disable-next-line @typescript-eslint/member-ordering
    backendLicenses$: Observable<License[]> = this._backendLicenses.asObservable();

    private _frontendLicenses: BehaviorSubject<License[]> = new BehaviorSubject<License[]>([]);

    // eslint-disable-next-line @typescript-eslint/member-ordering
    frontendLicenses$: Observable<License[]> = this._frontendLicenses.asObservable();

    constructor(private _httpClient: HttpClient) {}

    loadLicense(): void {
        this._httpClient.get<any>('assets/data/3rd-party-licenses.json').subscribe((res) => {
            this._backendLicenses.next(res.backend);
            this._frontendLicenses.next(res.frontend);
        });
    }
}
