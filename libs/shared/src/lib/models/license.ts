export interface License {
    library: string;
    copyright: string;
    license: string;
    projectUrl: string;
    personalUrl: string;
    licenseUrl: string;
}
