export interface Release {
    number: string;
    timestamp: string;
    added: string[];
    changed: string[];
    deleted: string[];
}
