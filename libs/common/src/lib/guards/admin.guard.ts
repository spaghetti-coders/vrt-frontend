import { Injectable } from '@angular/core';
import { Router, UrlTree } from '@angular/router';
import { AuthenticationService } from '@vrt/auth';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class AdminGuard {
    constructor(
        private authenticationService: AuthenticationService,
        private router: Router,
    ) {}

    canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (this.authenticationService.isAdmin()) {
            return true;
        } else {
            this.router.navigateByUrl('/events');
            return false;
        }
    }
}
