import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthenticationService } from '@vrt/auth';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class RoleGuard {
    constructor(
        private _authenticationService: AuthenticationService,
        private _router: Router,
    ) {}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this._authenticationService.isInRole(['manager', 'admin']);
    }
}
