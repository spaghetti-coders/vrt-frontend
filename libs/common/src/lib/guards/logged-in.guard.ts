import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '@vrt/auth';
import { IS_DEMO_BUILD } from '@vrt/utils';

@Injectable({
    providedIn: 'root',
})
export class LoggedInGuard {
    isLoggedIn = false;

    constructor(
        @Inject(IS_DEMO_BUILD) private isDemoBuild: boolean,
        private authenticationService: AuthenticationService,
        private router: Router,
    ) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if ((this.isDemoBuild && !this.isLoggedIn) || this.authenticationService.getCurrentState() === null) {
            this.router.navigate(['/check'], {
                queryParams: { returnUrl: state.url },
            });
            this.isLoggedIn = true;
        }

        if (this.authenticationService.getCurrentState() !== null) {
            this.isLoggedIn = true;
            return this.isLoggedIn;
        }
        return this.isLoggedIn;
    }
}
