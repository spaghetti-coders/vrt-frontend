import { HttpClient, HttpContext } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { Store } from '@ngxs/store';
import { AppVenue, AppVenuePagedResult, BASE_PATH, Configuration, VenueModel, VenuesService } from '@vrt/api';
import { EVENT_EXAMPLE, METADATA_EXAMPLE, VENUE_EXAMPLE } from '@vrt/testing';
import { VenuesState } from '@vrt/venue-api';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class VenuesMockService extends VenuesService {
    constructor(
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(BASE_PATH) basePath: string,
        @Optional() configuration: Configuration,
        private store: Store,
    ) {
        super(http, basePath, configuration);
    }

    override findAvailableVenues(
        page?: number,
        pageSize?: number,
        filter?: string,
        sortBy?: string,
        sortOrder?: string,
        observe?: 'body',
        reportProgress?: boolean,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<AppVenuePagedResult> {
        return of({
            items: [VENUE_EXAMPLE],
            metadata: { ...METADATA_EXAMPLE },
        } as AppVenuePagedResult) as Observable<AppVenuePagedResult>;
    }

    override findVenues(
        page?: number,
        pageSize?: number,
        filter?: string,
        sortBy?: string,
        sortOrder?: string,
        observe?: 'body',
        reportProgress?: boolean,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<AppVenuePagedResult> {
        return of({
            items: [VENUE_EXAMPLE],
            metadata: { ...METADATA_EXAMPLE },
        } as AppVenuePagedResult);
    }

    override createVenue(
        venueModel?: VenueModel,
        observe?: 'body',
        reportProgress?: boolean,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<AppVenue> {
        const event = {
            ...EVENT_EXAMPLE,
            ...(observe as any),
            id: (this.store.selectSnapshot(VenuesState.venues)?.length ?? 0) + 1,
        } as AppVenue;
        return of(event);
    }

    override deleteVenue(
        id: number,
        observe?: 'body',
        reportProgress?: boolean,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of(true);
    }

    override updateVenue(
        id: number,
        venueModel?: VenueModel,
        observe?: 'body',
        reportProgress?: boolean,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<AppVenue> {
        const venue = this.store.selectSnapshot(VenuesState.venues)?.find((value) => value.id === id);
        return of({ ...venue, ...venueModel } as AppVenue);
    }
}
