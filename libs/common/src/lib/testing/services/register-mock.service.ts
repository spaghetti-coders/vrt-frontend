import { HttpClient, HttpContext } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import {
    AppRegistration,
    AppRegistrationPagedResult,
    BASE_PATH,
    Configuration,
    EventRegistrationModel,
    EventRegistrationsService,
    RegistrationStatus,
} from '@vrt/api';
import { EXAMPLE_REGISTRATIONS, METADATA_EXAMPLE, REGISTRATION_EXAMPLE } from '@vrt/testing';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class RegisterMockService extends EventRegistrationsService {
    constructor(
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(BASE_PATH) basePath: string,
        @Optional() configuration: Configuration,
    ) {
        super(http, basePath, configuration);
    }

    override registerToEvent(
        eventId: number,
        observe?: 'body',
        reportProgress?: boolean,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<AppRegistration> {
        return of({
            eventId,
            userId: 10,
            registrationDate: new Date(Date.now()).toISOString(),
            userDisplayName: 'Max Mustermann',
            status: Math.floor(Math.random() * 2) + 1,
        } as AppRegistration);
    }

    override addRegistration(
        eventRegistrationModel?: EventRegistrationModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of(REGISTRATION_EXAMPLE);
    }

    override deleteRegistration(
        eventRegistrationModel?: EventRegistrationModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return super.deleteRegistration(eventRegistrationModel, observe, reportProgress, options);
    }

    override findRegistrationsByEvent(
        eventId: number,
        sortBy?: string,
        status?: RegistrationStatus,
        page?: number,
        pageSize?: number,
        filter?: string,
        sortOrder?: string,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of({
            items: EXAMPLE_REGISTRATIONS,
            metaData: METADATA_EXAMPLE,
        } as AppRegistrationPagedResult);
    }

    override findRegistrationsByUser(
        userId: number,
        sortBy?: string,
        status?: RegistrationStatus,
        page?: number,
        pageSize?: number,
        filter?: string,
        sortOrder?: string,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return super.findRegistrationsByUser(userId, sortBy, status, page, pageSize, filter, sortOrder, observe, reportProgress, options);
    }

    override unregisterFromEvent(
        eventId: number,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of(true);
    }
}
