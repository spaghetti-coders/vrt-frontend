import { HttpClient, HttpContext } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { Store } from '@ngxs/store';
import {
    AccountService,
    AppUser,
    BASE_PATH,
    Configuration,
    LoginModel,
    PasswordResetModel,
    RecoveryTokenRequest,
    RefreshTokenRequest,
    RegistrationModel,
    UpdateAccountModel,
    UpdatePasswordModel,
} from '@vrt/api';
import { Observable, of } from 'rxjs';

@Injectable()
export class AccountMockService extends AccountService {
    constructor(
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(BASE_PATH) basePath: string,
        @Optional() configuration: Configuration,
        private store: Store,
    ) {
        super(http, basePath, configuration);
    }

    override deleteAccount(
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of(true);
    }

    override getCurrentUser(
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return super.getCurrentUser(observe, reportProgress, options);
    }

    override login(
        loginModel?: LoginModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return super.login(loginModel, observe, reportProgress, options);
    }

    override logout(
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of(true);
    }

    override refreshToken(
        refreshTokenRequest?: RefreshTokenRequest,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return super.refreshToken(refreshTokenRequest, observe, reportProgress, options);
    }

    override register(
        registrationModel?: RegistrationModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return super.register(registrationModel, observe, reportProgress, options);
    }

    override requestRecoveryToken(
        recoveryTokenRequest?: RecoveryTokenRequest,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of(true);
    }

    override resetPassword(
        passwordResetModel?: PasswordResetModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of(true);
    }

    override upateAccount(
        updateAccountModel?: UpdateAccountModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        const user = JSON.parse(sessionStorage.getItem('user') ?? '') as AppUser;
        return of({ ...user, ...updateAccountModel });
    }

    override upatePassword(
        updatePasswordModel?: UpdatePasswordModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of(true);
    }
}
