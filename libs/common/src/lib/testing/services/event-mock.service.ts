import { HttpClient, HttpContext } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { Store } from '@ngxs/store';
import { AppEvent, AppEventPagedResult, BASE_PATH, Configuration, EventModel, EventsService } from '@vrt/api';
import { EventsState, RemoveEventSaved } from '@vrt/event-api';
import { EVENT_EXAMPLE, METADATA_EXAMPLE } from '@vrt/testing';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class EventMockService extends EventsService {
    constructor(
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(BASE_PATH) basePath: string,
        @Optional() configuration: Configuration,
        private store: Store,
    ) {
        super(http, basePath, configuration);
    }

    override createEvent(
        eventModel?: EventModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        const event = {
            ...EVENT_EXAMPLE,
            ...eventModel,
            id: (this.store.selectSnapshot(EventsState.events)?.length ?? 0) + 1,
        } as AppEvent;
        return of(event);
    }

    override deleteEvent(
        id: number,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        this.store.dispatch(new RemoveEventSaved({ id }));
        return of(true);
    }

    override findAvailableEvents(
        sortBy?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        sortOrder?: string,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of({
            items: [
                { ...EVENT_EXAMPLE, id: 0 },
                { ...EVENT_EXAMPLE, date: new Date(2099, 1, 16), id: 1 },
            ],
            metadata: { ...METADATA_EXAMPLE },
        } as AppEventPagedResult);
    }

    override findEvents(
        sortBy?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        sortOrder?: string,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return super.findEvents(sortBy, page, pageSize, filter, sortOrder, observe, reportProgress, options);
    }

    override findFutureEvents(
        sortBy?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        sortOrder?: string,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of({
            items: [
                { ...EVENT_EXAMPLE, id: 0 },
                { ...EVENT_EXAMPLE, date: new Date(2099, 1, 16), id: 1 },
            ],
            metadata: { ...METADATA_EXAMPLE },
        } as AppEventPagedResult);
    }

    override findPastEvents(
        sortOrder?: string,
        sortBy?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of({
            items: [
                { ...EVENT_EXAMPLE, date: new Date(2019, 12, 25).toISOString(), id: 0 },
                {
                    ...EVENT_EXAMPLE,
                    date: new Date(2020, 1, 16).toISOString(),
                    id: 1,
                },
            ],
            metadata: { ...METADATA_EXAMPLE },
        } as AppEventPagedResult);
    }

    override getEvent(
        id: number,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return super.getEvent(id, observe, reportProgress, options);
    }

    override updateEvent(
        id: number,
        eventModel?: EventModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        const event = this.store.selectSnapshot(EventsState.events)?.find((value) => value.id === id);
        return of({ ...event, ...eventModel } as AppEvent);
    }
}
