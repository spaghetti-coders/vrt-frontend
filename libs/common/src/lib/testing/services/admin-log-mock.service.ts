import { HttpClient, HttpContext } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { Store } from '@ngxs/store';
import { AdminLogService, AppLogEntryPagedResult, BASE_PATH, Configuration } from '@vrt/api';
import { LOGS_EXAMPLE, METADATA_EXAMPLE } from '@vrt/testing';
import { Observable, of } from 'rxjs';

@Injectable()
export class AdminLogMockService extends AdminLogService {
    constructor(
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(BASE_PATH) basePath: string,
        @Optional() configuration: Configuration,
        private store: Store,
    ) {
        super(http, basePath, configuration);
    }

    override findLogEntries(
        sortBy?: string,
        sortOrder?: string,
        page?: number,
        pageSize?: number,
        filter?: string,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of({
            items: LOGS_EXAMPLE,
            metaData: METADATA_EXAMPLE,
        } as AppLogEntryPagedResult);
    }
}
