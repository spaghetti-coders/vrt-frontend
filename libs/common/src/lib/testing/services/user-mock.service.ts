import { HttpClient, HttpContext } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { Store } from '@ngxs/store';
import { AdminUserState } from '@vrt/admin-api';
import {
    AppUser,
    AppUserPagedResult,
    BASE_PATH,
    Configuration,
    UpdateUserModel,
    UpdateUserStatusModel,
    UserStatus,
    UsersService,
} from '@vrt/api';
import { METADATA_EXAMPLE, USERS_EXAMPLE } from '@vrt/testing';
import { Observable, of } from 'rxjs';

@Injectable()
export class UserMockService extends UsersService {
    constructor(
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(BASE_PATH) basePath: string,
        @Optional() configuration: Configuration,
        private store: Store,
    ) {
        super(http, basePath, configuration);
    }

    override deleteUser(
        id: number,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of(true);
    }

    override findUsers(
        status?: UserStatus,
        page?: number,
        pageSize?: number,
        filter?: string,
        sortBy?: string,
        sortOrder?: string,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of({
            items: USERS_EXAMPLE,
            metaData: METADATA_EXAMPLE,
        } as AppUserPagedResult);
    }

    override generateRecoveryToken(
        id: number,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        return of({
            recoveryToken: 'NEW_TOKEN?!',
            expires: new Date(2099, 12, 23).toISOString(),
        });
    }

    override getUser(
        id: number,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        const user = this.store.selectSnapshot(AdminUserState.users)?.find((stateUser) => stateUser.id === id) ?? ({} as AppUser);
        return of(user);
    }

    override updateUser(
        id: number,
        updateUserModel?: UpdateUserModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        const user = this.store.selectSnapshot(AdminUserState.users)?.find((stateUser) => stateUser.id === id) ?? ({} as AppUser);
        return of({ ...user, ...updateUserModel });
    }

    override updateUserStatus(
        id: number,
        updateUserStatusModel?: UpdateUserStatusModel,
        observe: any = 'body',
        reportProgress = false,
        options?: {
            httpHeaderAccept?: 'text/plain' | 'application/json' | 'text/json';
            context?: HttpContext;
        },
    ): Observable<any> {
        const user = this.store.selectSnapshot(AdminUserState.users)?.find((value) => value.id === id);
        return of({ ...user, ...updateUserStatusModel } as AppUser);
    }
}
