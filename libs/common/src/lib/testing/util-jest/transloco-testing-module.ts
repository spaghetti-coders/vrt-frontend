import { TranslocoTestingModule, TranslocoTestingOptions } from '@ngneat/transloco';

const de = {};

export function getTranslocoModule(options: TranslocoTestingOptions = {}) {
    return TranslocoTestingModule.forRoot({
        langs: { de },
        translocoConfig: {
            availableLangs: ['de'],
            defaultLang: 'de',
        },
        preloadLangs: true,
        ...options,
    });
}
