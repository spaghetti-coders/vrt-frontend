import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { NgxsModule, Store } from '@ngxs/store';
import { EventsState, FindRegistration } from '@vrt/event-api';
import { DEFAULTPARAMETERS, NotificationSnackBarService } from '@vrt/utils';
import { provideAutoSpy } from 'jest-auto-spies';
import { RegistrationsResolver } from './registrations.resolver';

describe('RegistrationsResolver', () => {
    let resolver: RegistrationsResolver;
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([EventsState]), HttpClientTestingModule, RouterModule.forRoot([])],
            providers: [provideAutoSpy(NotificationSnackBarService)],
            teardown: { destroyAfterEach: false },
        });
        resolver = TestBed.inject(RegistrationsResolver);
        store = TestBed.inject(Store);
    });

    it('should be created', () => {
        expect(resolver).toBeTruthy();
    });

    it('should load the registrations by event id', () => {
        const spy = jest.spyOn(store, 'dispatch');
        const resolve = resolver.resolve({
            paramMap: new Map([['eventId', '2']]),
            queryParams: {},
        } as any);

        expect(spy).toHaveBeenCalledWith(
            new FindRegistration({
                registrationsApiParameters: {
                    eventId: 2,
                    ...DEFAULTPARAMETERS,
                    status: undefined,
                },
            }),
        );
        expect(spy.mock.calls.length).toBe(1);
        expect(resolve).toBeTruthy();
    });
});
