import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { NgxsModule, Store } from '@ngxs/store';
import { VenuesService } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { DEFAULTPARAMETERS, IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { LoadVenues, VenuesState } from '@vrt/venue-api';
import { provideAutoSpy } from 'jest-auto-spies';
import { VenuesResolver } from './venues.resolver';

describe('VenuesResolver', () => {
    let resolver: VenuesResolver;
    let authService: AuthenticationService;
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([VenuesState]), HttpClientTestingModule, RouterModule.forRoot([])],
            providers: [
                provideAutoSpy(NotificationSnackBarService),
                { provide: IS_DEMO_BUILD, useValue: false },
                {
                    provide: VenuesService,
                    useValue: {
                        findVenues: jest.fn().mockReturnValue([]),
                    },
                },
                {
                    provide: Store,
                    useValue: {
                        selectSnapshot: jest.fn(),
                        dispatch: jest.fn(),
                    },
                },
            ],
            teardown: { destroyAfterEach: false },
        });
        resolver = TestBed.inject(VenuesResolver);
        store = TestBed.inject(Store);
        authService = TestBed.inject(AuthenticationService);
    });

    it('should be created', () => {
        expect(resolver).toBeTruthy();
    });

    it('should load all venues', () => {
        store.selectSnapshot = jest.fn().mockReturnValue(false);
        authService.isInRole = jest.fn().mockReturnValue(true);
        authService.requestCurrentUser();
        const resolved = resolver.resolve();

        expect(store.dispatch).toHaveBeenCalledWith(new LoadVenues({ apiCallParameters: DEFAULTPARAMETERS }));
        expect(resolved).toBeTruthy();
    });

    it('should load available venues', () => {
        authService.isInRole = jest.fn().mockReturnValue(false);
        const resolved = resolver.resolve();

        expect(store.dispatch).toHaveBeenCalledWith(new LoadVenues({ apiCallParameters: DEFAULTPARAMETERS }));
        expect(resolved).toBeTruthy();
    });
});
