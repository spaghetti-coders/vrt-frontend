import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { provideAutoSpy } from 'jest-auto-spies';

import { RouterModule } from '@angular/router';
import { EventsState, LoadEventById, SetEventFromStore } from '@vrt/event-api';
import { EVENT_EXAMPLE } from '@vrt/testing';
import { EventResolver } from './event.resolver';

describe('EventResolver', () => {
    let resolver: EventResolver;
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([EventsState]), HttpClientTestingModule, RouterModule.forRoot([])],
            providers: [provideAutoSpy(NotificationSnackBarService), { provide: IS_DEMO_BUILD, useValue: false }],
            teardown: { destroyAfterEach: false },
        });
        resolver = TestBed.inject(EventResolver);
        store = TestBed.inject(Store);
    });

    it('should be created', () => {
        expect(resolver).toBeTruthy();
    });

    it('should dispatch Load Event action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        resolver.resolve(<any>{ queryParams: { id: 1 } });

        expect(spy).toHaveBeenCalledWith(new LoadEventById({ id: 1 }));
    });

    it('should dispatch Set Event From State action', () => {
        store.reset({
            appEvents: {
                appEventPagedResult: {
                    items: [EVENT_EXAMPLE],
                },
                selectedEventId: EVENT_EXAMPLE.id,
            },
        });
        const spy = jest.spyOn(store, 'dispatch');
        resolver.resolve(<any>{ queryParams: { id: 12 } });

        expect(spy).toHaveBeenCalledWith(new SetEventFromStore({ id: 12 }));
    });

    it('should dispatch action via paramMap', () => {
        const spy = jest.spyOn(store, 'dispatch');
        resolver.resolve({
            paramMap: new Map([['eventId', '2']]),
            queryParams: {},
        } as any);
        expect(spy).toHaveBeenCalledWith(new LoadEventById({ id: 2 }));
    });

    it('should dispatch action via paramMap', () => {
        const spy = jest.spyOn(store, 'dispatch');
        resolver.resolve({
            paramMap: new Map([['id', '2']]),
            queryParams: {},
        } as any);
        expect(spy).toHaveBeenCalledWith(new LoadEventById({ id: 2 }));
    });
});
