import { Injectable } from '@angular/core';

import { Store } from '@ngxs/store';
import { PaginatorState } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { EventsState, LoadAvailableEvents, LoadFutureEvents, LoadPastEvents } from '@vrt/event-api';
import { FeatureState } from '../+state/index';

@Injectable({
    providedIn: 'root',
})
export class EventsResolver {
    constructor(
        private store: Store,
        private authenticationService: AuthenticationService,
    ) {}

    resolve(): boolean {
        if (this.authenticationService.isInRole(['user'])) {
            this.store.dispatch(
                new LoadAvailableEvents({
                    apiCallParameters: {
                        page: undefined,
                        pageSize: this.store.selectSnapshot(PaginatorState.pageSize),
                        filter: this.store.selectSnapshot(EventsState.filterValue),
                        sortBy: undefined,
                        sortOrder: undefined,
                    },
                }),
            );
        }
        if (this.authenticationService.isInRole(['admin', 'manager'])) {
            if (this.store.selectSnapshot(FeatureState.toggleSwitch)) {
                this.store.dispatch(
                    new LoadFutureEvents({
                        apiCallParameters: {
                            page: undefined,
                            pageSize: this.store.selectSnapshot(PaginatorState.pageSize),
                            filter: this.store.selectSnapshot(EventsState.filterValue),
                            sortBy: undefined,
                            sortOrder: undefined,
                        },
                    }),
                );
            } else {
                this.store.dispatch(
                    new LoadPastEvents({
                        filter: this.store.selectSnapshot(EventsState.filterValue),
                        pageSize: this.store.selectSnapshot(PaginatorState.pageSize),
                        sortBy: 'Date',
                        sortOrder: 'desc',
                    }),
                );
            }
        }

        return true;
    }
}
