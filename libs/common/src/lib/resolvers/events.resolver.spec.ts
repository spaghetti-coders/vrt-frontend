import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgxsModule, Store } from '@ngxs/store';
import { AccountService, EventsService } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { DEFAULTPARAMETERS, IS_DEMO_BUILD, NotificationSnackBarService } from '@vrt/utils';
import { provideAutoSpy } from 'jest-auto-spies';
import { MockModule } from 'ng-mocks';
import { of } from 'rxjs';

import { RouterModule } from '@angular/router';
import { EventsState, LoadAvailableEvents, LoadFutureEvents } from '@vrt/event-api';
import { USER_EXAMPLE } from '@vrt/testing';
import { EventsResolver } from './events.resolver';

describe('EventsResolver', () => {
    let resolver: EventsResolver;
    let store: Store;
    let authService: AuthenticationService;
    let eventsService: EventsService;
    let accountService: AccountService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([EventsState]), HttpClientTestingModule, RouterModule.forRoot([]), MockModule(MatSnackBarModule)],
            providers: [provideAutoSpy(NotificationSnackBarService), { provide: IS_DEMO_BUILD, useValue: false }],
            teardown: { destroyAfterEach: false },
        });
        resolver = TestBed.inject(EventsResolver);
        store = TestBed.inject(Store);
        authService = TestBed.inject(AuthenticationService);
        eventsService = TestBed.inject(EventsService);
        accountService = TestBed.inject(AccountService);
    });

    it('should be created', () => {
        expect(resolver).toBeTruthy();
    });

    it('should load available events', () => {
        const spy = jest.spyOn(store, 'dispatch');
        store.reset({
            appEvents: {
                isLoaded: false,
                filterValue: 'Example',
            },
        });
        accountService.getCurrentUser = jest.fn().mockReturnValue(of(USER_EXAMPLE));

        authService.requestCurrentUser();
        resolver.resolve();

        expect(spy).toHaveBeenCalledWith(
            new LoadAvailableEvents({
                apiCallParameters: {
                    ...DEFAULTPARAMETERS,
                    filter: 'Example',
                },
            }),
        );
    });

    it('should load all future events', waitForAsync(() => {
        const spy = jest.spyOn(store, 'dispatch');
        store.reset({
            appEvents: {
                isLoaded: false,
            },
        });
        accountService.getCurrentUser = jest.fn().mockReturnValue(of({ ...USER_EXAMPLE, role: 'admin' }));
        authService.isInRole = jest.fn().mockReturnValue(true);
        resolver.resolve();

        expect(authService.isInRole(['admin'])).toBeTruthy();
        expect(spy).toHaveBeenCalledWith(new LoadFutureEvents({ apiCallParameters: DEFAULTPARAMETERS }));
    }));

    it('should return true if resolve was called', waitForAsync(() => {
        store.reset({
            appEvents: {
                isLoaded: true,
            },
        });
        resolver.resolve();
        expect(resolver.resolve).toBeTruthy();
    }));
});
