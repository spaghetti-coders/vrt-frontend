import { Injectable } from '@angular/core';

import { Store } from '@ngxs/store';
import { AuthenticationService } from '@vrt/auth';
import { LoadAvailableVenues, LoadVenues, VenuesState } from '@vrt/venue-api';

@Injectable({
    providedIn: 'root',
})
export class VenuesResolver {
    constructor(
        private store: Store,
        private authenticationService: AuthenticationService,
    ) {}

    resolve(): boolean {
        if (this.authenticationService.isInRole(['admin', 'manager'])) {
            this.store.dispatch(
                new LoadVenues({
                    apiCallParameters: {
                        page: undefined,
                        pageSize: this.store.selectSnapshot(VenuesState.metadata)?.pageSize,
                        filter: undefined,
                        sortBy: undefined,
                        sortOrder: undefined,
                    },
                }),
            );
        } else {
            this.store.dispatch(
                new LoadAvailableVenues({
                    apiCallParameters: {
                        page: undefined,
                        pageSize: this.store.selectSnapshot(VenuesState.metadata)?.pageSize,
                        filter: undefined,
                        sortBy: undefined,
                        sortOrder: undefined,
                    },
                }),
            );
        }

        return true;
    }
}
