import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { FindRegistration } from '@vrt/event-api';

@Injectable({
    providedIn: 'root',
})
export class RegistrationsResolver {
    constructor(private store: Store) {}

    resolve(route: ActivatedRouteSnapshot): boolean {
        const eventId = route.paramMap.get('eventId');

        if (eventId) {
            this.store.dispatch(
                new FindRegistration({
                    registrationsApiParameters: {
                        eventId: +eventId,
                        page: undefined,
                        pageSize: undefined,
                        status: undefined,
                        filter: undefined,
                        sortBy: undefined,
                        sortOrder: undefined,
                    },
                }),
            );
        }
        return true;
    }
}
