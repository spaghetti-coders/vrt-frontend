import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { EventsState, LoadEventById, SetEventFromStore } from '@vrt/event-api';

@Injectable({
    providedIn: 'root',
})
export class EventResolver {
    constructor(private store: Store) {}

    resolve(route: ActivatedRouteSnapshot): boolean {
        const { id } = route.queryParams;
        const eventId = route.paramMap?.get('eventId') ?? id ?? route.paramMap?.get('id');

        this.store.dispatch(new SetEventFromStore({ id: +eventId }));

        if (!this.store.selectSnapshot(EventsState.selectedVrtEventFromStore)) {
            this.store.dispatch(new LoadEventById({ id: +eventId }));
        }
        return true;
    }
}
