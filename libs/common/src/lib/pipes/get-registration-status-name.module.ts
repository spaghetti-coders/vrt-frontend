import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GetRegistrationStatusNamePipe } from './get-registration-status-name.pipe';

@NgModule({
    declarations: [GetRegistrationStatusNamePipe],
    exports: [GetRegistrationStatusNamePipe],
    imports: [CommonModule],
})
export class GetRegistrationStatusNameModule {}
