import { Pipe, PipeTransform } from '@angular/core';
import { RegistrationStatus } from '@vrt/api';

@Pipe({
    name: 'getRegistrationStatusName',
})
export class GetRegistrationStatusNamePipe implements PipeTransform {
    transform(value: RegistrationStatus | undefined): string | undefined {
        switch (value) {
            case RegistrationStatus.NUMBER_0:
                return 'NotRegistered';
            case RegistrationStatus.NUMBER_1:
                return 'Registered';
            case RegistrationStatus.NUMBER_2:
                return 'OnWaitingList';
        }
        return undefined;
    }
}
