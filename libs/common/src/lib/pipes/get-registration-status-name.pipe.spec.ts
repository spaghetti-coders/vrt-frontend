import { RegistrationStatus } from '@vrt/api';
import { GetRegistrationStatusNamePipe } from './get-registration-status-name.pipe';

describe('GetRegistrationStatusNamePipe', () => {
    let pipe: GetRegistrationStatusNamePipe;
    beforeEach(() => {
        pipe = new GetRegistrationStatusNamePipe();
    });
    it('create an instance', () => {
        expect(pipe.transform(undefined)).toEqual(undefined);
        expect(pipe).toBeTruthy();
    });

    it('should return not registered', () => {
        expect(pipe.transform(RegistrationStatus.NUMBER_0)).toEqual('NotRegistered');
    });

    it('should return registered', () => {
        expect(pipe.transform(RegistrationStatus.NUMBER_1)).toEqual('Registered');
    });

    it('should return not registered', () => {
        expect(pipe.transform(RegistrationStatus.NUMBER_2)).toEqual('OnWaitingList');
    });
});
