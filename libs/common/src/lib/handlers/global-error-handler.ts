import { HttpErrorResponse } from '@angular/common/http';
import { EventEmitter, Injectable, Injector } from '@angular/core';
import { SentryErrorHandler } from '@sentry/angular-ivy';
import { NotificationSnackBarService } from '@vrt/utils';
import { ErrorService, LoggingService } from '../services';

@Injectable()
export class GlobalErrorHandler extends SentryErrorHandler {
    onError = new EventEmitter();

    constructor(
        private injector: Injector,
        private notificationService: NotificationSnackBarService,
    ) {
        super();
    }

    setShowDialog(show: boolean) {
        this._options.showDialog = show;
    }

    override handleError(error: Error | HttpErrorResponse): void {
        const errorService = this.injector.get(ErrorService);
        const logger = this.injector.get(LoggingService);
        let message;
        let stackTrace;

        if (error instanceof HttpErrorResponse) {
            // Server Error
            message = errorService.getServerMessage(error);
            stackTrace = errorService.getServerStack(error);
            this.notificationService.showError(message);
        } else {
            // Client Error
            message = errorService.getClientMessage(error);
            this.notificationService.showError(message);
            stackTrace = errorService.getClientStack(error);
        }
        // Always log errors
        logger.logError(message, stackTrace);

        this.onError.emit(error);
        super.handleError(error);
    }
}
