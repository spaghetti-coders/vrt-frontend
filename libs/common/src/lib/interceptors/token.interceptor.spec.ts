import { HTTP_INTERCEPTORS, HttpClient, HttpInterceptor } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';
import { LoginResult } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { IS_DEMO_BUILD, JWT_EXAMPLE, NotificationSnackBarService } from '@vrt/utils';
import { MockProviders } from 'ng-mocks';
import { of } from 'rxjs';
import { TokenInterceptor } from './token.interceptor';

describe('TokenInterceptor', () => {
    let interceptor: TokenInterceptor;
    let httpMock: HttpTestingController;
    let httpClient: HttpClient;
    let router: Router;
    let authenticationService: AuthenticationService;
    const consoleError = console.error;

    beforeAll(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterTestingModule, NgxsModule.forRoot([])],
            providers: [
                { provide: IS_DEMO_BUILD, useValue: false },
                { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
                MockProviders(NotificationSnackBarService),
            ],
            teardown: { destroyAfterEach: false },
        });

        interceptor = (<HttpInterceptor[]>TestBed.inject(HTTP_INTERCEPTORS)).find(
            (service) => service instanceof TokenInterceptor,
        ) as TokenInterceptor;
        httpMock = TestBed.inject(HttpTestingController);
        authenticationService = TestBed.inject(AuthenticationService);
        httpClient = TestBed.inject(HttpClient);
        router = TestBed.inject(Router);
        router.navigate = jest.fn();
    });

    beforeEach(() => {
        jest.clearAllMocks();
        interceptor.isRefreshing = false;
    });

    it('should be created', () => {
        expect(interceptor).toBeTruthy();
    });

    it('should dispatch SetLoading for a single request when it fails', waitForAsync(() => {
        localStorage.setItem('auth_token', JWT_EXAMPLE);
        const spy = jest.spyOn(authenticationService, 'refreshToken');
        httpClient.get<any>('/api/is/this/a/test').subscribe();

        const mockErrorResponse = { status: 401, statusText: 'Bad Request' };
        httpMock.expectOne('/api/is/this/a/test').flush({}, mockErrorResponse);
        httpMock.expectOne('http://localhost/api/Account/refresh');
        httpMock.verify();

        expect(spy).toHaveBeenCalled();
    }));

    it('should trigger the logout workflow on token workflow on a 401 status', waitForAsync(() => {
        localStorage.setItem('auth_token', JWT_EXAMPLE);
        authenticationService.logout = jest.fn();
        httpClient.get<any>('/api/is/this/a/refresh').subscribe(() => {
            expect(authenticationService.logout).toHaveBeenCalled();
        });

        const mockErrorResponse = { status: 401, statusText: 'UNAUTHORIZED' };
        const apiRequest = httpMock.expectOne('/api/is/this/a/refresh');
        apiRequest.flush({}, mockErrorResponse);

        const refreshRequest = httpMock.expectOne('http://localhost/api/Account/refresh');
        httpMock.verify();
    }));

    it('should trigger the refresh token workflow on a 401 status', waitForAsync(() => {
        localStorage.setItem('refresh_token', 'REFRESH_TOKEN');
        localStorage.setItem('auth_token', JWT_EXAMPLE);
        authenticationService.refreshToken = jest.fn().mockReturnValue(
            of({
                accessToken: JWT_EXAMPLE,
                refreshToken: 'REFRESH_TOKEN',
            } as LoginResult),
        );
        authenticationService.storeTokens = jest.fn();
        httpClient.get<any>('/api/is/this/a/test123').subscribe((res: string) => {
            expect(res).toBe('all good');
            expect(authenticationService.refreshToken).toHaveBeenCalled();
            expect(authenticationService.storeTokens).toHaveBeenCalled();
        });

        const mockErrorResponse = {
            status: 401,
            statusText: 'UNAUTHORIZED',
            url: 'refresh',
        };
        const errorRequest = httpMock.expectOne('/api/is/this/a/test123');
        errorRequest.flush({}, mockErrorResponse);

        const request = httpMock.expectOne('/api/is/this/a/test123');
        request.flush('all good');

        httpMock.verify();
    }));
});
