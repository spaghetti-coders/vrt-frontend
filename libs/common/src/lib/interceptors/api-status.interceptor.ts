import { HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { SetLoadingIndicator } from '../+state';

@Injectable()
export class ApiStatusInterceptor implements HttpInterceptor {
    private pendingRequests = 0;

    constructor(private store: Store) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.url.includes('/api/') || req.headers.get('Accept') === 'image/*') {
            return next.handle(req.clone());
        } else {
            if (this.pendingRequests === 0) {
                this.store.dispatch(new SetLoadingIndicator({ isLoading: true }));
            }
            return next.handle(req.clone()).pipe(
                tap((event: any) => {
                    if (event.type === HttpEventType.Sent) {
                        this.pendingRequests += 1;
                    } else if (event.type === HttpEventType.Response) {
                        this.pendingRequests -= 1;
                    }
                }),
                catchError((err) => {
                    this.pendingRequests -= 1;
                    return throwError(err);
                }),
                finalize(() => {
                    if (this.pendingRequests === 0) {
                        this.store.dispatch(new SetLoadingIndicator({ isLoading: false }));
                    }
                }),
            );
        }
    }
}
