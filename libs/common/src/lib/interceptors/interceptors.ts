import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiStatusInterceptor } from './api-status.interceptor';
import { TokenInterceptor } from './token.interceptor';

export const httpInterceptors = [
    {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptor,
        multi: true,
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: ApiStatusInterceptor,
        multi: true,
    },
    // ... weitere Interceptoren
];
