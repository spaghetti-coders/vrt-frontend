import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginResult } from '@vrt/api';
import { AuthenticationService } from '@vrt/auth';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, filter, switchMap, take } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    isRefreshing = false;

    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    private static addToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
        return request.clone({
            headers: request.headers.set('Authorization', `Bearer ${token}`),
        });
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let clonedRequest = request;
        const token = this.authenticationService.getJwtToken();
        if (token) {
            clonedRequest = TokenInterceptor.addToken(request, token);
        }
        const reqUrlParts = request.url.split('/');
        const isReqUrlLoginUrl = reqUrlParts.some((url: string) => url === 'login');
        const isReqUrlRefreshUrl = reqUrlParts.some((url: string) => url === 'refresh');

        return next.handle(clonedRequest).pipe(
            catchError((error: HttpResponse<any>) => {
                if (error.status === 401 && !isReqUrlLoginUrl) {
                    if (isReqUrlRefreshUrl) {
                        this.authenticationService.processLogout();
                    }
                    return this.handle401Error(request, next);
                } else {
                    return throwError(error);
                }
            }),
        );
    }

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshTokenSubject.next(null);

            return this.authenticationService.refreshToken().pipe(
                catchError((err) => {
                    this.authenticationService.processLogout();
                    return throwError(err);
                }),
                switchMap((loginResult: LoginResult) => {
                    this.isRefreshing = false;
                    this.authenticationService.storeTokens(loginResult);
                    this.refreshTokenSubject.next(loginResult.accessToken);
                    return next.handle(TokenInterceptor.addToken(request, loginResult.accessToken ?? ''));
                }),
            );
        } else {
            return this.refreshTokenSubject.pipe(
                filter((accessToken) => accessToken != null),
                take(1),
                switchMap((accessToken) => {
                    return next.handle(TokenInterceptor.addToken(request, accessToken));
                }),
            );
        }
    }
}
