export class SetLoadingIndicator {
    static readonly type = '[Feature] set loading indicator';

    constructor(public payload: { isLoading: boolean }) {}
}

export class SetToggleState {
    static readonly type = '[Feature] set toggle state';

    constructor(public payload: { toggleSwitch: boolean }) {}
}
