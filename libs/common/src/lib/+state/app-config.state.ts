import { Injectable } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { SwitchLang, SwitchTheme } from './app-config.actions';

export interface AppConfigStateModel {
    isDarkThemeSelected: boolean;
    language: string;
}

const defaults = {
    isDarkThemeSelected: false,
    language: 'en',
};

@State<AppConfigStateModel>({
    name: 'appConfig',
    defaults,
})
@Injectable()
export class AppConfigState {
    constructor(private translocoService: TranslocoService) {}

    @Selector([AppConfigState])
    static isDarkThemeSelected({ isDarkThemeSelected }: AppConfigStateModel): boolean {
        return isDarkThemeSelected;
    }

    @Selector([AppConfigState])
    static language({ language }: AppConfigStateModel): string {
        return language;
    }

    @Action(SwitchTheme)
    switchTheme({ getState, patchState }: StateContext<AppConfigStateModel>, { payload: { isDarkThemeSelected } }: SwitchTheme) {
        patchState({ isDarkThemeSelected });
    }

    @Action(SwitchLang)
    switchLang({ getState, patchState }: StateContext<AppConfigStateModel>, { payload: { language } }: SwitchLang) {
        patchState({ language });
        this.translocoService.setActiveLang(language);
        localStorage.setItem('lang', language);
    }
}
