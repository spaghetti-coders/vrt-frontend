import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { SetLoadingIndicator, SetToggleState } from './feature.actions';

export interface FeatureStateModel {
    isLoading: boolean;
    toggleSwitch: boolean;
}

const defaults = {
    isLoading: false,
    toggleSwitch: true,
};

@State<FeatureStateModel>({
    name: 'feature',
    defaults,
})
@Injectable()
export class FeatureState {
    @Selector([FeatureState])
    static isLoading({ isLoading }: FeatureStateModel): boolean {
        return isLoading;
    }

    @Selector([FeatureState])
    static toggleSwitch({ toggleSwitch }: FeatureStateModel): boolean {
        return JSON.parse(sessionStorage.getItem('toggleSwitch') ?? 'null') ?? toggleSwitch;
    }

    @Action(SetLoadingIndicator)
    loadingIndicator({ patchState }: StateContext<FeatureStateModel>, { payload }: SetLoadingIndicator) {
        const { isLoading } = payload;
        patchState({ isLoading });
    }

    @Action(SetToggleState)
    setToggleState({ patchState }: StateContext<FeatureStateModel>, { payload }: SetToggleState) {
        const { toggleSwitch } = payload;
        sessionStorage.setItem('toggleSwitch', toggleSwitch + '');
        patchState({ toggleSwitch });
    }
}
