export class SwitchTheme {
    static readonly type = '[AppConfig] Switch theme';

    constructor(public payload: { isDarkThemeSelected: boolean }) {}
}

export class SwitchLang {
    static readonly type = '[AppConfig] Switch language';

    constructor(public payload: { language: string }) {}
}
