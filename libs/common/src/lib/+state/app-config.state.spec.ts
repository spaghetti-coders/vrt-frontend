import { TestBed, waitForAsync } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { getTranslocoModule } from '../testing/util-jest/transloco-testing-module';
import { SwitchTheme } from './app-config.actions';
import { AppConfigState } from './app-config.state';

describe('AppConfig actions', () => {
    let store: Store;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([AppConfigState]), getTranslocoModule()],
            teardown: { destroyAfterEach: false },
        });
        store = TestBed.inject(Store);
    }));

    it('should create an action and add an item', () => {
        store.dispatch(new SwitchTheme({ isDarkThemeSelected: true }));
        expect(store.selectSnapshot(AppConfigState.isDarkThemeSelected)).toBeTruthy();
    });
});
