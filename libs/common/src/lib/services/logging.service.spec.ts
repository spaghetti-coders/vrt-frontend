import { LoggingService } from './logging.service';

describe('Logging Service', () => {
    it('should log an error', () => {
        const service = new LoggingService();
        // eslint-disable-next-line no-console
        console.log = jest.fn();

        service.logError('Example', 'Info');

        // eslint-disable-next-line no-console
        expect(console.log).toHaveBeenCalledWith('LoggingService: Example');
        // eslint-disable-next-line no-console
        expect(console.log).toHaveBeenCalledTimes(1);
    });
});
