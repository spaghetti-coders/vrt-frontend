import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { ErrorService } from './error.service';

describe('Error Service', () => {
    let service: ErrorService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [],
            teardown: { destroyAfterEach: false },
        });
        service = TestBed.inject(ErrorService);
    });

    it('should return no internet connection', () => {
        const clientError = new Error('ERROR');
        // todo remember for readonly props
        jest.spyOn(navigator, 'onLine', 'get').mockReturnValueOnce(false);
        expect(service.getClientMessage(clientError)).toBe('No Internet Connection');
    });

    it('should return incoming error message', () => {
        const clientError = new Error('ERROR');
        // todo remember for readonly props
        jest.spyOn(navigator, 'onLine', 'get').mockReturnValueOnce(true);
        expect(service.getClientMessage(clientError)).toBe('ERROR');
    });

    it('should return incoming ClientStack error', () => {
        const clientError = new Error('ERROR');
        clientError.stack = 'API';
        expect(service.getClientStack(clientError)).toBe('API');
    });

    it('should return incoming ServerMessage', () => {
        const serverError = new HttpErrorResponse({
            error: 'ERROR',
            status: 500,
            statusText: 'Fatal Error',
        });
        expect(service.getServerMessage(serverError)).toBe('Http failure response for (unknown url): 500 Fatal Error');
    });

    it('should return incoming ServerStack', () => {
        const serverError = new HttpErrorResponse({
            error: 'ERROR',
            status: 500,
            statusText: 'Fatal Error',
        });
        expect(service.getServerStack(serverError)).toBe('stack');
    });
});
