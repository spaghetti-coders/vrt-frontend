import { TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { NotificationSnackBarService } from './notification-snack-bar.service';

describe('Notification Service', () => {
    let service: NotificationSnackBarService;
    let matSnackBar: MatSnackBar;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [MatSnackBarModule],
            providers: [],
            teardown: { destroyAfterEach: false },
        });
        service = TestBed.inject(NotificationSnackBarService);
        matSnackBar = TestBed.inject(MatSnackBar);
    });

    it('should call success methode', () => {
        matSnackBar.open = jest.fn();
        service.config.panelClass = ['notification-success'];
        service.showSuccess('DONE');
        expect(matSnackBar.open).toHaveBeenCalledWith('DONE', 'X', service.config);
    });

    it('should call showError methode', () => {
        matSnackBar.open = jest.fn();
        service.config.panelClass = ['notification-error'];
        service.showError('ERROR');
        expect(matSnackBar.open).toHaveBeenCalledWith('Fehler: ERROR', 'X', service.config);
    });

    it('should call showWarning methode', () => {
        matSnackBar.open = jest.fn();
        service.config.panelClass = ['notification-warning'];
        service.showWarning('WARN');
        expect(matSnackBar.open).toHaveBeenCalledWith('WARN', 'X', service.config);
    });

    it('should call showMessage methode', () => {
        matSnackBar.open = jest.fn();
        service.config.panelClass = ['notification-message'];
        service.showMessage('SHOW');
        expect(matSnackBar.open).toHaveBeenCalledWith('SHOW', 'X', service.config);
    });
});
