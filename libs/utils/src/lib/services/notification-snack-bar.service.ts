import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root',
})
export class NotificationSnackBarService {
    config: MatSnackBarConfig = {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: 'bottom',
    };

    constructor(
        private matSnackBar: MatSnackBar,
        private zone: NgZone,
    ) {}

    showSuccess(message: string): void {
        this.config.panelClass = ['notification-success'];
        this.showSnackbar(message, 'X');
    }

    showError(message: string): void {
        this.config.panelClass = ['notification-error'];
        this.showSnackbar(`Fehler: ${message}`, 'X');
    }

    showWarning(message: string): void {
        this.config.panelClass = ['notification-warning'];
        this.showSnackbar(message, 'X');
    }

    showMessage(message: string): void {
        this.config.panelClass = ['notification-message'];
        this.showSnackbar(message, 'X');
    }

    showSnackbar(message: string, action?: string) {
        this.zone.run(() => {
            this.matSnackBar.open(message, action, this.config);
        });
    }
}
