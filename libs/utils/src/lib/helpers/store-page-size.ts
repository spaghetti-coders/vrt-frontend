export const savePageSize = (pageSize: number) => {
    localStorage.setItem('pageSize', `${pageSize}`);
};
