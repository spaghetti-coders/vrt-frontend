import { UrlParser } from './urlParser';

describe('UrlParser', () => {
    it('should parse return url', () => {
        // Arrange
        const url = '/';

        // Act
        UrlParser.getParams(url);

        // Assert
        expect(UrlParser.returnUrl).toBe('/events');
    });

    it('should return fallback url', () => {
        // Arrange
        const url = undefined;

        // Act
        UrlParser.getParams(url);

        // Assert
        expect(UrlParser.returnUrl).toBe('/events');
    });

    it('should return fallback url', () => {
        // Arrange
        const url = 'localhost:4200/events/?id=1';

        // Act
        UrlParser.getParams(url);

        // Assert
        expect(UrlParser.returnUrl).toBe('localhost:4200/events/');
    });
});
