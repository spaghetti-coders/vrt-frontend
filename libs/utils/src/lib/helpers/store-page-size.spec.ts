import { savePageSize } from './store-page-size';

describe('Store page size', () => {
    it('should save pageSize to localstorage', () => {
        savePageSize(25);
        expect(Number(localStorage.getItem('pageSize'))).toEqual(25);
    });
});
