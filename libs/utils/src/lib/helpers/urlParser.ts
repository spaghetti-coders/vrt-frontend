export class UrlParser {
    static returnUrl: string;

    static queryParams = {};

    static getParams(url: string | undefined): void {
        if (url === '/' || url === undefined) {
            url = '/events';
        }
        if (url.indexOf('?') > -1) {
            const paramString = url.substring(url.indexOf('?') + 1, url.length);
            const paramArray = paramString.split('&');
            paramArray.map((item: string) => {
                const keyValArr = item.split('=');
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                this.queryParams[keyValArr[0]] = keyValArr[1];
            });
            this.returnUrl = url.substring(0, url.indexOf('?'));
        } else {
            this.returnUrl = url;
        }
    }
}
