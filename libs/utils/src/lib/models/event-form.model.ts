import { FormControl } from '@angular/forms';

export interface EventForm {
    id: FormControl<number | undefined>;
    name: FormControl<string>;
    date: FormControl<string>;
    time: FormControl<string>;
    isOpenForRegistration: FormControl<boolean>;
    description: FormControl<string>;
    capacity: FormControl<number>;
    venueId: FormControl<number>;
}
