export enum UserStatusLocal {
    PendingRegistration,
    Active,
    Inactive,
    PendingDeletion,
    Any,
}

export function stringIsNumber(value: string): boolean {
    return !isNaN(Number(value));
}

export function getEnumValues(enumme: any): any[] {
    return Object.keys(enumme)
        .filter((value) => !stringIsNumber(value))
        .map((key) => enumme[key]);
}

export class UserStatusAdapter {
    label = 'N/A';

    value = -1;

    static allElements(): UserStatusAdapter[] {
        return getEnumValues(UserStatusLocal).map((value) => UserStatusAdapter.fromEnum(value));
    }

    static fromEnum(userStatus: UserStatusLocal): UserStatusAdapter {
        const result = new UserStatusAdapter();
        switch (userStatus) {
            case UserStatusLocal.Active:
                result.label = 'Active';
                break;
            case UserStatusLocal.Inactive:
                result.label = 'Inactive';
                break;
            case UserStatusLocal.PendingDeletion:
                result.label = 'PendingDeletion';
                break;
            case UserStatusLocal.PendingRegistration:
                result.label = 'PendingRegistration';
                break;
            case UserStatusLocal.Any:
                result.label = 'Any';
                break;
        }
        result.value = userStatus;
        return result;
    }
}
