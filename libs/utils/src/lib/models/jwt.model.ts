export interface Jwt {
    iss: string;
    exp: number;
    aud: string;
    role: string;
    name: string;
    nameidentifier: string;
    sid: number;
}

export const JWT_EXAMPLE_RAW: Jwt = {
    aud: 'https://mysite.com/',
    name: 'peter',
    nameidentifier: 'peter',
    role: 'admin',
    sid: 2,
    iss: 'https://mysite.com/',
    exp: 9999999999,
};

export const JWT_EXAMPLE = `.${btoa(JSON.stringify(JWT_EXAMPLE_RAW))}.`;
