import { InjectionToken } from '@angular/core';

export const IS_DEMO_BUILD = new InjectionToken<boolean>('IS_PROD_BUILD');
