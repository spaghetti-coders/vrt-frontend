import { HttpClient } from '@angular/common/http';
import { Injectable, NgModule } from '@angular/core';
import { TRANSLOCO_LOADER, Translation, TranslocoLoader, TranslocoModule, provideTransloco } from '@ngneat/transloco';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class TranslocoHttpLoader implements TranslocoLoader {
    constructor(private http: HttpClient) {}

    getTranslation(lang: string): Observable<Translation> {
        return this.http.get<Translation>(`/assets/i18n/${lang}.json`);
    }
}

@NgModule({
    exports: [TranslocoModule],
    providers: [
        provideTransloco({
            config: {
                availableLangs: ['de', 'en'],
                defaultLang: localStorage.getItem('lang') ?? 'de',
                // Remove this option if your application doesn't support changing language in runtime.
                reRenderOnLangChange: true,
                prodMode: true,
            },
        }),
        { provide: TRANSLOCO_LOADER, useClass: TranslocoHttpLoader },
    ],
})
export class I18nModule {}
