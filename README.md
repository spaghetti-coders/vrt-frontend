<h1 align="center">Welcome to Venue Registration Tool - Frontend 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.3.1-blue.svg?cacheSeconds=2592000" />
  <a href="https://gitlab.com/spaghetti-coders/vrt-frontend/-/blob/main/LICENSE" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
  <a href="https://twitter.com/mnkyjs" target="_blank">
    <img alt="Twitter: mnkyjs" src="https://img.shields.io/twitter/follow/mnkyjs.svg?style=social" />
  </a>
</p>

> A tool to better manage event administration for e.g. a clubs registration and protocols

### 🏠 [Homepage](https://vrt.k-projects.eu/auth/login?returnUrl=%2F)

### ✨ [Demo](https://event-planer.uk.to/auth/login?returnUrl=%2F)

## Install

```sh
npm install
```

## Usage

```sh
npm run start
```

## Run tests

```sh
npm run test
```

## Author

👤 **Dennis Hundertmark & Karl-Heinz Holzigel**

-   Website: https://mnky-js.com/
-   Twitter: [@mnkyjs](https://twitter.com/mnkyjs)
-   Github: [@mnkyjs](https://github.com/mnkyjs)
-   LinkedIn: [@DennisHundertmark](https://linkedin.com/in/DennisHundertmark)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/spaghetti-coders/vrt-frontend/-/issues).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2022 [Dennis Hundertmark](https://github.com/mnkyjs).

This project is [MIT](https://gitlab.com/spaghetti-coders/vrt-frontend/-/blob/main/LICENSE) licensed.

---

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
