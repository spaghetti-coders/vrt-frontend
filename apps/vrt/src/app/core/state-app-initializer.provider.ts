import { OverlayContainer } from '@angular/cdk/overlay';
import { APP_INITIALIZER } from '@angular/core';
import { Store } from '@ngxs/store';
import { FeatureState, SwitchTheme } from '@vrt/common';

function initializeState(store: Store, feature: FeatureState, container: OverlayContainer) {
    return (): void => {
        const darkThemeMode = 'dark-theme-mode';
        const selectedTheme = localStorage.getItem('theme');
        let isDarkThemeSelected = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;

        if (selectedTheme === null) {
            const userPrefersDark = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
            localStorage.setItem('theme', userPrefersDark ? 'Dark' : 'Light');

            isDarkThemeSelected = userPrefersDark;
        } else {
            isDarkThemeSelected = selectedTheme === 'Dark';
        }

        if (isDarkThemeSelected) {
            store.dispatch(new SwitchTheme({ isDarkThemeSelected }));
            container.getContainerElement().classList.add(darkThemeMode);
        } else {
            container.getContainerElement().classList.remove(darkThemeMode);
        }
    };
}

/**
 * **🚀 Perf Tip for LCP, TTI:**
 *
 * Use `APP_INITIALIZER` and an init method in data services to run data fetching
 * on app bootstrap instead of component initialization.
 */
export const GLOBAL_STATE_APP_INITIALIZER_PROVIDER = [
    {
        provide: APP_INITIALIZER,
        useFactory: initializeState,
        deps: [Store, FeatureState, OverlayContainer],
        multi: true,
    },
];
