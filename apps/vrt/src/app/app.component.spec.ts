import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { NgxsModule, Store } from '@ngxs/store';
import { AppConfigState, SwitchLang, getTranslocoModule } from '@vrt/common';
import { LoadingIndicatorModule } from '@vrt/ui';
import { MockModule } from 'ng-mocks';
import { of } from 'rxjs';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    let store: Store;
    const logError = console.error;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                MockModule(LoadingIndicatorModule),
                NgxsModule.forRoot([AppConfigState]),
                RouterModule.forRoot([
                    {
                        path: 'check',
                        component: AppComponent,
                    },
                ]),
                getTranslocoModule(),
            ],
            declarations: [AppComponent],
            providers: [
                {
                    provide: SwUpdate,
                    useValue: {
                        isEnabled: true,
                        versionUpdates: of({
                            type: 'VERSION_READY',
                            currentVersion: {
                                hash: 'current_hash',
                                appData: { version: 1.1 },
                            },
                            latestVersion: {
                                hash: 'available_hash',
                                appData: { version: 1.2 },
                            },
                        } as VersionReadyEvent),
                    },
                },
            ],
            teardown: { destroyAfterEach: false },
        });
    });

    beforeEach(() => {
        console.error = jest.fn();
        store = TestBed.inject(Store);
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the app', () => {
        expect(component).toBeTruthy();
    });

    it('should match snapshot', () => {
        expect(fixture).toMatchSnapshot();
    });

    it('should init language to de', waitForAsync(() => {
        const spy = jest.spyOn(store, 'dispatch');
        window.localStorage.setItem('lang', 'de');

        component.initLang();

        expect(spy).toHaveBeenCalledWith(new SwitchLang({ language: 'de' }));
    }));

    it('should trigger the service worker process', waitForAsync(() => {
        component.swUpdate.versionUpdates.subscribe((update: any) => {
            expect(update.currentVersion.appData).toBeTruthy();
            expect(update.currentVersion.appData).toStrictEqual({ version: 1.1 });
        });
    }));
});
