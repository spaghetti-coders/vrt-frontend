import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { Store } from '@ngxs/store';
import { SwitchLang } from '@vrt/common';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { fallbackRouteToDefault } from './routing-default.utils';

@Component({
    selector: 'vrt-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit, OnDestroy {
    subscriptions: Subscription = new Subscription();

    constructor(
        private store: Store,
        private router: Router,
        public swUpdate: SwUpdate,
    ) {
        setTimeout(() => this.router.navigate([fallbackRouteToDefault(document.location.pathname)]));
    }

    ngOnInit(): void {
        this.initLang();
        if (this.swUpdate.isEnabled) {
            this.subscriptions.add(
                this.swUpdate.versionUpdates
                    .pipe(
                        filter((evt: any): evt is VersionReadyEvent => evt.type === 'VERSION_READY'),
                        map((versionEvent: any) => {
                            const currentVersion = versionEvent.currentVersion.appData?.version;
                            const newVersion = versionEvent.latestVersion.appData?.version;
                            const changelog = versionEvent.latestVersion.appData?.changelog;
                            const confirmationText = `Ein Update ist verfügbar von
                        ${currentVersion} zu ${newVersion}.
                        Änderungen: ${changelog}
                        Update installieren?`;
                            if (window.confirm(confirmationText)) {
                                window.location.reload();
                            }
                            return versionEvent;
                        }),
                    )
                    .subscribe(),
            );
        }
    }

    initLang() {
        const language = localStorage.getItem('lang') ?? 'de';
        this.store.dispatch(new SwitchLang({ language }));
    }

    ngOnDestroy(): void {
        this.subscriptions.unsubscribe();
    }
}
