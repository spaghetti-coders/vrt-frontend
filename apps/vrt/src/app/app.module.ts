import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeEn from '@angular/common/locales/en';
import { APP_INITIALIZER, ErrorHandler, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { JwtModule } from '@auth0/angular-jwt';
import { TranslocoService } from '@ngneat/transloco';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsModule } from '@ngxs/store';
import { ApiModule, Configuration, ConfigurationParameters, PaginatorState } from '@vrt/api';
import { AppConfigState, FeatureState, GlobalErrorHandler, httpInterceptors } from '@vrt/common';
import { I18nModule } from '@vrt/i18n';
import { IS_DEMO_BUILD } from '@vrt/utils';
import { environment } from '../environments/environment';

import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { Router } from '@angular/router';
import { TraceService } from '@sentry/angular-ivy';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GLOBAL_STATE_APP_INITIALIZER_PROVIDER } from './core/state-app-initializer.provider';

registerLocaleData(localeDe);
registerLocaleData(localeEn);

export function tokenGetter(): string | null {
    return localStorage.getItem(`auth_token`);
}

export function apiConfigFactory(): Configuration {
    const params: ConfigurationParameters = {
        basePath: environment.apiUrl,
    };
    return new Configuration(params);
}

@NgModule({
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        I18nModule,
        ApiModule.forRoot(apiConfigFactory),
        AppRoutingModule,
        JwtModule.forRoot({
            config: {
                tokenGetter,
                allowedDomains: [environment.apiUrl],
                disallowedRoutes: [],
            },
        }),
        NgxsModule.forRoot([FeatureState, AppConfigState, PaginatorState], {
            developmentMode: !environment.production,
            selectorOptions: {
                suppressErrors: false,
                injectContainerState: false,
            },
        }),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        NgxsStoragePluginModule.forRoot({
            key: ['appConfig'],
        }),
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
            // Register the ServiceWorker as soon as the application is stable
            // or after 30 seconds (whichever comes first).
            registrationStrategy: 'registerWhenStable:30000',
        }),
    ],
    providers: [
        ...httpInterceptors,
        GLOBAL_STATE_APP_INITIALIZER_PROVIDER,
        {
            provide: ErrorHandler,
            useClass: GlobalErrorHandler,
        },
        {
            provide: TraceService,
            deps: [Router],
        },
        {
            provide: APP_INITIALIZER,
            useFactory: () => () => {},
            deps: [TraceService],
            multi: true,
        },
        {
            provide: LOCALE_ID,
            useFactory: (translate: TranslocoService) => {
                return translate.getActiveLang();
            },
            deps: [TranslocoService],
        },
        // {
        //     provide: ErrorHandler,
        //     useValue: createErrorHandler({
        //         showDialog: false,
        //         logErrors: true,
        //     }),
        // },
        { provide: IS_DEMO_BUILD, useValue: environment.isDemo },
        provideHttpClient(withInterceptorsFromDi()),
    ],
})
export class AppModule {}
