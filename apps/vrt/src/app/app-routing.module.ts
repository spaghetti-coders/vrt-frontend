import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard, EventsResolver, LoggedInGuard, RoleGuard, VenuesResolver } from '@vrt/common';

const ROUTES: Routes = [
    {
        path: 'auth',
        loadChildren: () => import('@vrt/ui').then((mod) => mod.AuthModule),
    },
    {
        path: '',
        canActivate: [LoggedInGuard],
        children: [
            {
                path: 'admin',
                canActivate: [LoggedInGuard, AdminGuard],
                loadChildren: () => import('@vrt/ui').then((mod) => mod.AdminAreaModule),
            },
            {
                path: 'events',
                canActivate: [LoggedInGuard],
                loadChildren: () => import('@vrt/ui').then((mod) => mod.EventModule),
                resolve: {
                    events: EventsResolver,
                    venues: VenuesResolver,
                },
            },
            {
                path: 'venues',
                canActivate: [LoggedInGuard, RoleGuard],
                loadChildren: () => import('@vrt/ui').then((mod) => mod.VenueModule),
                resolve: {
                    venues: VenuesResolver,
                },
            },
            {
                path: 'user',
                canActivate: [LoggedInGuard],
                loadChildren: () => import('@vrt/ui').then((mod) => mod.UserAreaModule),
            },
        ],
    },
    {
        path: 'impressum',
        loadChildren: () => import('@vrt/ui').then((mod) => mod.ImpressumModule),
    },
    {
        path: 'privacy',
        loadChildren: () => import('@vrt/ui').then((mod) => mod.PrivacyModule),
    },
    {
        path: 'about',
        loadChildren: () => import('@vrt/ui').then((mod) => mod.AboutModule),
    },
    {
        path: 'check',
        loadChildren: () => import('@vrt/ui').then((mod) => mod.LoadingPageModule),
    },
    { path: '**', redirectTo: 'check', pathMatch: 'full' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(ROUTES, {
            scrollPositionRestoration: 'enabled',
            enableTracing: false,
            initialNavigation: 'disabled',
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
