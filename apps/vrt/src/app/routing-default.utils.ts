export const defaultRedirectRoute = 'check';
export const fallbackRouteToDefault = (route: string) => (route !== '/' ? route : defaultRedirectRoute);
