export const environment = {
    production: true,
    isCypressBuild: false,
    apiUrl: '',
    defaultLang: 'de',
    isDemo: false,
};
