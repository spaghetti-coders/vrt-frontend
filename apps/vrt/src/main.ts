import { enableProdMode, isDevMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { browserTracingIntegration, init, replayIntegration } from '@sentry/angular-ivy';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
    enableProdMode();
}

init({
    dsn: 'https://ef182995dfe44e738a8a1aea9500ff7d@glitch-tip.mnky-js.com/2',
    integrations: [
        // Registers and configures the Tracing integration,
        // which automatically instruments your application to monitor its
        // performance, including custom Angular routing instrumentation
        browserTracingIntegration(),
        // Registers the Replay integration,
        // which automatically captures Session Replays
        replayIntegration(),
    ],

    environment: isDevMode() ? 'development' : 'production',

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,

    // Set `tracePropagationTargets` to control for which URLs distributed tracing should be enabled
    tracePropagationTargets: ['localhost', /^https:\/\/vrt.k-projects\.eu/],

    // Capture Replay for 10% of all sessions,
    // plus for 100% of sessions with an error
    replaysSessionSampleRate: 0.1,
    replaysOnErrorSampleRate: 1.0,
});

setTimeout(() =>
    platformBrowserDynamic()
        .bootstrapModule(AppModule)
        .catch((err) => console.error(err)),
);
