// playwright-dev-page.ts
import { expect, Locator, Page } from '@playwright/test';

export class LoginPage {
    readonly page: Page;

    readonly usernameInput: Locator;

    readonly passwordInput: Locator;

    readonly loginButton: Locator;

    readonly signInButton: Locator;

    token = '';

    constructor(page: Page) {
        this.page = page;
        this.usernameInput = page.getByTestId('username');
        this.passwordInput = page.getByTestId('password');
        this.loginButton = page.getByRole('button', { name: 'Anmelden' });
        this.signInButton = page.getByRole('button', { name: ' Registrieren ' });
    }

    async login(username: string, password: string) {
        await this.page.goto('/auth/login');

        await this.usernameInput.fill(username);

        await this.passwordInput.fill(password);

        await this.signInButton.isVisible();

        await this.loginButton.first().click();

        await this.page.route('**/api/Account/login', async (route) => {
            const response = await route.fetch();
            const body = await response.json();
            this.token = body.accessToken;
            expect(response.status()).toEqual(200);
        });
    }
}
