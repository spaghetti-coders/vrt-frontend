import { expect, test } from '@playwright/test';
import { LoginPage } from './login-page';

import { config } from 'dotenv';

config();

test.describe.configure({
    mode: 'parallel',
});

test.beforeEach(async ({ page }) => {
    const loginPage = new LoginPage(page);
    await loginPage.login(process.env['USERNAME'] as string, process.env['PASSWORD'] as string);
});

test('Sign in/out for an event', async ({ page }) => {
    await expect(page.getByRole('button', { name: 'Veranstaltungen' })).toBeVisible();

    const eventCard = page.getByTestId('vrt-event-card-0');

    await eventCard.getByTestId('info-button').click();

    await page.getByTestId('participants-button').click();

    await page.getByTestId('menu-icon').click();

    const signOutBtn = page.getByText('Abmelden');
    const signInBtn = page.getByText('Anmelden');

    const status = page.locator('mat-card-subtitle');

    if (await signOutBtn.isVisible()) {
        await signOutBtn.click();
        await page.getByRole('button', { name: 'Abmelden' }).click();
        await expect(status).toHaveText('nicht angemeldet');
    } else {
        await signInBtn.click();
        await expect(status).toHaveText('angemeldet');
    }
});
