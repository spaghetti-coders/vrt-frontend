import { expect, test } from '@playwright/test';
import { config } from 'dotenv';
import { LoginPage } from './login-page';

config();
test.describe.configure({ mode: 'serial' });
test.beforeEach(async ({ page }) => {
    const loginPage = new LoginPage(page);
    await loginPage.login(process.env['ADMIN_USERNAME'] as string, process.env['ADMIN_PASSWORD'] as string);
});

test('go to profile', async ({ page }) => {
    await page.getByRole('button', { name: 'dennis' }).click();
    await page.getByRole('menuitem', { name: 'Mein Profil' }).click();
    await expect(page.getByText('Benutzername')).toBeVisible();
});

test('switch theme', async ({ page }) => {
    await page.getByRole('button', { name: 'dennis' }).click();
    await page.getByRole('menuitem', { name: 'Dark Mode' }).click();
    await page.getByRole('button', { name: 'dennis' }).click();
    await page.getByRole('menuitem', { name: 'Light Mode' }).click();
});

test('change email', async ({ page }) => {
    page.on('response', async (response) => {
        if (response.url().includes('/api/Account/update')) {
            expect(response.status()).toEqual(200);
        }
    });

    await page.getByRole('button', { name: 'dennis' }).click();
    await page.getByRole('menuitem', { name: 'Mein Profil' }).click();
    await page.getByRole('button', { name: 'Profil bearbeiten' }).click();
    await page.getByPlaceholder('max.mustermann@example.com').click();
    await page.getByPlaceholder('max.mustermann@example.com').fill('peter.gottlieb@gmx.net');
    await page.getByRole('checkbox').click();
    await page.getByRole('button', { name: 'Aktualisieren' }).click();

    await expect(page.getByText('Gespeichert')).toBeVisible();
});

test('change password', async ({ page }) => {
    page.on('response', async (response) => {
        if (response.url().includes('/api/Account/update-password')) {
            expect(response.status()).toEqual(200);
        }
    });

    await page.getByRole('button', { name: 'dennis' }).click();
    await page.getByRole('menuitem', { name: 'Mein Profil' }).click();
    await page.getByRole('button', { name: 'Passwort ändern' }).click();
    await page.getByTestId('password').fill(process.env['ADMIN_PASSWORD'] as string);
    await page.getByTestId('password').press('Tab');
    await page.getByTestId('confirm_password').fill(process.env['ADMIN_PASSWORD'] as string);
    await page.getByRole('button', { name: 'Aktualisieren' }).click();
    await expect(page.locator('div').filter({ hasText: 'Passwort geändert' }).nth(2)).toBeVisible();

    await page.getByTestId('password').click();
    await page.getByTestId('password').fill(process.env['ADMIN_PASSWORD'] as string);
    await page.getByTestId('password').press('Tab');

    await page.getByTestId('confirm_password').click();
    await page.getByTestId('confirm_password').fill(process.env['ADMIN_PASSWORD'] as string);
    await page.getByRole('button', { name: 'Aktualisieren' }).click();
});
