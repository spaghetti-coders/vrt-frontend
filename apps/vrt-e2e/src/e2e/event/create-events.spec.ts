import { expect, test } from '@playwright/test';
import dayjs from 'dayjs';
import { LoginPage } from '../user-workflow/login-page';

const events = [
    { name: 'Events 1', description: '' },
    {
        name: 'Events 2',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque deleniti dignissimos dolorum earum laudantium maxime mollitia nihil, nisi, perferendis quis repellat saepe voluptatem. Debitis, eius ipsam natus pariatur placeat.',
    },
    {
        name: 'Events 3',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque deleniti dignissimos dolorum earum laudantium maxime mollitia nihil.',
    },
];
test.describe.configure({ mode: 'serial' });

test('Events Workflow', async ({ browser, browserName }) => {
    if (browserName === 'webkit') {
        return;
    }

    const context = await browser.newContext();
    const page = await context.newPage();

    await test.step('Create Events', async () => {
        const month = dayjs().add(1, 'month').format('MMM');
        const days = ['12', '22', '27'];
        const currentYear = dayjs().year();

        const { ADMIN_USERNAME, ADMIN_PASSWORD } = process.env;
        await new LoginPage(page).login(ADMIN_USERNAME as string, ADMIN_PASSWORD as string);

        for (const event of events) {
            await page.getByTestId('create-event-btn').click();
            await page.getByTestId('name').fill(event.name);

            await page.getByTestId('date-btn').click();
            await page.locator('.mat-calendar-period-button').click();
            await page.getByRole('button', { name: `${currentYear}`, exact: true }).click();

            await page.locator(`button:has-text("${month}")`).click();

            await page
                .locator(`button:has-text("${days[Math.floor(Math.random() * days.length)]}")`)
                .last()
                .click();

            await page.getByTestId('event-time').fill('12:00');

            await page.getByTestId('stepper-buttons-1').getByRole('button', { name: 'Weiter' }).click();

            await page.getByTestId('venue').click();
            await page.getByTestId('venueOption-0').click();

            await page.waitForTimeout(500);

            await page.getByTestId('capacity').fill('30');

            await page.waitForTimeout(500);

            await page.getByTestId('stepper-buttons-2').getByRole('button', { name: 'Weiter' }).click();

            await page.getByTestId('description').fill(event.description);

            await page.getByTestId('isOpenForRegistration').click();

            const responsePromise = page.waitForResponse('**/api/Events');

            await page.getByTestId('submit').click();

            const response = await responsePromise;

            expect(response.status()).toBe(201);
        }
    });

    await test.step('Update Event', async () => {
        const eventList = page.getByTestId('event-list');
        const menuBtn = page.getByTestId('vrt-event-card-0').getByTestId('card-menu');
        const editBtn = page.getByText('Bearbeiten');
        const nameInput = page.getByTestId('name');

        await expect(eventList).toBeVisible();

        await eventList.scrollIntoViewIfNeeded();

        await menuBtn.click();
        await editBtn.click();

        await expect(nameInput).toBeVisible();
        await nameInput.clear();
        await nameInput.fill('Updated Event');

        await page.getByTestId('stepper-buttons-1').getByRole('button', { name: 'Weiter' }).click();
        await page.getByTestId('stepper-buttons-2').getByRole('button', { name: 'Weiter' }).click();

        await page.getByTestId('submit').click();

        await page.waitForURL('**/events');

        await expect(page.getByText('Updated Event')).toBeVisible();
    });

    await test.step('Delete Event', async () => {
        const eventList = page.getByTestId('event-list');
        const menuBtn = page.getByTestId('vrt-event-card-0').getByTestId('card-menu');

        await expect(eventList).toBeVisible();

        await eventList.scrollIntoViewIfNeeded();

        await menuBtn.click();

        await page.getByTestId('delete-btn').click();

        await page.getByRole('button', { name: 'Löschen' }).click();

        await page.close();
    });
});
