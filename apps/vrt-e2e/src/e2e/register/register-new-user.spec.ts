import { expect, test } from '@playwright/test';

test('Register new user workflows', async ({ browser }) => {
    const user = {
        username: Math.random().toString(36).substring(2, 10),
        displayName: Math.random().toString(36).substring(2, 10),
        email: Math.random().toString(36).substring(2, 10) + '@mail.com',
        password: Math.random().toString(36).substring(2, 10),
        receiveNotifications: true,
    };

    const context = await browser.newContext();
    const page = await context.newPage();

    await test.step('should display register page', async () => {
        await page.goto('/auth/register');
        await expect(page.locator('[data-testid=register-wrapper]')).toBeVisible();
    });

    await test.step('user should fill out register form', async () => {
        await page.fill('[data-testid=username]', user.username);
        await page.fill('[data-testid=displayname]', user.displayName);
        await page.fill('[data-testid=password]', user.password);
        await page.fill('[data-testid=confirm_password]', user.password);
    });

    await test.step('should validate email required error if checkbox is checked', async () => {
        await page.getByRole('checkbox').check();
        await page.getByTestId('email').focus();
        await page.getByTestId('email').blur();
        await expect(page.getByText('Das Feld ist erforderlich')).toBeVisible();
        await page.getByTestId('email').fill(user.email);
        await page.getByRole('checkbox').uncheck();
    });

    await test.step('should create new user and redirect to confirm page', async () => {
        await page.route('/api/Account/register', (route) => route.continue());
        await page.click('[data-testid=submit]');

        await page.waitForURL('/auth/confirm');

        expect(page.url()).toContain('/auth/confirm');

        await page.click('[data-testid=back-btn]');
        expect(page.url()).toContain('/auth/login');
    });
});
