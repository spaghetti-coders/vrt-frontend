import { expect, test } from '@playwright/test';

test('Register new user workflows', async ({ browser }) => {
    const { CI } = process.env;

    if (!!CI) {
        return;
    }

    const { ADMIN_USERNAME, ADMIN_PASSWORD, USERNAME, PASSWORD } = process.env;

    const admin = {
        username: ADMIN_USERNAME as string,
        displayName: 'test-displayname',
        email: 'test-email@mail.com',
        password: ADMIN_PASSWORD as string,
        receiveNotifications: true,
    };

    const user = {
        username: USERNAME as string,
        displayName: 'test-displayname',
        email: 'test-email@mail.com',
        password: PASSWORD as string,
        receiveNotifications: true,
    };

    const context = await browser.newContext();
    const page = await context.newPage();

    await test.step('should display register page', async () => {
        await page.goto('/auth/register');
        await expect(page.locator('[data-testid=register-wrapper]')).toBeVisible();
    });

    await test.step('should fill out the register form', async () => {
        await page.fill('[data-testid=username]', admin.username);
        await page.fill('[data-testid=displayname]', admin.displayName);
        await page.fill('[data-testid=password]', user.password);
        await page.fill('[data-testid=confirm_password]', user.password);
    });

    await test.step('should validate email required error if checkbox is checked', async () => {
        await page.getByRole('checkbox').check();
        await page.getByTestId('email').focus();
        await page.getByTestId('email').blur();
        await expect(page.getByText('Das Feld ist erforderlich')).toBeVisible();
        await page.getByTestId('email').fill(user.email);
        await page.getByRole('checkbox').uncheck();
    });

    await test.step("should validate that the user can't register with taken email address", async () => {
        await page.route('**/api/Account/register', (route) => {
            route.fulfill({
                status: 400,
                contentType: 'application/json',
                body: JSON.stringify({
                    Code: 'E101',
                    Message: 'Email address already taken.',
                    Type: 2,
                }),
            });
        });
        await page.click('[data-testid=submit]');

        await expect(page.getByText('Email address already taken.')).toBeVisible();
    });
});
