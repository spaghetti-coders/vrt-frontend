import { expect, test } from '@playwright/test';
import { LoginPage } from '../user-workflow/login-page';

test('Admin Audit (mobile view)', async ({ page }) => {
    const { ADMIN_USERNAME, ADMIN_PASSWORD } = process.env;
    await new LoginPage(page).login(ADMIN_USERNAME as string, ADMIN_PASSWORD as string);

    await page.getByTestId('hamburger-menu').click();

    await page.getByTestId('admin-audit-link').click();

    await page.getByTestId('audit-menu-0').click();

    await page.getByTestId('audit-details-0').click();

    await expect(page.getByRole('heading', { name: 'Details' })).toBeVisible();

    await page.getByTestId('close-btn').click();

    await expect(page.getByTestId('audit-table')).toBeVisible();
});
