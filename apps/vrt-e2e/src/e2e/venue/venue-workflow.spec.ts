import { expect, test } from '@playwright/test';
import { LoginPage } from '../user-workflow/login-page';

test('Venue Workflow', async ({ browser }) => {
    const context = await browser.newContext();
    const page = await context.newPage();
    const randomVenueName = `test-venue-${Math.floor(Math.random() * 1000)}`;

    const { ADMIN_USERNAME, ADMIN_PASSWORD } = process.env;
    await new LoginPage(page).login(ADMIN_USERNAME as string, ADMIN_PASSWORD as string);

    await test.step('should create a new venue', async () => {
        await page.click('[data-testid="hamburger-menu"]');
        await page.click('[routerlink="/venues"]');
        await page.locator('button').filter({ hasText: 'add' }).click();
        await page.fill('#mat-input-3', randomVenueName);
        await page.fill('#mat-input-4', 'Muster Str. 33, 29883 Muster');
        await page.fill('#mat-input-5', '76');
        await page.locator('#cp').evaluate((element, value) => ((element as any).value = value), '#FF0000');
        await page.getByRole('checkbox').click();
        await page.getByRole('button', { name: 'Erstellen' }).click();
        await page.waitForURL('**/venues');
        await expect(page.getByText(randomVenueName).last()).toBeVisible();
    });

    await test.step('should update venue', async () => {
        await page.click(':nth-child(2) > .cdk-column-isActive > .ng-star-inserted > .mat-icon');
        await page.getByRole('row', { name: randomVenueName }).last().getByRole('button').click();
        await page.getByText('Bearbeiten').click();
        await page.getByTestId('name').fill(`${randomVenueName} UPDATED`);
        await page.getByText(' Aktualisieren ').click();
        await expect(page.getByText('Gespeichert')).toBeVisible();
    });

    await test.step('should show venue details', async () => {
        await page.getByRole('row', { name: randomVenueName }).last().getByRole('button').click();
        await page.getByText('Details').click();
        await expect(page.getByText(randomVenueName).last()).toBeVisible();
        await expect(page.locator('.mat-mdc-card-subtitle')).toBeVisible();
        await expect(page.locator('.label')).toBeVisible();
        await page.getByText('Zurück').click();
    });

    await test.step('should delete venue', async () => {
        await page.getByRole('row', { name: randomVenueName }).last().getByRole('button').click();
        await page.getByText('Löschen').click();
        await expect(page.locator('p')).toBeVisible();
        await page.getByRole('button', { name: 'Löschen' }).click();
        await expect(page.getByText(randomVenueName, { exact: true })).not.toBeVisible();
    });
});
