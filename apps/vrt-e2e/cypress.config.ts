import { nxE2EPreset } from '@nx/cypress/plugins/cypress-preset';
import { defineConfig } from 'cypress';

export default defineConfig({
    e2e: {
        ...nxE2EPreset(__filename),
        testIsolation: false,
    },
    videosFolder: 'cypress/videos',
    screenshotsFolder: 'cypress/screenshots',
    fixturesFolder: 'cypress/fixtures',
    viewportWidth: 375,
    viewportHeight: 812,
    watchForFileChanges: false,
    projectId: 'mj2vyn',
    chromeWebSecurity: false,
});
