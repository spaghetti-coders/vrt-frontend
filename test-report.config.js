module.exports = {
    reports: [
        {
            targets: [
                {
                    name: 'slack',
                    inputs: {
                        url: `${process.env.SLACK_URL}`,
                        title: 'VRT E2E 🚀',
                        publish: 'test-summary-slim',
                    },
                    extensions: [
                        {
                            name: 'hyperlinks',
                            inputs: {
                                links: [
                                    {
                                        text: 'Pipeline',
                                        url: 'https://gitlab.com/spaghetti-coders/vrt-frontend/-/pipelines',
                                    },
                                    {
                                        text: 'Test Report',
                                        url: `${process.env.REPORT_URL}`,
                                    },
                                ],
                            },
                        },
                    ],
                },
            ],
            results: [
                {
                    type: 'junit',
                    files: ['reports/results.xml'],
                },
            ],
        },
    ],
};
